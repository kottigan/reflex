#ifndef SYMBOLDISPLAY_H
#define SYMBOLDISPLAY_H

#include "reflex/sinks/Event.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/io/Display.h"

/** SymbolDisplay
 * convenient access of symbols on the EZChronos display
 * through event based interface
 */
class SymbolDisplay : public reflex::Activity
{
public:
    reflex::Event input; // call this to access the symbol

    /* Mode for symbol access on notify */
    enum Mode {
        SET // set the symbol
        ,CLEAR // clear the symbol
        ,TOGGLE // toggle the symbol
    };

    enum Symbol {
        HEART = reflex::display::ICON_HEART
        ,STOPWATCH = reflex::display::ICON_STOPWATCH
        ,RECORD = reflex::display::ICON_RECORD
        ,ALARM = reflex::display::ICON_ALARM
        ,BEEPER1 = reflex::display::ICON_BEEPER1
        ,BEEPER2 = reflex::display::ICON_BEEPER2
        ,BEEPER3 = reflex::display::ICON_BEEPER3
    };

private:
    Mode mode; // current mode
    reflex::display::Symbol symbol; // current symbol
    reflex::display::Symbols *symbols; // access to display symbols

public:
    /* constructor
     * @param mode, initial access mode
     * @param symbol, initial symbol to access
     */
    SymbolDisplay(Mode mode = SET, Symbol symbol = HEART)
    {
        input.init(this); // trigger activity on input
        this->symbols = NULL;
        setMode(mode);
        setSymbol(symbol);
    }

    /* init
     * configure necessary components
     * @param symbols, class to access display symbols as defined in Display.h
     */
    void init(reflex::display::Symbols *symbols)
    {
        this->symbols = symbols;
    }

    /* setMode
     * set the symbol access mode
     * @param mode, new mode
     */
    void setMode(Mode mode) {
        this->mode = mode;
    }

    /* setSymbol
     * set the symbol that is accessed
     * @param symbol, new symbol
     */
    void setSymbol(Symbol symbol) {
        this->symbol = (reflex::display::Symbol) symbol;
    }

    /* run
     * main method of activity
     * access symbol as specified in current mode
     */
    void run () {
        if (!symbols) return;

        switch (mode) {
        case SET:
            symbols->set(symbol);
            break;
        case CLEAR:
            symbols->clear(symbol);
            break;
        case TOGGLE:
            symbols->toggle(symbol);
            break;
        default: // should not happen
            break;
        }
    }
};

#endif // SYMBOLDISPLAY_H
