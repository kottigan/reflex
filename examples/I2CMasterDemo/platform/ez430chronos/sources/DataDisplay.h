#ifndef DATADISPLAY_H
#define DATADISPLAY_H

#include "reflex/types.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/scheduling/Activity.h"

/** DataDisplay
 *  Prints the TData (sensor readings etc.) value on the EZ430 Display
 *  (upper line or lower line). It prints hexadecimal representations
 *  of data up to 16 bit on upper display line or up to 20 bits on lower.
 */
template<typename TData, typename TDisplayLine>
class DataDisplay : public reflex::Activity {
    TDisplayLine *display;
public:
    reflex::SingleValue1<TData> input;

    /** init */
    void init(TDisplayLine* dis) {
        display = dis;
        input.init(this);
    }

    /** run (display value) */
    void run () {
        if (display)
            *display = input.get();
    }
};

#endif // DATADISPLAY_H
