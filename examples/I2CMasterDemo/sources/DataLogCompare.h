#ifndef DATALOGCOMPARE_H
#define DATALOGCOMPARE_H


#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/sinks/Event.h"

/* DataLogCompare
 * read and write structs of data and compare the read value with
 * the previously written ones. Signal success or error depending
 * on whether the data matches.
 * Intended for use with EEPROM drivers like reflex/io/EEPROM_24xx512.h
 * This could be used as a logging component with validation.
 *
 * NOTE: everything inline because I feel lazy today...
 */
template <typename TData>
class DataLogCompare : public reflex::Activity
{
private:
    reflex::Sink2<unsigned char*, uint16> *write; // write->assign(buf, len)
    reflex::Sink2<unsigned char*, uint16> *read; // read->assign(buf, len)
    reflex::Sink0 *signal_success; // called after successful compare
    reflex::Sink0 *signal_error; // called on any error or mismatch on compare

    TData original; // the original data received through input
    TData compareMe; // read, then compare with input

    enum STATUS {
        IDLE // wait for input
        ,WRITING // write in progess
        ,READING // read in progess
    } status; // status for state machine

public:
    reflex::SingleValue1<TData> input; // receive new data to compare
    reflex::Event success; // notified if read/write was successful
    reflex::Event error; // notified on error

    /* constructor */
    DataLogCompare()
        : errorFunc(*this)
    {
        input.init(this); // call run on new input
        success.init(this); // call run after read/write success
        error.init(&errorFunc); // call error functor on error
        status = IDLE;
    }

    /* init
     * connect the peripheral components
     * @param read, where to read data from
     * @param write, where to write data to
     * @param success, called after successful compare
     * @param error, called when an error occured (mismatch etc.)
     */
    void init(reflex::Sink2<unsigned char*, uint16> *read,
              reflex::Sink2<unsigned char*, uint16> *write,
              reflex::Sink0 *success, reflex::Sink0 *error)
    {
        this->read = read;
        this->write = write;
        this->signal_success = success;
        this->signal_error = error;
    }

    /* run
     * main method of activity
     * triggered by input and success (yeah!)
     * issues requests according to current state
     */
    void run() {
        switch (status)
        {
        case IDLE:
            // we presumably got an input
            original = input.get();
            status = WRITING;
            if (write) write->assign((unsigned char*)(&original), sizeof(TData));
            break;
        case WRITING:
            // writing finished successfully
            status = READING;
            if (read) read->assign((unsigned char*)(&compareMe), sizeof(TData));
            break;
        case READING:
            // read finished sucessfully
            if (original == compareMe) {
                if (signal_success) signal_success->notify();
                status = IDLE;
                break;
            }
            // call default error function
            on_error();
            break;
        default:
            // should not happen
            break;
        }
    }

private:
    /* on_error
     * an error occured. This is an activity triggered by the error event.
     * It will notify the signal_error sink and reset state machine to idle.
     */
    void on_error() {
        status = IDLE;
        if (signal_error) signal_error->notify();
    }

    /* functor for error function as activity */
    reflex::ActivityFunctor<DataLogCompare<TData>, &DataLogCompare<TData>::on_error> errorFunc;
};

#endif // DataLogCompare_H
