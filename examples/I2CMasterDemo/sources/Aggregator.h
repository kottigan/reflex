#ifndef AGGREGATOR_H
#define AGGREGATOR_H

#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/SingleValue.h"

/* class Aggregator
 * receive values of multiple data types and combine them in a new struct.
 * At least 2 template parameters have to be provided, since an aggregator
 * is not useful for less than 2 data types. The new struct will be signaled
 * through a Sink1 interface, when every input has received data.
 */
template< typename T1
        , typename T2
        , typename T3 = void
        , typename T4 = void>
class Aggregator;

/* Aggregator for 2 values */
template<typename T1, typename T2>
class Aggregator<T1, T2>
{
public:
    /* new aggregated type */
    struct ResultT {
        T1 val1;
        T2 val2;

        /* provide default == operator that does per value compare */
        bool operator==(const ResultT &other)
        {
            return ((val1 == other.val1) && (val2 == other.val2));
        }
        /* and != operator based on that... */
        bool operator!=(const ResultT &other)
        {
            return !(*this == other);
        }
    };

    /* inputs for single values */
    reflex::SingleValue1<T1> input1;
    reflex::SingleValue1<T2> input2;

private:
    ResultT value; // struct that will be assigned to output
    reflex::Sink1<ResultT> *output; // output that receives combined value
    uint8 freebits; // bitfield for bookkeeping of already assigned values
    /* enum for convenient bookkeeping */
    enum State {
        FREE = 0x0
        ,VAL1 = 0x01
        ,VAL2 = 0x02
        ,FULL = VAL1 + VAL2
    };

public:
    /* constructor */
    Aggregator()
        : assign_T1_func(*this)
        , assign_T2_func(*this)
    {
        input1.init(&assign_T1_func);
        input2.init(&assign_T2_func);
    }

    /* init
     * connect subsequent sink
     */
    void init(reflex::Sink1<ResultT> *output) {
        this->output = output;
    }

private:
    /* check_full
     * check whether freebits are set for all inputs
     * signal output if necessary and reset freebits
     */
    void check_full() {
        if (freebits == FULL) {
            if (output) output->assign(value);
            freebits = FREE;
        }
    }

    /* assign_T1
     * received new value for T1, update freebits, assign output if FULL
     */
    void assign_T1() {
        value.val1 = input1.get();
        freebits |= VAL1;
        check_full();
    }

    /* assign_T2
     * received new value for T2, update freebits, assign output if FULL
     */
    void assign_T2() {
        value.val2 = input2.get();
        freebits |= VAL2;
        check_full();
    }

    /* functors that turn above methods into activities */
    reflex::ActivityFunctor<Aggregator<T1,T2>, &Aggregator<T1,T2>::assign_T1> assign_T1_func;
    reflex::ActivityFunctor<Aggregator<T1,T2>, &Aggregator<T1,T2>::assign_T2> assign_T2_func;
};

/* Aggregator for 3 values */
template<typename T1, typename T2, typename T3>
class Aggregator<T1, T2, T3>
{
public:
    /* new aggregated type */
    struct ResultT {
        T1 val1;
        T2 val2;
        T3 val3;

        /* provide default == operator that does per value compare */
        bool operator==(const ResultT &other)
        {
            return ((val1 == other.val1)
                    && (val2 == other.val2)
                    && (val3 == other.val3));
        }
        /* and != operator based on that... */
        bool operator!=(const ResultT &other)
        {
            return !(*this == other);
        }
    };

    /* inputs for single values */
    reflex::SingleValue1<T1> input1;
    reflex::SingleValue1<T2> input2;
    reflex::SingleValue1<T3> input3;

private:
    ResultT value; // struct that will be assigned to output
    reflex::Sink1<ResultT> *output; // output that receives combined value
    uint8 freebits; // bitfield for bookkeeping of already assigned values
    /* enum for convenient bookkeeping */
    enum State {
        FREE = 0x0
        ,VAL1 = 0x01
        ,VAL2 = 0x02
        ,VAL3 = 0x04
        ,FULL = VAL1 + VAL2 + VAL3
    };

public:
    /* constructor */
    Aggregator()
        : assign_T1_func(*this)
        , assign_T2_func(*this)
        , assign_T3_func(*this)
    {
        input1.init(&assign_T1_func);
        input2.init(&assign_T2_func);
        input3.init(&assign_T3_func);
    }

    /* init
     * connect subsequent sink
     */
    void init(reflex::Sink1<ResultT> *output) {
        this->output = output;
    }

private:
    /* check_full
     * check whether freebits are set for all inputs
     * signal output if necessary and reset freebits
     */
    void check_full() {
        if (freebits = FULL) {
            if (output) output->assign(value);
            freebits = FREE;
        }
    }

    /* assign_T1
     * received new value for T1, update freebits, assign output if FULL
     */
    void assign_T1() {
        value.val1 = input1.get();
        freebits |= VAL1;
        check_full();
    }

    /* assign_T2
     * received new value for T2, update freebits, assign output if FULL
     */
    void assign_T2() {
        value.val2 = input2.get();
        freebits |= VAL2;
        check_full();
    }

    /* assign_T3
     * received new value for T3, update freebits, assign output if FULL
     */
    void assign_T3() {
        value.val3 = input3.get();
        freebits |= VAL3;
        check_full();
    }

    /* functors that turn above methods into activities */
    reflex::ActivityFunctor<Aggregator<T1,T2,T3>, &Aggregator<T1,T2,T3>::assign_T1> assign_T1_func;
    reflex::ActivityFunctor<Aggregator<T1,T2,T3>, &Aggregator<T1,T2,T3>::assign_T2> assign_T2_func;
    reflex::ActivityFunctor<Aggregator<T1,T2,T3>, &Aggregator<T1,T2,T3>::assign_T3> assign_T3_func;
};

/* Aggregator for 4 values */
template<typename T1, typename T2, typename T3, typename T4>
class Aggregator
{
public:
    /* new aggregated type */
    struct ResultT {
        T1 val1;
        T2 val2;
        T3 val3;
        T4 val4;

        /* provide default == operator that does per value compare */
        bool operator==(const ResultT &other)
        {
            return ((val1 == other.val1)
                    && (val2 == other.val2)
                    && (val3 == other.val3)
                    && (val4 == other.val4));
        }
        /* and != operator based on that... */
        bool operator!=(const ResultT &other)
        {
            return !(*this == other);
        }
    };

    /* inputs for single values */
    reflex::SingleValue1<T1> input1;
    reflex::SingleValue1<T2> input2;
    reflex::SingleValue1<T3> input3;
    reflex::SingleValue1<T4> input4;

private:
    ResultT value; // struct that will be assigned to output
    reflex::Sink1<ResultT> *output; // output that receives combined value
    uint8 freebits; // bitfield for bookkeeping of already assigned values
    /* enum for convenient bookkeeping */
    enum State {
        FREE = 0x0
        ,VAL1 = 0x01
        ,VAL2 = 0x02
        ,VAL3 = 0x04
        ,VAL4 = 0x08
        ,FULL = VAL1 + VAL2 + VAL3 + VAL4
    };

public:
    /* constructor */
    Aggregator()
        : assign_T1_func(*this)
        , assign_T2_func(*this)
        , assign_T3_func(*this)
        , assign_T4_func(*this)
    {
        input1.init(&assign_T1_func);
        input2.init(&assign_T2_func);
        input3.init(&assign_T3_func);
        input4.init(&assign_T4_func);
    }

    /* init
     * connect subsequent sink
     */
    void init(reflex::Sink1<ResultT> *output) {
        this->output = output;
    }

private:
    /* check_full
     * check whether freebits are set for all inputs
     * signal output if necessary and reset freebits
     */
    void check_full() {
        if (freebits = FULL) {
            if (output) output->assign(value);
            freebits = FREE;
        }
    }

    /* assign_T1
     * received new value for T1, update freebits, assign output if FULL
     */
    void assign_T1() {
        value.val1 = input1.get();
        freebits |= VAL1;
        check_full();
    }

    /* assign_T2
     * received new value for T2, update freebits, assign output if FULL
     */
    void assign_T2() {
        value.val2 = input2.get();
        freebits |= VAL2;
        check_full();
    }

    /* assign_T3
     * received new value for T3, update freebits, assign output if FULL
     */
    void assign_T3() {
        value.val3 = input3.get();
        freebits |= VAL3;
        check_full();
    }

    /* assign_T4
     * received new value for T4, update freebits, assign output if FULL
     */
    void assign_T4() {
        value.val4 = input4.get();
        freebits |= VAL4;
        check_full();
    }

    /* functors that turn above methods into activities */
    reflex::ActivityFunctor<Aggregator<T1,T2,T3,T4>, &Aggregator<T1,T2,T3,T4>::assign_T1> assign_T1_func;
    reflex::ActivityFunctor<Aggregator<T1,T2,T3,T4>, &Aggregator<T1,T2,T3,T4>::assign_T2> assign_T2_func;
    reflex::ActivityFunctor<Aggregator<T1,T2,T3,T4>, &Aggregator<T1,T2,T3,T4>::assign_T3> assign_T3_func;
    reflex::ActivityFunctor<Aggregator<T1,T2,T3,T4>, &Aggregator<T1,T2,T3,T4>::assign_T4> assign_T4_func;
};

#endif // AGGREGATOR_H
