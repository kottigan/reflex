/*
 *    This file is part of REFLEX.
 *
 *    Copyright 2014 BTU Cottbus-Senftenberg, Distributed Systems /
 *    Operating Systems Group. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "BlinkApplication.h"

using namespace examples;
using namespace virtualtimers;
using namespace reflex;

BlinkApplication::BlinkApplication() :
    t0(VirtualTimer::PERIODIC),
    t1(VirtualTimer::PERIODIC),
    t2(VirtualTimer::PERIODIC),
    func0(*this), func1(*this), func2(*this)
{
    // Connect timer outputs to events
    t0.connect_output(&e0);
    t1.connect_output(&e1);
    t2.connect_output(&e2);

    // Events are related to activities
    e0.init(&func0);
    e1.init(&func1);
    e2.init(&func2);

    // Connect to the display driver
    out_state = displayDriver.get_in_state();

    // Initialize timers with random values
    t0.set(333);
    t1.set(1111);
    t2.set(6666);

    // Initialize other members
    currentState = 0;
}

void BlinkApplication::blink0()
{
    currentState ^= 0x01;
    out_state->assign(currentState);
}

void BlinkApplication::blink1()
{
    currentState ^= 0x02;
    out_state->assign(currentState);
}

void BlinkApplication::blink2()
{
    currentState ^= 0x04;
    out_state->assign(currentState);
}
