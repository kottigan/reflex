#ifndef OUTPUTDRIVER
#define OUTPUTDRIVER
/*
 *    This file is part of REFLEX.
 *
 *    Copyright 2014 BTU Cottbus-Senftenberg, Distributed Systems /
 *    Operating Systems Group. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include <reflex/scheduling/ActivityFunctor.h>
#include <reflex/sinks/SingleValue.h>
#include <reflex/timer/VirtualTimer.h>

namespace examples {
namespace virtualtimers {
namespace posix {

/*!
\brief Prints the timer status on the command line.

This class represents a platform-dependent component.

*/
class DisplayDriver {
public:
    DisplayDriver();
    inline reflex::Sink1<uint8>* get_in_state() { return &in_state; }

private:
    reflex::SingleValue1<uint8> in_state;

    void run_display();
    reflex::ActivityFunctor<DisplayDriver, &DisplayDriver::run_display> act_display;

    reflex::VirtualTimer timer; // dummy timer to acquire system time
};

}

namespace platform = posix;
}
}

#endif
