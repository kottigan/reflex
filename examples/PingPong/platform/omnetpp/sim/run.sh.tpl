#!/bin/bash
TARGET=@target@
REFLEXPATH=@reflexpath@
MIXIMPATH=@miximpath@
OMNETPATH=@omnetpath@
NEDPATH=@nedpath@
TARGETDIR=@targetdir@

export NEDPATH=$NEDPATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$MIXIMPATH/src/:$OMNETPATH/lib/

$TARGETDIR/$TARGET -r 0 -f ../sim/omnetpp.ini
