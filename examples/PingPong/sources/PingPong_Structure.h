/*
 * PingPong_Structure.h
 *
 *  Created on: 16.07.2012
 *      Author: slohs
 */

#ifndef PINGPONG_STRUCTURE_H_
#define PINGPONG_STRUCTURE_H_


#include "reflex/types.h"
#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/Queue.h"
#include "reflex/scheduling/ActivityFunctor.h"

template <typename T>
class PingPong_Structure
{
protected:
	/** Pointer to lower Layer */
	reflex::Sink1<reflex::Buffer*>* output;

	/** Input sink for receiving messages */
	reflex::Queue<reflex::Buffer*> input;

	/** Pool for Message generating */
	reflex::Pool* pool;

	/** Timer to schedule the round handle method */
	reflex::VirtualTimer timer;

	/** Timer event */
	reflex::Event event;

public:
	PingPong_Structure() :
			timer(reflex::VirtualTimer::ONESHOT)
			, timerActivity(*this)
			, receiveActivity(*this)
	{
		pool = NULL;
		output = NULL;

		timer.connect_output(&event);

		event.init(&timerActivity);

		input.init(&receiveActivity);
	}

	/** Stub for timer handling */
	void timerHandle_Stub()
	{
		static_cast<T*>(this)->timerHandle_Impl();
	}

	/** Stub for message reception handling */
	void receiveHandle_Stub()
	{
		static_cast<T*>(this)->receiveHandle_Impl();
	}

	/** Connects the memory buffer pool */
	void connect_Pool(reflex::Pool* pool)
	{
		this->pool = pool;
	}

	/** Connects the output to lower layer */
	void connect_output(reflex::Sink1<reflex::Buffer*>* lowerLayerIn)
	{
		output = lowerLayerIn;
	}

	/** Returns the input for the lower layer */
	reflex::Sink1<reflex::Buffer*>* get_input()
	{
		return &input;
	}

protected:
	reflex::ActivityFunctor<PingPong_Structure,&PingPong_Structure::timerHandle_Stub> timerActivity;
	reflex::ActivityFunctor<PingPong_Structure,&PingPong_Structure::receiveHandle_Stub> receiveActivity;
};


#endif /* PINGPONG_STRUCTURE_H_ */
