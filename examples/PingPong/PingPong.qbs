import qbs 1.0
import qbs.FileInfo
import qbs.TextFile
import qbs.Process
import qbs.ModUtils
import ReflexApplication
import ReflexProject

ReflexProject {
    buffer_maxPoolCount : 2
    core_schedulingScheme : "FIFO_SCHEDULING"
    omnetpp_nodeCount : 255

    ReflexApplication {
        name: "pingpongreflex"

        files: [
            "sources/PingPong.cc",
            "platform/" + qbs.architecture + "/sources/*.cc",
        ]

        cpp.includePaths : [
            "sources",
            "platform/" + qbs.architecture + "/sources"
        ]
    }


    Product
    {
        name : "omnet-specific-files"
        type : "runscripts"
        condition : qbs.architecture == "omnetpp"

        Depends { name : "pingpongreflex" }
        Depends { name : "reflex.omnetpp" }

        Group {
            name : "application ned files"
            condition : qbs.architecture == "omnetpp"
            prefix : "platform/omnetpp/ned/pingpongreflex/"
            files : [
                "*.ned"
            ]
            qbs.install : true
            qbs.installDir : "ned/pingpongreflex/"
        }

        Group {
            name : "simulation and ini files"
            condition : qbs.architecture == "omnetpp"
            prefix : "platform/omnetpp/sim/"
            files : [
                "*.xml",
                "*.ini",
                "topologies"
            ]
            qbs.install : true
            qbs.installDir : "sim"
        }

        /* Runscripts are tagged as application files,
          otherwise the rule below would not be executed */
        Group {
            name : "runscripts"
            fileTagsFilter : "runscripts"
            qbs.install : true
            qbs.installDir : "sim"
        }

        /* Tag template files for the rule below. */
        Group {
            name : "runscripts-tpl"
            prefix : "platform/omnetpp/sim/"
            files : [
                "run.sh.tpl",
                "debug.sh.tpl"
            ]
            fileTags : "tpl"
        }

        property var mappings : ({
                targetdir : "../bin",
                target : "pingpongreflex",
                reflexpath : FileInfo.joinPaths(sourceDirectory, "/../../"),
                omnetpath : reflex.omnetpp.omnetpath,
                miximpath : reflex.omnetpp.miximpath,
                nedpath : "../ned"
                        + ":$MIXIMPATH/src/base"
                        + ":$MIXIMPATH/src/modules"
                        + ":$MIXIMPATH/src/inet_stub"
        })

        Rule {
            name : "process template files for runscripts"
            inputs : 'tpl'

            Artifact {
                filePath: input.completeBaseName
                fileTags: "runscripts"
            }

            prepare: {
                var cmd = new JavaScriptCommand();
                cmd.description = "Process template file '" + input.filePath + "'";
                cmd.highlight = "codegen";
                cmd.sourceCode = function() {
                    var mappings = product.mappings;
                    var file = new TextFile(input.filePath);
                    var content = file.readAll();
                    file.close();
                    for (key in mappings) {
                        var value = mappings[key];
                        var pattern = '@' + key + '@';
                        content = content.replace(pattern, value);
                    }
                    file = new TextFile(output.filePath, TextFile.WriteOnly);
                    file.truncate();
                    file.write(content);
                    file.close();

                    var process = new Process();
                    process.exec('chmod', ['+x', output.filePath]);
                }
                return cmd;
            }
        }
    }
}
