#include "TestSuite.h"
#include "TestEnvironment.h"
#include "reflex/memory/Flash.h"
#include "reflex/System.h"

using namespace reflex;
using namespace mcu;
using namespace unitTest;

TestEnvironment::TestEnvironmentImplementation::TestEnvironmentImplementation() :
    _communicationPool(0), _outputChannel(&_communicationPool), act_result(*this), act_txFinished(*this)
{
    in_txFinished.init(&act_txFinished);
	in_result.init(&act_result);
    currentIndex = 0;
    testList = 0;

#ifdef TIME_TRIGGERED_SCHEDULING
    // Register internal activities with slot 0
    // This ensures, that the activities are executed
    // at least once, even though no timer has been
    // configured for the scheduler, yet.
    getSystem().scheduler.registerActivity(&act_result, 0);
    getSystem().scheduler.registerActivity(&act_txFinished, 0);
#endif

#if defined EDF_SCHEDULING || defined EDF_SCHEDULING_SIMPLE
    // Configure these activities with a response time bigger than 0 because
    // preemptive test cases must be able to set up activities with smaller
    // response times.
    act_result.setResponseTime(TestEnvironment::DefaultEdfResponseTime);
    act_txFinished.setResponseTime(TestEnvironment::DefaultEdfResponseTime);
#endif
}

void TestEnvironment::TestEnvironmentImplementation::run_result()
{
	TestResult result = in_result.get();
    _outputChannel.write("\"; ");
	switch (result)
	{
	case TestOk:
        _outputChannel.write("ok");
		break;
	case TestFailed:
        _outputChannel.write("not ok");
		break;
	case TestSkipped:
        _outputChannel.write("skip");
		break;
	}
    _outputChannel.write("\n");
    _outputChannel.flush();
	currentTestSuite()->~TestSuite();
    currentIndex++;
}

void TestEnvironment::TestEnvironmentImplementation::run_txFinished()
{
    if (currentIndex < sizeOfTestList)
    {
        TestCase test = Flash::read(testList + currentIndex);
        _outputChannel.write(test.name);
        _outputChannel.write("; ");
        _outputChannel.write(test.id);
        _outputChannel.write("; \"");
        GeneratorStub generator = test.stub;
        generator(&_testSuiteBuffer, test.id);
    }
    else
    {
        _outputChannel.write("# FINISHED");
        _outputChannel.writeln();
        act_txFinished.lock();
    }
}

void TestEnvironment::TestEnvironmentImplementation::setTestList(const TestCase* list, uint16 size)
{
    this->testList = list;
    this->sizeOfTestList = size;
}

void TestEnvironment::TestEnvironmentImplementation::start()
{
    _outputChannel.write("Running tests 1..");
    _outputChannel.write(sizeOfTestList);
    _outputChannel.writeln();
}
