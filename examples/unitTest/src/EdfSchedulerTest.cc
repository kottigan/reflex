
#include "EdfSchedulerTest.h"

using namespace unitTest;
using namespace reflex;

EdfSchedulerTest::EdfSchedulerTest(uint16 id) : TestSuite(id),
    act_1(*this), act_2(*this), execLog{0}, expectedLog{0},
        timer(VirtualTimer::ONESHOT)
{
    in_1.init(&act_1);
    in_2.init(&act_2);
    timer.connect_output(this);

    currentExecCount = 0;

    switch (currentTestId())
    {
    /* act_1 comes first, then _act2 */
    case ExecutionOrder:
        expectedLog[0] = 1;
        expectedLog[1] = 2;
        in_1.notify();
        in_2.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    /* exec act_1 twice */
    case MultipleExecution:
        expectedLog[0] = 1;
        expectedLog[1] = 1;
        in_1.notify();
        in_1.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    /* Locked activity must not be scheduled */
    case Lock:
        expectedLog[0] = 0;
        expectedLog[1] = 0;
        act_1.lock();
        in_1.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    /* Locked activity must be scheduled after being unlocked*/
    case Unlock:
        expectedLog[0] = 1;
        expectedLog[1] = 0;
        act_1.lock();
        in_1.notify();
        act_1.unlock();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    case TriggerTightDeadlineAfterLoose:
        expectedLog[0] = 2;
        expectedLog[1] = 1;
        act_1.setResponseTime(TestEnvironment::DefaultEdfResponseTime + 2);
        act_2.setResponseTime(TestEnvironment::DefaultEdfResponseTime + 1);
        in_1.notify();
        in_2.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    case TriggerLooseDeadlineAfterTight:
        expectedLog[0] = 1;
        expectedLog[1] = 2;
        act_1.setResponseTime(TestEnvironment::DefaultEdfResponseTime + 1);
        act_2.setResponseTime(TestEnvironment::DefaultEdfResponseTime + 2);
        in_1.notify();
        in_2.notify();
        setTimeoutMs(DefaultTimeoutMs);
        break;
    // Activities triggered by an interrupt handler must be dispatched after
    // the interrupt handler has been finished. This tests correct usage of
    // the scheduling monitor (issue #6).
    case SchedulingMonitor:
        expectedLog[0] = 3;
        expectedLog[1] = 1;
        timer.set(1);
        setTimeoutMs(DefaultTimeoutMs);
        break;
    // A tight deadline activity must preempt a loose one.
    case LooseDL_Interrupt_TightDL:
        expectedLog[0] = 1;
        expectedLog[1] = 0;
        act_1.setResponseTime(TestEnvironment::DefaultEdfResponseTime - 1);
        in_1.notify();
        execLog[currentExecCount] = 0;
        currentExecCount++;
        setTimeoutMs(DefaultTimeoutMs);
        break;
    // A loose priority activity must NOT preempt a tight one.
    case TightDL_Interrupt_LooseDL:
        expectedLog[0] = 0;
        expectedLog[1] = 1;
        act_1.setResponseTime(TestEnvironment::DefaultEdfResponseTime + 1);
        in_1.notify();
        execLog[currentExecCount] = 0;
        currentExecCount++;
        setTimeoutMs(DefaultTimeoutMs);
        break;
    default:
        setResultSkipped();
    }
}

EdfSchedulerTest::~EdfSchedulerTest()
{
    timer.stop();
}

void EdfSchedulerTest::run_1()
{
    execLog[currentExecCount] = 1;
    currentExecCount++;
}

void EdfSchedulerTest::run_2()
{
    execLog[currentExecCount] = 2;
    currentExecCount++;
}

void EdfSchedulerTest::timeout()
{
    switch(currentTestId())
    {
    case ExecutionOrder:
    case MultipleExecution:
    case Lock:
    case Unlock:
    case TriggerTightDeadlineAfterLoose:
    case TriggerLooseDeadlineAfterTight:
    case SchedulingMonitor:
    case LooseDL_Interrupt_TightDL:
    case TightDL_Interrupt_LooseDL:
        VERIFY((execLog[0] == expectedLog[0]) && (execLog[1] == expectedLog[1]),
                "log: (%u,%u), expected (%u, %u)",
                execLog[0], execLog[1], expectedLog[0], expectedLog[1]);
        setResultOk();
        break;
    default:
        TestSuite::timeout();
    }
}

/*!
This is an synchronous method, called directly from a VirtualTimer on interrupt
level. This is necessary to test preemption.
 */
void EdfSchedulerTest::notify()
{
    switch(currentTestId())
    {
    case SchedulingMonitor:
        in_1.notify();
        execLog[currentExecCount] = 3;
        currentExecCount++;
        break;
    default:
        VERIFY(false, "timer must NOT fire in this testcase" );
    }
}
