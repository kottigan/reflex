#ifndef NodeConfiguration_h
#define NodeConfiguration_h

#include "reflex/data_types/Singleton.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/io/IOPort.h"
#include "reflex/io/Usart.h"
#include "reflex/System.h"
#include "TestEnvironment.h"
//#include "EnumeralMismatch.h"

namespace reflex
{
enum PowerGroups
{
	AWAKE = reflex::PowerManager::GROUP1,
	SLEEP = reflex::PowerManager::GROUP2,
	NOOPGROUP = reflex::PowerManager::NOTHING
};

template<mcu::McuType mcuType>
class NodeConfiguration: public System
{
public:
	NodeConfiguration();

	PoolManager poolManager;
	atmega::Usart<mcu::Usart0> usart;
    unitTest::TestEnvironment testEnvironment;

	static const unitTest::TestCase testList[];
};

inline NodeConfiguration<mcu::CurrentMcu>& getApplication()
{
	extern NodeConfiguration<mcu::CurrentMcu> system;
	return system;
}



} //reflex

#endif

