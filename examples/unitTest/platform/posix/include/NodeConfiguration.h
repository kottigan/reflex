#ifndef NODECONFIGURATION_H
#define NODECONFIGURATION_H

#include "reflex/io/Console.h"
#include "reflex/System.h"

#include "TestCase.h"
#include "TestEnvironment.h"

namespace reflex
{

enum PowerGroups
{
	DEFAULT = reflex::PowerManager::GROUP1, DISABLED = reflex::PowerManager::NOTHING
};


class NodeConfiguration : public System
{
public:
	NodeConfiguration();

private:
    Console console;
    unitTest::TestEnvironment testEnvironment;
	static const unitTest::TestCase testList[];
};

inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} //reflex

#endif
