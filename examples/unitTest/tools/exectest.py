#!/usr/bin/env python

from threading import Thread
from Queue import Queue, Empty


class NonBlockingStreamReader:
	'''
	Reads input from a stream object without blocking.

	NonBlockingStreamReader provides an asynchronous interface to text streams
	that would normally block if no more data is available. This is a
	especially a problem when reading from stdout in long-running
	sub-processes, where one would like to cancel reading after a certain time.

	The method :meth:`NonBlockingStreamReader.readline` can be used to retrieve
	data.

	NonBlockingStreamReader runs in its own thread and uses a thread-safe queue
	as input buffer. It supports the
	'''

	def __init__(self, stream):
		'''
		Creates a new reader instance whereas *stream* is the input to read
		from.
		'''

		self._s = stream
		self._q = Queue()

		def _populateQueue(stream, queue):
			while True:
				line = stream.readline()
				if line:
					queue.put(line)
				else:
					return


		self._t = Thread(target = _populateQueue, args = (self._s, self._q))
		self._t.daemon = True
		self._t.start()

	def readline(self, timeout = None):
		'''
		Returns the next line from the stream. If no data can be obtained
		within *timeout* seconds, ``None`` is returned.
		'''
		try:
			return self._q.get(block = timeout is not None,
					timeout = timeout)
		except Empty:
			return None


import re
from subprocess import Popen, PIPE
from sys import stderr, stdout
from time import sleep

class ApplicationError(Exception):
	pass


class State:
	'''Fake enumeration for parser states'''
	class Header: pass
	class TestCase: pass
	class Finished: pass


def testStreamToTap(inputStream, tapStream, timeout = 10):
	testStream = NonBlockingStreamReader(inputStream)
	nrOfTests = 0
	currentTestIndex = 1

	state = State.Header
	nextState = State.Header
	while True:
		state = nextState

		if state is State.Header:
			line = testStream.readline(timeout)
			if line is None:
				raise ApplicationError('Timeout when reading test header.')

			stderr.write(line)
			pattern = '^Running tests 1\.\.(\d+)'
			match = re.compile(pattern).match(line)
			if not match:
				message = 'Malformed test header.Expected "{0}" but got "{1}".'.format(pattern, line.strip())
				raise ApplicationError(message)

			tapStream.write('TAP version 13\n')
			tapStream.write('1..{0}\n'.format(match.group(1)))
			nrOfTests = match.group()
			nextState = State.TestCase
			continue

		elif state is State.TestCase:
			line = testStream.readline(timeout)

			if line is None:
				tapStream.write('Bail out! Timeout in test case {0}\n'.format(currentTestIndex))
				message = 'Timeout in test case {0}'.format(currentTestIndex)
				raise ApplicationError(message)

			stderr.write(line)
			if "FINISHED" in line:
				nextState = State.Finished
				continue

			pattern = '^(.+?);\s(\d+);\s\"(.*)\";\s(ok|not\sok|skipped)'
			match = re.compile(pattern).match(line)
			if not match:
				message = 'Malformed test result. Expected "{0}", but got "{1}".'.format(pattern, line)
				raise ApplicationError(message)

			[name, id, comment, result] = match.groups()
			tapStream.write('{0} {1} - {2}\n'.format(result, currentTestIndex, name))
			if comment:
				tapStream.write('  ---\n')
				tapStream.write('  Comment: {0}\n'.format(comment))
				tapStream.write('  ...\n')
			currentTestIndex += 1
			nextState = State.TestCase
			continue
		elif state is State.Finished:
			return


# Main program
import argparse

parser = argparse.ArgumentParser(description='''
	Executes a test application and forwards TAP-compatible output to stdout.
	If a timeout occurs, the test application is killed and this program
	returns 1.
	''')
parser.add_argument(
	"-t",
	"--timeout",
	type = int,
	default = 5,
	help = '''
		wait for TIMEOUT seconds to complete one test case.
		Default is 5 seconds.''')
parser.add_argument("command", type = str,
			help = "Run command and parse output.")
args = parser.parse_args()

try:
	testApp = None
	testApp = Popen(args.command.split(' '),
				stdin = PIPE,
				stdout = PIPE,
				stderr = PIPE,
				shell = False)
	testStreamToTap(testApp.stdout, stdout, args.timeout)
	exit(0)
except IOError as e:
	stderr.write('IO error({0}): {1}\n'.format(e.errno, e.strerror))
	exit(1)
except OSError as e:
	if e.errno is 2:
		message = 'Error: "{0}" is not a valid command.\n'.format(args.command)
	else:
		message = 'OS error({0}): {1}\n'.format(e.errno, e.strerror)
	stderr.write(message)
	exit(1)
except ApplicationError as e:
	stderr.write('Error: {0}\n'.format(e.args[0]))
	exit(1)
except KeyboardInterrupt as e:
	stderr.write('Aborted\n')
	exit(1)
finally:
	if testApp is not None:
		testApp.kill()
