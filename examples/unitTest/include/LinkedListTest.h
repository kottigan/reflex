#ifndef LINKEDLISTTEST_H
#define LINKEDLISTTEST_H

#include "TestSuite.h"

#include <reflex/data_types/ChainLink.h>
#include <reflex/data_types/LinkedList.h>

namespace unitTest
{

/*!
\ingroup unitTest
\brief Tests correct behaviour of reflex::data_types::LinkedList.

 */
class LinkedListTest: public TestSuite
{
public:
    enum TestCases
    {
        IsEmpty = 0,
        BasicTests,
        Append,
        Remove,
        Insert,
        InsertBefore,
        TakeFirst
    };

    LinkedListTest(uint16 id);

private:
    struct SortedElement : public reflex::data_types::ChainLink
    {
        bool lessEqual(const SortedElement& other) { return this <= &other; }
    };

    reflex::data_types::LinkedList<SortedElement*> list;
    SortedElement elements[3];
};

}

#endif // LINKEDLISTTEST_H
