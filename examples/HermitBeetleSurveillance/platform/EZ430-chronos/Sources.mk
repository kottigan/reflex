#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# *	Revision:	 $Id: Sources.mk 1335 2009-12-09 19:58:17Z snu $
# *	Author:		 Sören Höckner
# */

CC_SOURCES_CONTROLLER += \
    lcd/LCD_B.cc

CC_SOURCES_PLATFORM += \
    io/Display.cc \

CC_SOURCES_LIB += \
