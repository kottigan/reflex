/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_EZ430_CHRONOS_BUTTON_H
#define REFLEX_EZ430_CHRONOS_BUTTON_H

#include "reflex/MachineDefinitions.h"
#include "reflex/io/Ports.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/sinks/Sink.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/powerManagement/PowerManageAble.h"

namespace reflex {

namespace buttons {

// enum for convenient button access by name
enum {
     M1 = 2
    ,M2 = 1
    ,BL = 3
    ,S1 = 4
    ,S2 = 0
};

} // ns buttons
/* FIXME: should we include the Button<> class in namespace? */


/* Forward declaration of Button class. The debounce time is used to
 * block interrupts from the button pin for a certain amount of time
 * after an interrupt was signaled (DEBOUNCE_TIME milliseconds).
 * A DEBOUNCE_TIME of zero leads to an efficient implementation without
 * virtual timer.
 */
template <uint8 PIN, Time DEBOUNCE_TIME = 0>
class Button;

template <uint8 PIN>
class Button<PIN, 0> : public PowerManageAble
{
private:
    enum {
        BUTTON_MASK = 0x1 << PIN
    };
public:

    Button();

    ~Button() {}

    /** init
     *  connect sink that is triggered by button
     */
    void init(reflex::Sink0 *sink);

protected:
    //! enables interrupts of button
    virtual void enable();
    //! disables interrupts of button
    virtual void disable();

    data_types::Singleton< mcu::Port2::IVDispatcher > port2IV; /// where the interrupts of port2 are delegated to
};

template <uint8 PIN, Time DEBOUNCE_TIME>
class Button : public Button<PIN, 0>, public Sink0
{
private:
    Sink0* output;
    VirtualTimer debounce_timer;

    /* debouncer class to reenable interrupts after timer event */
    class Debouncer : public Sink0 {
        Button<PIN, DEBOUNCE_TIME>& button;

    public:
        Debouncer(Button<PIN, DEBOUNCE_TIME>& button) : button(button) { }

        void notify() {
            button.enable(); // reenable interrupts for pin
        }
    };

    Debouncer debouncer;

public:

    Button();

    ~Button() {}

    /** init
     *  connect sink that is triggered by button
     */
    void init(reflex::Sink0 *sink);

    void notify();
};

template <uint8 PIN>
Button<PIN, 0>::Button()
    : PowerManageAble(PowerManageAble::PRIMARY)
    , port2IV(mcu::interrupts::PORT2)
{
    this->setSleepMode(mcu::LPM4);
}

template <uint8 PIN>
void Button<PIN, 0>::init(Sink0 *sink)
{
    (*port2IV)[mcu::Port2::Interrupt_traits::localVector(PIN)] = sink;
}

/*! configures the pin on port2 for desired button.
    The direction will be configured as input with enabled internal
    pulldown resistor. The interrupt transition will be configured with
    "low to high".
 */
template <uint8 PIN>
void Button<PIN, 0>::enable()
{
    //select io function for pins
    mcu::Port2()->SEL &= ~BUTTON_MASK;
    //configure pins as input
    mcu::Port2()->DIR &= ~BUTTON_MASK;
    //enable internal pulldowns
    mcu::Port2()->OUT &= ~BUTTON_MASK;
    mcu::Port2()->REN |= BUTTON_MASK;
    //set interrupts on low to high transition
    mcu::Port2()->IES &= ~BUTTON_MASK;
    //reset interrupt flags
    mcu::Port2()->IFG &= ~BUTTON_MASK;
    //enable the interupts for the connected pins
    mcu::Port2()->IE |= BUTTON_MASK;
}

/*! disables interrupt for button
*/
template <uint8 PIN>
void Button<PIN, 0>::disable()
{
    mcu::Port2()->IE &= ~BUTTON_MASK;
}

/** implementation for debounced button */

template <uint8 PIN, Time DEBOUNCE_TIME>
Button<PIN, DEBOUNCE_TIME>::Button() : Button<PIN,0>(),
        debounce_timer(VirtualTimer::ONESHOT),
        debouncer(*this)
{
    debounce_timer.connect_output(&debouncer);
}

template <uint8 PIN, Time DEBOUNCE_TIME>
void Button<PIN, DEBOUNCE_TIME>::init(Sink0 *sink)
{
    (*(this->Button<PIN, 0>::port2IV))[mcu::Port2::Interrupt_traits::localVector(PIN)] = this;
    output = sink;
}

template <uint8 PIN, Time DEBOUNCE_TIME>
void Button<PIN, DEBOUNCE_TIME>::notify()
{
    this->disable(); // disable further interrupts on pin
    debounce_timer.set(DEBOUNCE_TIME); // set debounce time
    // notify output
    if (output)
        output->notify();
}


}// ns reflex

#endif // BUTTON_H
