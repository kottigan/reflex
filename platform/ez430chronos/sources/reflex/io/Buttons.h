/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_EZ430_CHRONOS_BUTTONS_H
#define REFLEX_EZ430_CHRONOS_BUTTONS_H

#include "reflex/io/Ports.h"
#include "reflex/interrupts/InterruptDispatcher.h"
#include "reflex/data_types/Singleton.h"
#include "reflex/sinks/Sink.h"
#include "reflex/powerManagement/PowerManageAble.h"
namespace reflex {


class Buttons
	: public PowerManageAble
{
	enum {
		 PIN_M1=BIT_2
		,PIN_M2=BIT_1
		,PIN_BL=BIT_3
		,PIN_S1=BIT_4
		,PIN_S2=BIT_0
	   ,AllButtons = PIN_M1|PIN_M2|PIN_BL|PIN_S1|PIN_S2
	};
	typedef mcu::Port2::Interrupt_traits Interrupt_traits;
public:
	enum Vector {
		 IV_M1=Interrupt_traits::PIN2
		,IV_M2=Interrupt_traits::PIN1
		,IV_BL=Interrupt_traits::PIN3
		,IV_S1=Interrupt_traits::PIN4
		,IV_S2=Interrupt_traits::PIN0
	};

public:
	Buttons();

	~Buttons() {}

	Sink0*& operator[] (const Vector& i) { return (*port2IV)[ static_cast<const Interrupt_traits::Vector>(i) ];}


	inline void connect_outM1(Sink0 *sink)
	{
		(*port2IV)[ static_cast<const Interrupt_traits::Vector>(IV_M1) ] = sink;
	}

	inline void connect_outM2(Sink0 *sink)
	{
		(*port2IV)[ static_cast<const Interrupt_traits::Vector>(IV_M2) ] = sink;
	}

	inline void connect_outBL(Sink0 *sink)
	{
		(*port2IV)[ static_cast<const Interrupt_traits::Vector>(IV_BL) ] = sink;
	}

	inline void connect_outS1(Sink0 *sink)
	{
		(*port2IV)[ static_cast<const Interrupt_traits::Vector>(IV_S1) ] = sink;
	}

	inline void connect_outS2(Sink0 *sink)
	{
		(*port2IV)[ static_cast<const Interrupt_traits::Vector>(IV_S2) ] = sink;
	}



protected:
	//! enabled interrupts of all buttons
	virtual void enable();
	//! disables interrupts of all buttons
	virtual void disable();

protected:
	//! where the interrupts of port2 are delegated to
	data_types::Singleton< mcu::Port2::IVDispatcher > port2IV;
};


}// ns reflex


#endif // BUTTONS_H
