#ifndef HardwareTimerMilli_h
#define HardwareTimerMilli_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 HardwareTimerMilli
 *
 *	Author:	 Stefan Nuernberger
 *
 *	Description: HardwareTimer in millisecond precision
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/timer/HardwareTimer.h"
#include "reflex/timer/TimerA.h"

namespace reflex {
namespace msp430 {

/**
 * This class is an abstraction of TimerA to be used as an input
 * to a VirtualizedTimer. It assumes TimerA uses ACLK as input
 * source in continuous mode. It downscales the 32khz ACLK to
 * millisecond precision. Additionally it counts the TimerA overflows
 * to offer a local time value of sizeof(Time) bytes.
 *
 */
class HardwareTimerMilli : public HardwareTimer, public Sink0 {
private:
	enum Precision {
		PRECISION_SHIFT = 5, // shift from ACLK to milliseconds
		OVERFLOW_SHIFT = 11, // shift for the overflow counter to local time
		MAX_TIMER_VALUE = MAX_UINT16 >> PRECISION_SHIFT, // largest interval
		SAFE_MINIMUM = 2 // minimum interval where it is safe to set timer directly
	};

	class Counter : public Sink0 {
	public:
		/* constructor */
		Counter() : value(0) {}

		/* notify
		 * implements Sink0 interface
		 */
		void notify();

		/* get
		 * @return Time the current value
		 */
		Time get() {
            return (Time) value;
		}

        /* get64
         * @return uint64 current value in full width
         */
        uint64 get64() {
            return value;
        }

	private:
        uint64 value;
	};

    TimerA *timer; ///< the real hardware timer
    Time delta; ///< time until notify
    Time time; ///< time when the timer was set
    Counter overflows; ///< overflow counter

public:

	/** constructor */
	HardwareTimerMilli();


	/** configureTimer
	 * configure the underlying TimerA instance
	 * @param timer the TimerA instance used by the HardwareTimer
	 */
	void configureTimer(TimerA& timer);

	/** notify
	 * implements Sink0
	 * notifies output
	 */
	void notify();

	/** getNow
	 * @return current counter value (local time)
	 */
	Time getNow();

    /** getTimestamp
     * @return 64 bit counter value in hardware precision
     */
    uint64 getTimestamp();

    /** start
	 * set a timeout until next interrupt and start the timer
	 * @param ticks number of ticks until timer event
	 */
	void start(Time ticks);

	/**
	 * startAt
	 * set a new time interrupt to occur ticks after t0
	 * @param t0 time from where to set the timer
	 * @param ticks number of ticks after t0 when the timer should fire
	 */
	void startAt(Time t0, Time ticks);

	/** stop
	 * stop the hardware timer. The local time counter will still work,
	 * but the alarm counter will be turned off.
	 */
	void stop();

private:

	/** setAlarm
	 * set the alarm counter
	 */
	void setAlarm();
};

} // msp430
} // reflex

#endif

