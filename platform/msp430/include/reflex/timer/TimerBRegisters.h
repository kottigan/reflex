#ifndef TimerBRegisters_h
#define TimerBRegisters_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 TimerBRegisters
 *
 *	Author:	 Carsten Schulze
 *
 *	Description: This are the Registers used to programm TimerB
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

namespace reflex {

namespace timerB {

struct TimerBReg {
	volatile int  TBIV;        //= 0x011e
	volatile char dummy1[0x60];
	volatile int  TBCTL;       //= 0x0180,
	volatile int  TBCCTL0;     //= 0x0182,
	volatile int  TBCCTL1;     //= 0x0184,
	volatile int  TBCCTL2;     //= 0x0186, capture
	volatile int  TBCCTL3;     //= 0x0188,
	volatile int  TBCCTL4;     //= 0x018a,
	volatile int  TBCCTL5;     //= 0x018c,
	volatile int  TBCCTL6;     //= 0x018e,
	volatile int  TBR;         //= 0x0190,
	volatile int  TBCCR0;      //= 0x0192,
	volatile int  TBCCR1;      //= 0x0194,
	volatile int  TBCCR2;      //= 0x0196
	volatile int  TBCCR3;      //= 0x0198
	volatile int  TBCCR4;      //= 0x019a
	volatile int  TBCCR5;      //= 0x019c
	volatile int  TBCCR6;      //= 0x019e
};

enum TIMER_B{
	TBCLGRP     	= 0x6000,
	TBSSELx     	= 0x0300,
	TBSSEL01    	= 0x0100, // ACLK
	TBSSEL10    	= 0x0200, // SMCLK
	TBIE        	= 0x0002,
	MCx         	= 0x0030,
	MC01        	= 0x0010, //Up mode: the timer counts up to TACCR0
	MC10        	= 0x0020, //continue Mode
	TBIFG       	= 0x0001,
	TBCLR       	= 0x0004,
	TBIDx       	= 0x00c0,
	TBID00       	= 0x0000, // DIV 1
	TBID01       	= 0x0040, // DIV 2
	TBID10       	= 0x0080, // DIV 4
	TBID11       	= 0x00c0, // DIV 8
	TBIVx       	= 0x000e,
	TBCNTLx     	= 0x1800, //max TBR = 0xff
	TBCNTL00     	= 0x0000, //max TBR = 0xffff
	TBCNTL01     	= 0x0800, //max TBR = 0xffff
	TBCNTL10     	= 0x1000, //max TBR = 0xffff
	TBCNTL11    	= 0x1800, //max TBR = 0xffff
	CMx          	= 0xc000, //capture on rising and falling edge
	CM10          = 0x8000,	//capture on falling edge
  CM01          = 0x4000,	//capture on rising edge
	SCS          	= 0x0800, // capture synch or asynch to clock
	CCIE         	= 0x0010, //capture/compare interrupt enable
	CAP          	= 0x0100, //capture mode
	COV          	= 0x0020, //capture overflow
	OUTMODx     	= 0x00e0,
	OUTMOD101     = 0x00a0,
	CCIFG        	= 0x0001, //captur/compare interrupt flag
	CCIS0					=	0x1000, //CCIxB //Capture/Compare input select (00=CCIxA)
	CCIS1					= 0x2000,	//GND
	CCISx					= 0x3000	//VCC
	//MAXTIME	= 0xffff
};

} //TimerBReg

} //reflex

#endif
