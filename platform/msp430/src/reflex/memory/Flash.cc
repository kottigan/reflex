/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 */
#include "reflex/MachineDefinitions.h"
#include "reflex/debug/Assert.h"
//#include "reflex/interrupts/InterruptLock.h"
#include "reflex/memory/Flash.h"
#include "reflex/timer/BasicClockRegisters.h" //as
#include "reflex/memory/memcpy.h"

//#include "NodeConfiguration.h"

using namespace reflex;

volatile FlashRegisters* Flash::registers;
uint8 Flash::FNx;
extern "C" bool _interruptsDisable();
extern "C" void _interruptsEnable();
extern "C" void _reset();

Flash::Flash()
{
	registers = (FlashRegisters*)mcu::FLASH_BASE;
  uint16 smclkdiv = basicClockModule::SMCLK_KHZ/FLASHFREQUENCY + 1;
  Assert(basicClockModule::SMCLK_KHZ/smclkdiv >= MINFLASHFREQUENCY)
  Assert(basicClockModule::SMCLK_KHZ/smclkdiv <= MAXFLASHFREQUENCY)
  Assert(smclkdiv < 64)
    //use SMCLK divided by smclkdiv as clock for Flash modul
    registers->FCTL2 = ( FWKEY | FSSEL10 | smclkdiv );
}

bool Flash::erase(caddr_t start, size_t size)
{
	
  _interruptsDisable();
  //align to full segment
  size=size + (SEGMENTSIZE - (size & (SEGMENTSIZE-1)));
  //write once on each segment to erase
  for(unsigned i=0; i<size; i+=SEGMENTSIZE){
    registers->FCTL3 = ( FWKEY );  //unlock flash
    registers->FCTL1 = ( FWKEY | ERASE); //set to segment erase
    start[i] = 0;		//dummy write
    registers->FCTL1 = ( FWKEY );
    registers->FCTL3 = ( FWKEY | LOCK ); // lock flash
  }	
  _interruptsEnable();
  return true;

}

void Flash::read(caddr_t dest, caddr_t src, size_t size)
{
	memcpy(dest,src,size);
}

bool Flash::write(caddr_t src, caddr_t dest, size_t size)
{
 
  char* rpos = src;
  char* wpos = dest;

  while(rpos < src+size) {
    if( !writeByte( wpos, *rpos ) ){
      return false;
    }
    ++rpos;
    ++wpos;
  }
  return true;
}


bool Flash::writeByte(char* pos,char value)
{

  //if value is initial value after erase do nothing
  //this prevents from multiple writing of 0xffff
  if(*pos == value){
    return true;
  }
  _interruptsDisable();
       	
  registers->FCTL3 = ( FWKEY ); //clear Lock
  registers->FCTL1 = ( FWKEY | WRT); //also information mem
  *pos = value;
  registers->FCTL1 = ( FWKEY );
  registers->FCTL3 = ( FWKEY | LOCK ); // Lock again
  _interruptsEnable();
  return ( *pos == value);
}


bool Flash::copy(caddr_t src, caddr_t dest, size_t size)
{ 
  //write is 1->0 is only possible with erase of segment
  bool result = erase(dest,size);
  result &= write(src,dest,size);
  return result;
}





 void Flash::move(caddr_t src, caddr_t dest, size_t size)
 {
  _interruptsDisable();
       
  //align to full segment
  size_t size_upper =size + (SEGMENTSIZE - (size & (SEGMENTSIZE-1)));
  char* rpos = src;
  char* wpos = dest;
	

  //erase and copy segmentwise
  for(unsigned i=0; i<size_upper; i+=SEGMENTSIZE){


    //delete dest segment
    registers->FCTL3 = ( FWKEY );  //unlock flash
    registers->FCTL1 = ( FWKEY | ERASE); //set to segment erase

    dest[i] = 0;
		
    registers->FCTL1 = ( FWKEY );
    registers->FCTL3 = ( FWKEY | LOCK ); // lock flash
	  
    //copy segment 
    while((rpos < src+size) && 
	  (wpos < dest+i+SEGMENTSIZE)){ //i=x*segmentsize+1
	  
      registers->FCTL3 = ( FWKEY ); //clear Lock
      registers->FCTL1 = ( FWKEY | WRT); //also information mem
	    
      *wpos = *rpos;
	    
      registers->FCTL1 = ( FWKEY );
      registers->FCTL3 = ( FWKEY | LOCK ); // Lock again
	    
      ++rpos;
      ++wpos;
	    
    }//while
    
    //delete src segment
    registers->FCTL3 = ( FWKEY );  //unlock flash
    registers->FCTL1 = ( FWKEY | ERASE); //set to segment erase

    src[i] = 0;
		
    registers->FCTL1 = ( FWKEY );
    registers->FCTL3 = ( FWKEY | LOCK ); // lock flash
 
	  
  }//for

  //no interrupt_enable through reset
  _reset();
  
}


