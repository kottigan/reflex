/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	     Carsten Schulze
 *
 *	Description: interrupt functions
 */

.global _interruptsDisable
.global _interruptsEnable

#define GIE 0x0008
.section .notUpdateAble,"ax"

//disables maskable interrupts
_interruptsDisable:
	//return true, if Interrupts where enabled
	mov r2,r15
	dint
	and #GIE,r15
	rrc r15
	nop //delay
	ret


//enables maskable interrupts
_interruptsEnable:
	eint
	ret

.section .text

//calls common interrupt wrapper routine, an gives
//a specific number as parameter for, later identification
//of interrupt
.macro wrapper p
.align 2
.global wrapper_\p
wrapper_\p:
	push r15 //r15 is used from gnu g++ as delivery register
	mov  #\p, r15
	br   #wrapper_body

.endm

.irp	i,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14
wrapper \i
.endr


//generic interrupt wrapper, saves the volatile registers
//see Stefane Carrez sites for procedure conventions
wrapper_body:
	/*Saving the Register ...*/
	push r4
	push r5
	push r6
	push r7
	push r8
	push r9
	push r10
	push r11
	push r12
	push r13
	push r14
	//r15 is allready saved in wrapper_\p

	/*... Saving the Register */

	/**
	 * handle gets param a0 from wrapper_0
	 * call handle
	 */
	call #handle

	/* restore register from stack ... */
	pop r14
	pop r13
	pop r12
	pop r11
	pop r10
	pop r9
	pop r8
	pop r7
	pop r6
	pop r5
	pop r4
	pop r15

	/* ... restore register from stack */

	bic   #0xf0,0(r1) // CPU ON
	reti


.extern _reset_vector__
.section .vectors, "ax", @progbits
	.word wrapper_0	; no source ( or power low)
	.word wrapper_1 ; P2.x
	.word wrapper_2; USART1 TX
	.word wrapper_3; USART1 RX
	.word wrapper_4; P1.x
	.word wrapper_5; Timer_A1, Timer_A2, TAR overflow
	.word wrapper_6; Timer_A0
	.word wrapper_7; ADC12
	.word wrapper_8; USART0 TX
	.word wrapper_9; USART0 RX
	.word wrapper_10; Watchdog/Timer, Timer mode
	.word wrapper_11; Comparator_A
	.word wrapper_12; Timer_B1 to Timer_B6, TBR overflow
	.word wrapper_13; Timer_B0
	.word wrapper_14; NMI, Osc. fault, Flash access violation
	.word _reset_vector__; POR, ext. Reset, Watchdog (_reset_vector__)
