import qbs 1.0

/*
  Meta project file for the REFLEX framework and all of its official
  components.
*/
Project {

    property var platform_packages : {
        return {
            atmega: ["atmega/atmega.qbs"],
            msp430x: [
                "msp430x/msp430x.qbs",
                "msp430x/msp430x_bootloader.qbs"
            ],
            ez430chronos : [
                "msp430x/msp430x.qbs",
                "msp430x/msp430x_bootloader.qbs",
                "ez430chronos/ez430chronos.qbs"
            ],
            omnetpp: ["omnetpp/omnetpp.qbs"],
            posix: ["posix/posix.qbs"]
        };
    }

//    SubProject {
//        inheritProperties : true

//        filePath:  {
//            var platform = qbs.architecture;
//            print("Building REFLEX for platform " + platform + ".");
//            if (!(platform in platform_packages))
//            {
//                 throw "Could not find packages for platform " + platform;
//            }

//            return (platform_packages[platform]);
//        }
//    }

    references: {
        var platform = qbs.architecture;
        print("Building REFLEX for platform " + platform + ".");
        if (!(platform in platform_packages))
        {
             throw "Could not find packages for platform " + platform;
        }
        print("References: " + platform_packages[platform]);
        return (platform_packages[platform]);
    }
}
