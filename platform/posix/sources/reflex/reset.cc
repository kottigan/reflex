/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		Karsten Walther
 */

#include <iostream>
using namespace std;

extern void _interruptsEnable();

/** This function is used for reset after immediate power down on
 *  microcontroller or if Watchdog fires.
 *
 *
 */
extern "C" void _reset(){
	cerr << "!!!!!!!!!!! Reset occured !!!!!!!!!!" << endl;
	while(true);
}
