/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		Reinhard Hemmerling
 *
 *  based on timer implementation in http://www.opengroup.org/onlinepubs/007908799/index.html
 */

#include "reflex/timer/SystemTimer.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/CoreApplication.h"

#include <signal.h>

extern "C" int _isLocked();

using namespace reflex;

// declaration of static components (for linker)
bool SystemTimer::active = false;
struct itimerval SystemTimer::itv;

/** constructor
 * initialize base class and variables
 */
SystemTimer::SystemTimer()
	: InterruptHandler(mcu::interrupts::INTVEC_SYSTEM_TIMER,PowerManageAble::PRIMARY)
{
	/* get system start time */
	timeval now;
	gettimeofday(&now, NULL);
	systemStartTime = (now.tv_sec * 1000) + ((now.tv_usec / 1000) % 1000);
	stoppedAt = 0;

	/* init signal handler */
	struct sigaction act;
	act.sa_handler = sigHandler;
	sigfillset(&act.sa_mask);
	act.sa_flags = 0;
	sigaction(SIGALRM, &act, 0);

	this->setSleepMode(mcu::LINUXYIELD);
}

/** getNow
 * @return current counter value (local time)
 */
Time SystemTimer::getNow() {
	if (active) {
		timeval now;
		gettimeofday(&now, NULL);

		Time msecs = (now.tv_sec * 1000) + ((now.tv_usec / 1000) % 1000);

		return msecs - systemStartTime;
	} else {
		return stoppedAt;
	}
}

/** getTimestamp
 * @return 64 bit counter value in hardware precision
 */
uint64 SystemTimer::getTimestamp()
{
    if (active) {
        timeval now;
        gettimeofday(&now, NULL);

        uint64 tstamp = (now.tv_sec * 1000000) + now.tv_usec;
        return tstamp - ((uint64)systemStartTime * 1000);
    } else {
        return ((uint64)stoppedAt * 1000);
    }
}

/** start
 * set a timeout until next interrupt and start the timer
 * @param ticks number of ticks until timer event
 */
void SystemTimer::start(Time ticks) {
	// prepare timer (oneshot)
	itv.it_interval.tv_sec = 0;
	itv.it_interval.tv_usec = 0;
	itv.it_value.tv_sec = ticks / 1000;
	itv.it_value.tv_usec = (ticks % 1000) * 1000;
	// set new timer value
	setitimer(ITIMER_REAL, &itv, NULL);
}

/**
 * startAt
 * set a new time interrupt to occur ticks after t0
 * @param t0 time from where to set the timer
 * @param ticks number of ticks after t0 when the timer should fire
 */
void SystemTimer::startAt(Time t0, Time ticks) {
	Time now = getNow();
	if ((t0 + ticks) > now)
    {
		start(t0 - now + ticks);
    }
	else
    {
		start(1); // minimum alarm timer interval
    }
}

/** stop
 * stop the hardware timer (alarm)
 * NOTE: this does not stop the local time counter!
 */
void SystemTimer::stop() {
	// prepare timer (off)
	itv.it_interval.tv_sec = 0;
	itv.it_interval.tv_usec = 0;
	itv.it_value = itv.it_interval;
	// set new timer value
	setitimer(ITIMER_REAL, &itv, NULL);
}

void SystemTimer::handle()
{
	// notify output flow primitive
	output->notify();
}

/** enable
 * enable the clock and timer
 */
void SystemTimer::enable()
{
	active = true;
	systemStartTime += getNow() - stoppedAt;
}

/** disable
 * disable the clock and timer
 */
void SystemTimer::disable()
{
	stoppedAt = getNow();
	active = false;
}

void SystemTimer::sigHandler(int sig)
{
	// check if timer is active (started)
	if (active)
	{
		// check if (simulated) interrupts are enabled
		if (!_isLocked())
		{
			// lock interrupts
			InterruptLock lock;
			// simulate timer interrupt
            ::handle(mcu::interrupts::INTVEC_SYSTEM_TIMER);
		} else {
			/* This is a simple implementation of a "pending"
			 * interrupt that is deferred until the interrupts are
			 * activated again. It just sets the itimer to deliver
			 * the signal again in some time ;-)
			 */
			// prepare timer (oneshot)
			itv.it_interval.tv_sec = 0;
			itv.it_interval.tv_usec = 0;
			itv.it_value.tv_sec = 0;
			itv.it_value.tv_usec = DEFER_SIGNAL_USEC;
			// set new timer value
			setitimer(ITIMER_REAL, &itv, NULL);
		}
	}

    (void)sig;
}
