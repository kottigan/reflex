#ifndef STOPTIMER_H
#define STOPTIMER_H

/*
 *	REFLEX -Real-time Event Flow EXecutive
 *
 *	Class(es):	Stoptimer
 *
 *	Author:		Carsten Schulze
 *
 *	Description: Timer for the use with te Stopwatch,
 *                 just for testing how long anything take,
 *                 DO NOT USE AS SYSTEMTIMER
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/timer/Stopwatch.h"

extern "C" unsigned long long readCpuTicks();

namespace reflex {

/**
 *	Timer for the Stopwatch
 *	@author Carsten Schulze
  
 */

class StopTimer 
	: public Stopwatch
{
public:
	/**
	 * Construtcor, initialize the Interrupt
	 */
	StopTimer() : Stopwatch() {};
	
	/**
	 * workaround, the gcc does not find this virtual function,
	 * if it has the same name like the other getTime function
	 * which is implemented here
	 */ 
	virtual Time getTime(Stopwatch::ID id) { return Stopwatch::getTime(id);};
protected :
	/**
	 * the used function from Stopwatch
	 */
	virtual Time getTime()
	{
		unsigned long long t = readCpuTicks();
		return t;
	}
};

}//namespace reflex

#endif
