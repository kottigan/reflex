/*
 * stacktrace.h
 *
 *  Created on: 17.08.2011
 *      Author: sbuechne
 */

#ifndef STACKTRACE_H_
#define STACKTRACE_H_

#ifdef EMULATOR

#include <stdio.h>
#include <signal.h>
#include <execinfo.h>
#include <cxxabi.h>
#include <dlfcn.h>
#include <stdlib.h>

using namespace std;

inline void print_trace( )
{
  using namespace abi;

	enum
	{
		MAX_DEPTH = 100
	};

	void *trace[MAX_DEPTH];

	Dl_info dlinfo;

	int status;
	const char *symname;
	char *demangled;

	int trace_size = backtrace(trace, MAX_DEPTH);

	printf("Call stack:");

	for (int i=0; i<trace_size; ++i)
	{
		if(!dladdr(trace[i], &dlinfo))
			continue;

		symname = dlinfo.dli_sname;

		demangled = __cxa_demangle(symname, NULL, 0, &status);
		if(status == 0 && demangled)
			symname = demangled;

		printf("[%d]: %s\n",i,  symname);

		if (demangled)
			free(demangled);
}

//
//	const size_t max_depth = 100;
//	size_t stack_depth;
//	void *stack_addrs[max_depth];
//	char **stack_strings;
//
//	stack_depth = backtrace(stack_addrs, max_depth);
//	stack_strings = backtrace_symbols(stack_addrs, stack_depth);
//
//	std::cerr << "TRACE:\n";
//
//	for (size_t i = 1; i < stack_depth; i++)
//	{
//		size_t sz = 2000;
//
//		char *function = static_cast<char*>(malloc(sz));
//		char *begin = stack_strings[i], *end = 0;
//
//		for (char *j = stack_strings[i]; *j; ++j)
//		{
//			if (*j == '(')
//			{
//				begin = j;
//			}
//			else if (*j == '+')
//			{
//				end = j;
//			}
//		}
//
//		if (begin && end)
//		{
//			*begin++ = 0;//'';
//			*end = 0;//'';
//
//			int status;
//			char *ret = abi::__cxa_demangle(begin, function, &sz, &status);
//			if (ret)
//			{
//				function = ret;
//			}
//			else
//			{
//				strncpy(function, begin, sz);
//				strncat(function, "()", sz);
//				function[sz-1] = 0;//'';
//			}
//
//			std::cerr << "TRACE: "<< stack_strings[i] << ": " << function << std::endl;
////						fprintf(out, "    %s:%s\n", stack.strings[i], function);
//		}
//		else
//		{
//			std::cerr << "TRACE: " <<stack_strings[i] << std::endl;
//		}
//		free(function);
//
//	}
//	free(stack_strings); // malloc()ed by backtrace_symbols
//
//	std::cerr << "\n";
}

#define EV_STACKTRACE print_trace()

#endif

#endif /* STACKTRACE_H_ */
