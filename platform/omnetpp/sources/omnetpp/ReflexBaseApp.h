#ifndef __REFLEX_REFLEXBASEAPP_H_
#define __REFLEX_REFLEXBASEAPP_H_

#include "reflex/CoreApplication.h"

/*!
The ReflexBaseApp class

This class is deprecated and its functionality has been replaced by
reflex::omnetpp::CoreApplication.
 */
class ReflexBaseApp : public reflex::omnetpp::CoreApplication
{
public:
    static cSimpleModule* currentModule();
};

inline cSimpleModule* ReflexBaseApp::currentModule()
{
    return instance();
}

#endif
