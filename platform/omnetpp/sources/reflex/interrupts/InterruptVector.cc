/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		Reinhard Hemmerling
 */

#include "reflex/CoreApplication.h"
#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/interrupts/InterruptVector.h"

extern "C" void wrapper_0();

using namespace reflex;

InterruptVector InterruptVector::itable[ITABLE_SIZE];

InterruptVector::InterruptVector()
{
}

void InterruptVector::execute()
{
	omnetpp::CoreApplication::instance()->guardian()->handle(vector);
}

InterruptVector& InterruptVector::getInterruptVector(int num)
{
	return itable[num];
}


void InterruptVector::operator= (char* targetVector)
{
   	itable[(int)this].vector = (int)(targetVector - (unsigned long)wrapper_0);
}


