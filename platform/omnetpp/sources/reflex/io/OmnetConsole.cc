/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Soeren Hoeckner
 */

#include "reflex/io/OmnetConsole.h"
#include "reflex/memory/Buffer.h"
#include <cenvir.h>
#include <iostream>

using namespace reflex;

void OmnetConsole::assign(Buffer* buffer)
{
	uint8* pos = (uint8*)buffer->getStart();
	unsigned size = buffer->getLength();
	while(size--)
	{
		ev << *(pos);
		std::cout << *(pos++);
	}
	std::cout<<std::flush;
	buffer->downRef();

}


