/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	HardwareTimerMilli
 
 *	Author:		Soeren Hoeckner
 * */

#include "reflex/timer/HardwareTimerMilli.h"
#include "reflex/interrupts/InterruptLock.h"

//#include "NodeConfiguration.h"

using namespace reflex;
//
HardwareTimerMilli::HardwareTimerMilli()
	: PowerManageAble(PowerManageAble::PRIMARY)
	, hwTimer()
{
//std::cerr<<__PRETTY_FUNCTION__<<std::endl;
	this->setSleepMode(mcu::LINUXIDLE);
	hwTimer.init(this);
}

void HardwareTimerMilli::notify() {
//std::cerr<<__PRETTY_FUNCTION__<<std::endl;
	this->zeroTime += delta;
	delta=0;
	if(output) { output->notify();}
}

//update system time and return it
Time HardwareTimerMilli::getNow() {
//std::cerr<<__PRETTY_FUNCTION__<<std::endl;
	return hwTimer.getTime(); //returns the simtime
}

/** getTimestamp
 * @return 64 bit counter value in hardware precision
 */
uint64 HardwareTimerMilli::getTimestamp()
{
    return hwTimer.getTimestamp(); //returns the simtime
}

void HardwareTimerMilli::start(Time deltaTime) {
//std::cerr<<__PRETTY_FUNCTION__<<std::endl;
	startAt(getNow(), deltaTime); //update systemtime and set hwtimer
}

void HardwareTimerMilli::startAt(Time t0, Time deltaTime) {
//std::cerr<<__PRETTY_FUNCTION__<<std::endl;
	this->zeroTime = t0;
	this->delta = deltaTime;
	hwTimer.reStart(delta);
}

void HardwareTimerMilli::stop() {
//std::cerr<<__PRETTY_FUNCTION__<<std::endl;
	hwTimer.stop();
}




void HardwareTimerMilli::enable() {
//std::cerr<<__PRETTY_FUNCTION__<<std::endl;
}

void HardwareTimerMilli::disable() {
	hwTimer.disable();
}
