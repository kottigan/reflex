#ifndef SYSTEMTIMER_h
#define SYSTEMTIMER_h
/**
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	SystemTimer, TimerListener
 *
 *	Author:		S�ren H�ckner
 *
 *	Description:	Timer abstraction for OMNet platform
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include <simtime.h>

#include "reflex/sinks/Sink.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/types.h"

class cMessage;

namespace reflex
{

/**
 * The SystemTimer in the guest system emulates a hardware clock by
 * registering as signal handler and letting the system signal when
 * the next tick occurs.
 * @author S�ren H�ckner
 * @version 1.0
 */
class SystemTimer
		: public InterruptHandler
{

public:

	/**	Constructor
	 *
	 */
	SystemTimer();

	/**	Destructor
	 *
	 */
	virtual ~SystemTimer();

	/**	set consumer object
	 *
	 *	@param output notified on timeout
	 */
	void init(Sink0 *output);

	/**	set tickspeed
	 *
	 *	@param ms timeout in milliseconds
	 */
	void setTickSpeed(Time ms);

	/**	set tickspeed and start timer
	 *
	 *	@param ms timeout in milliseconds
	 */
	void reStart(Time ms);

	const Time getTime() const;
    const uint64 getTimestamp() const;

	/**	start timer
	 *
	 */
	void start();

	/**	stop timer
	 *
	 */
	void stop();

	/**	start timer
	 *
	 */
	virtual void enable();

	/**	stop timer
	 *
	 */
	virtual void disable();
protected:

	/**	Timer handler called by the interrupt.
	 *	It is responsible for resetting the hardware to allow further interrupts.
	 *
	 */
	virtual void handle();



	Sink0 *output;			///< consumer


private :
	Time tickSpeed;			///< interval in ms
	double reflexTimeScale;
	SimTime simInterval;		///< interval in simtime
	bool active;			///< power switch of the timer
	cMessage *timeoutEvent;	/** Selfmessage */

};

} //namespace
#endif
