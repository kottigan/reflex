import qbs 1.0

Project {
    name : "Msp430x Wireless Bootloader"

    condition : project.msp430x_bootloader_enabled == true

    Product {
        name : "msp430x-bootloader"
        type : "application"

        Depends { name : "reflex.msp430x" }

        property int bootloaderaddress : reflex.msp430x.bootloader_address
        property int channel : reflex.msp430x.bootloader_channel
        readonly property int nodeInfoAddress : reflex.msp430x.nodeinfoaddress
        readonly property int firmwareAddress : reflex.msp430x.update_firmwareaddress
        readonly property bool noVersionInformation : true

        Depends { name: "cpp" }
        Depends { name : "platform-essentials" }
        Depends { name : "ez430chronos" }
        Depends { name : "core" }

        Group {
            prefix : "sources/bootloader/"
            files : [
                "bsl.cc",
                "cpu.cc",
                "display.cc",
                "flash.cc",
                "radio.cc",
                "timer.cc",
                "RadioConfig.cc",
                "ReflexDisplay.cc"
            ]
        }

        Group {
            fileTagsFilter: "application"
            qbs.install : true
            qbs.installDir : "bin"
        }

        Group {
            prefix : "utils/"
            files : "bootloader/uploader.py"
            qbs.install: true
            qbs.installDir : "utils/"
        }

        cpp.defines : [
            "CHANNEL="
            + channel,
            "NODEINFOADDRESS=0x"
            + nodeInfoAddress.toString(16),
            "FIRMWAREADDRESS=0x"
            + firmwareAddress.toString(16)
        ]

        cpp.includePaths : "sources/"
        cpp.optimization : "small"

        cpp.linkerFlags : base.concat([
            "-Ttext=0x" + bootloaderaddress.toString(16)
        ])
    }
}
