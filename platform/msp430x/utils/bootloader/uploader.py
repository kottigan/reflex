#!/usr/bin/env python

# Author: Hannes Menzel, 2013
# uploader.py
# tested on pyhton version 2.6
#
# performs a wireless update based upon a  given hexfile

# usage (all fields needed):
# python2.6 uploader.py yourImage.hex startAddress versionNumber
# yourImage.hex: the hexfile, which has to be copied onto the wireless device
# startAddress: the start address in your hex-file where the __codeStart__ -Label points to
# versionNumber: the version number of the update
#


try:
    import serial  # Python2
except ImportError:
    from serial3 import *  # Python3
import array
import csv
import json
import os
import sys
import platform
from struct import *
from time import *
from intelhex import IntelHex

def startAccessPoint():
    return array.array('B', [0xFF, 0x07, 0x03]).tostring()# startMarker, Command, PacketSize

def stopAccessPoint():
    return array.array('B', [0xFF, 0x09, 0x03]).tostring()

def switchChannel(channel):
    return array.array('B', [0xFF, 0x074, 0x04, channel]).tostring()# startMarker, Command, PacketSize, channelNo

def startTX():
    return array.array('B', [0xFF, 0x75, 0x03]).tostring()# startMarker, Command, PacketSize

def stopTX():
    return array.array('B', [0xFF, 0x76, 0x03]).tostring()# startMarker, Command, PacketSize
    
def changeTXPower(power):
    return array.array('B', [0xFF, 0x77, 0x04, power]).tostring()# startMarker, Command, PacketSize, power

def sendData(data):
    x = array.array('B', [0xFF, 0x78, 0x03 + len(data)])# startMarker, Command, PacketSize, data (n bytes)
    for i in range(len(data)):
        x.append(data[i])
    return x.tostring()


def startOwnProtocol():
    return array.array('B', [0xFF, 0x7A, 0x03]).tostring()# startMarker, Command, PacketSize, 

def stopOwnProtocol():
    return array.array('B', [0xFF, 0x7B, 0x03]).tostring()# startMarker, Command, PacketSize, 

def getData():
    return array.array('B', [0xFF, 0x79, 0x03]).tostring()# startMarker, Command, PacketSize, 

def checkCommand(text):
    #time.sleep(0.01)
    sleep(0.01)
    erg = ser.read(100)
    if len(erg) >= 2:
        if erg[1] == 0x06:
            print (text, " ok: ", len(erg))
        else:
            print (text, " failed", "error code: ", erg[1])
    else:
        print (text, " failed", "length: ", len(erg))


def waitForMsg():
    MSGlength = 0
    while MSGlength < 5:
        ser.write(getData())
        TempData = ser.read(100)   
        MSGlength = len(TempData)
    return TempData

def interpreteMsg(msg):
    global state
    global packetsReceived
    global Messages
    global numberOfPackets
    global timebase
   

try:
    if (len(sys.argv) != 2):
        sys.stderr.write("Error: expecting a json file containing firmware information.\n")
        sys.exit(1)

    fileName = sys.argv[1]
    cmd_hexFile = ''
    cmd_firmwareAddress = 0
    cmd_softwareVersion = ''
    try:
        with open(fileName) as data_file:
            data = json.load(data_file)
            cmd_hexFile = os.path.join(os.path.dirname(fileName), data["file"])
            cmd_firmwareAddress = data["address"]
            cmd_softwareVersion = data["version"]

    except IOError as e:
            sys.stderr.write("Error reading file '{0}': {1}.\n".format(fileName, e.strerror))
            sys.exit(1)

    #############################################################################
    ## Read configuration and load hex file
    #############################################################################
    startAddress = cmd_firmwareAddress
    LAST_ADDRESS = 0xffff
    override = 0
    version = int(cmd_softwareVersion)
    progID = 1
    update_type = 1

    hexFile = IntelHex()
    try:
        hexFile.loadfile(cmd_hexFile, format='hex')
    except IOError as e:
        sys.stderr.write("Error reading file '{0}': {1}.\n".format(cmd_hexFile, e.strerror))
        sys.exit(1)

    hexFile.start_addr = {'IP': startAddress}

    sys.stdout.write("Firmware image: '{0}'\n".format(cmd_hexFile))
    sys.stdout.write("Firmware version: '{0}'\n".format(cmd_softwareVersion))
    sys.stdout.write("Destination address: 0x{0:4x}\n".format(cmd_firmwareAddress))


    #############################################################################
    ##  Prepare datagrams for later sending
    #############################################################################
    PAYLOAD_SIZE = 20
    beaconPacket = array.array('B')

    packetsCount = (LAST_ADDRESS - startAddress) / PAYLOAD_SIZE
    remainder = (LAST_ADDRESS - startAddress) % PAYLOAD_SIZE
    if remainder != 0:
        packetsCount += 1

    def buildUpdateHeader(update_type, progID, res, override, version, reserved):
        progID = (progID << 2) | (res << 1) | override
        return array.array('B', [update_type, progID, version, reserved])


    # Create an update beacon packet with meta information
    beaconPacket = buildUpdateHeader(
            update_type = 2,
            progID = 1,
            res = 0, # whatever this is
            override = override,
            version = version,
            reserved = 0
        ) + array.array(
            'B',
            [
                packetsCount & 0xff,
                (packetsCount >> 8) & 0xff,
                remainder,
                0x00
            ]
        )

    # Create update data packets
    dataHeader = buildUpdateHeader(
        3,
        progID = 1,
        res = 0,
        override = override,
        version = version,
        reserved = 0
    )
    repeatHeader = buildUpdateHeader(
        5,
        progID = 1,
        res = 0,
        override = override,
        version = version,
        reserved = 0
    )

    dataPackets = []
    addr = startAddress
    i = 0
    while(addr <= LAST_ADDRESS):
        seqNo = array.array('B', [i & 0xff, (i >> 8) & 0xff])

        # Count consecutive empty chunks of PAYLOAD_SIZE bytes in hex file
        emptyChunks = 0
        while all(x==0xff for x in hexFile.tobinarray(start=addr, size = PAYLOAD_SIZE)) and (addr <= LAST_ADDRESS):
            emptyChunks += 1
            addr += PAYLOAD_SIZE

        if (emptyChunks > 0):
            # print('{0:04d} Skipping {1} empty bytes'.format(i, emptyPackets * dataSize))
            payload = array.array('B', [emptyChunks & 0xff, (emptyChunks >> 8) & 0xff])
            dataPackets.append(repeatHeader + seqNo + payload)
            i += emptyChunks
        else:
            payload = hexFile.tobinarray(start=addr, size = PAYLOAD_SIZE)
            text = ' '.join('{0:02x}'.format(x) for x in payload)
            # print('{0:04d} ({1:04x}): {2}'.format(i, addr, text))
            dataPackets.append(dataHeader + seqNo + payload)
            addr += PAYLOAD_SIZE
            i += 1



    #############################################################################
    ## Test interface to access point
    #############################################################################
    done = 0

    def destPort():
        global done
        ser.write(startAccessPoint())
        tester = ser.read(100)
        if len(tester)  == 0 :
            done = 0
        else:
            done = 1

    if platform.system() == "Darwin":
        try:
            ser = serial.Serial("/dev/tty.usbmodem001",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass
        if done == 0:
            try:
                ser = serial.Serial("/dev/tty.usbmodem002",115200,timeout=0.0025)
                #done = 1
                destPort()
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial("/dev/tty.usbmodem003",115200,timeout=0.0025)
                #done = 1
                destPort()
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial("/dev/tty.usbmodem004",115200,timeout=0.0025)
                #done = 1
                destPort()
            except serial.serialutil.SerialException:
                pass
    elif platform.system() == "Linux":
        try:
            ser = serial.Serial("/dev/ttyACM0",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass
        if done == 0:
            try:
                ser = serial.Serial("/dev/ttyACM1",115200,timeout=0.0025)
                #done = 1
                destPort()
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial("/dev/ttyACM2",115200,timeout=0.0025)
                #done = 1
                destPort()
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial("/dev/ttyACM3",115200,timeout=0.0025)
                #done = 1
                destPort()
            except serial.serialutil.SerialException:
                pass
    elif platform.system() == "Windows":
        try:
            ser = serial.Serial(0,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass
        if done == 0:
            try:
                ser = serial.Serial(1,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(2,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(3,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(4,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(5,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(6,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(7,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(8,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(9,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(10,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(11,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(12,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(13,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(14,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(15,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(16,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(17,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(18,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(19,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass
        if done == 0:
            try:
                ser = serial.Serial(20,115200,timeout=0.0025)
                destPort()
                #done = 1
            except serial.serialutil.SerialException:
                pass

    if done == 0:
        sys.stderr.write("Error: No access point found.\n")
        sys.exit(1)


    ##################################
    ## Send data via access point
    ##################################
    #Start access point
    ser.write(startAccessPoint())
    ser.write(startOwnProtocol())

    # Send data stream interleaved with beacon packets
    while True:
        i = 0
        for i in range(0, len(dataPackets)):
            if (i % 8 == 0):
                ser.write(sendData(beaconPacket))
                sleep(0.05)
                ser.read(100)

            sys.stdout.write("\rSending packet {0:04d} of {1:d}".format(i, len(dataPackets)))
            sys.stdout.flush()
            ser.write(sendData(dataPackets[i]))
            sleep(0.05)
            ser.read(100)
            i += 1

    sys.stdout.write("Finished.\n")
    sys.exit(0)

except KeyboardInterrupt:
    sys.stderr.write("\nAborted by user\n")
    sys.exit(1)
