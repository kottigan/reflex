#ifndef RADIO_H
#define RADIO_H

#include <reflex/types.h>

////////////////////////////////////////////////////////////////////////////////////////////////////////
// radio driver declarations
/////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * reset the radio
 */
void radio_reset();

/**
 * configures the radio
 * @param radioCfg pointer to a 2-D array containing pairs of addresses of configuration registers and values, which
 * have to be write into
 * @param pairs number of pairs
 * @param channel the used channel when update packet was received in none-update phase
 *
 * @return 0 -> configuration failed, else configuration was successful
 */
uint8 radio_config(const uint8 radioCfg[][2], uint8 pairs, uint8 channel);


/**
 * transmit data via radio
 * @param data pointer to the data
 * @param len size of the data array
 *
 * @return the transmitted data
 */
uint8 u_radio_sendData(uint8* data, uint8 len);

bool radio_hasData();

/**
 * reads data form radio core into the buffer buf
 *
 * @param buf the buffer, where the data should be stored
 * @param len the length of the buffer
 *
 *
 * @return size of the transferred data
 */
uint8 radio_readData(uint8* buf, uint8 len/*, uint16& statusByte_1_2*/);

/**
 * switch the radio module on
 */
void radio_setReceiveDisabled();
void radio_setReceiveEnabled();
void radio_powerDown();

#endif // RADIO_H
