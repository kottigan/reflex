#ifndef RADIOCONFIG_H_
#define RADIOCONFIG_H_

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Author:		 Hannes Menzel
 *
 *	Description: add here your description
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#define SMARTRF_SETTING_CHANNR     0x00

#define SMARTRF_SETTING_MCSM0      0x18
#define SMARTRF_SETTING_PKTLEN     0xFF
#define SMARTRF_SETTING_PKTCTRL0   0x05
#define SMARTRF_SETTING_FIFOTHR    0x07
#define SMARTRF_SETTING_FSCTRL1    0x08
#define SMARTRF_SETTING_FSCTRL0    0x00
#define SMARTRF_SETTING_FREQ2      0x21
#define SMARTRF_SETTING_FREQ1      0x71
#define SMARTRF_SETTING_FREQ0      0x7A
#define SMARTRF_SETTING_MDMCFG4    0x7B
#define SMARTRF_SETTING_MDMCFG3    0x83
#define SMARTRF_SETTING_MDMCFG2    0x13
#define SMARTRF_SETTING_MDMCFG1    0x22
#define SMARTRF_SETTING_MDMCFG0    0xF8
#define SMARTRF_SETTING_DEVIATN    0x42
#define SMARTRF_SETTING_FOCCFG     0x1D
#define SMARTRF_SETTING_BSCFG      0x1C
#define SMARTRF_SETTING_AGCCTRL2   0xC7
#define SMARTRF_SETTING_AGCCTRL1   0x00
#define SMARTRF_SETTING_AGCCTRL0   0xB2
#define SMARTRF_SETTING_FREND1     0xB6
#define SMARTRF_SETTING_FREND0     0x10
#define SMARTRF_SETTING_FSCAL3     0xEA
#define SMARTRF_SETTING_FSCAL2     0x2A
#define SMARTRF_SETTING_FSCAL1     0x00
#define SMARTRF_SETTING_FSCAL0     0x1F
#define SMARTRF_SETTING_TEST2      0x81
#define SMARTRF_SETTING_TEST1      0x35
#define SMARTRF_SETTING_TEST0      0x09
//reengineered values! working, reception from original is possible
#define MRFI_SETTING_IOCFG0     0x1B
#define MRFI_SETTING_IOCFG1     0x1E
#define MRFI_SETTING_MCSM1      0x3C
#define MRFI_SETTING_MCSM0      0x10
#define MRFI_SETTING_PKTLEN     0x3D
#define MRFI_SETTING_PKTCTRL1   0x04 /* append mode enabled*/
#define MRFI_SETTING_PKTCTRL0   0x05 /* CRC enable + variable length*/
#define MRFI_SETTING_FIFOTHR    0x47


#ifdef __cplusplus
    #define sfrb_(x,x_) \
	    extern "C" volatile unsigned char x asm(#x_)

    #define sfrw_(x,x_) \
	    extern "C" volatile unsigned int x asm(#x_)

#if defined(__MSP430X__) || defined(__MSP430X2__)
    #define sfra_(x,x_) \
	    extern "C" volatile unsigned long int x asm(#x_)
#endif
#else //__cplusplus
    #define sfrb_(x,x_) \
	    volatile unsigned char x asm(#x_)

    #define sfrw_(x,x_) \
	    volatile unsigned int x asm(#x_)

#if defined(__MSP430X__) || defined(__MSP430X2__)
    #define sfra_(x,x_) \
	    volatile unsigned long int x asm(#x_)
#endif
#endif //__cplusplus

#define sfrb(x,x_) sfrb_(x,x_)

#define sfrw(x,x_) sfrw_(x,x_)

#if defined(__MSP430X__) ||defined(__MSP430X2__)
#define sfra(x,x_) sfra_(x,x_)
#endif

#define __MSP430_CC1101_BASE__	0x0f00


#define RF1AIFCTL0_             __MSP430_CC1101_BASE__ + 0x00  /* Radio interface control register 0 */
sfrw(RF1AIFCTL0, RF1AIFCTL0_);
#define RF1AIFCTL0_L_           __MSP430_CC1101_BASE__ + 0x00
sfrb(RF1AIFCTL0_L, RF1AIFCTL0_L_);
#define RF1AIFCTL0_H_           __MSP430_CC1101_BASE__ + 0x01
sfrb(RF1AIFCTL0_H, RF1AIFCTL0_H_);
#define RF1AIFCTL1_             __MSP430_CC1101_BASE__ + 0x02  /* Radio interface control register 1 */
sfrw(RF1AIFCTL1, RF1AIFCTL1_);
#define RF1AIFCTL1_L_           __MSP430_CC1101_BASE__ + 0x02
sfrb(RF1AIFCTL1_L, RF1AIFCTL1_L_);
#define RF1AIFCTL1_H_           __MSP430_CC1101_BASE__ + 0x03
sfrb(RF1AIFCTL1_H, RF1AIFCTL1_H_);
#define  RF1AIFIFG              RF1AIFCTL1_L   /* Radio interface interrupt flag register */
#define  RF1AIFIE               RF1AIFCTL1_H   /* Radio interface interrupt enable register */
#define RF1AIFCTL2_             __MSP430_CC1101_BASE__ + 0x04  /* (Radio interface control register 2) */
sfrw(RF1AIFCTL2, RF1AIFCTL2_);
#define RF1AIFCTL2_L_           __MSP430_CC1101_BASE__ + 0x04
sfrb(RF1AIFCTL2_L, RF1AIFCTL2_L_);
#define RF1AIFCTL2_H_           __MSP430_CC1101_BASE__ + 0x05
sfrb(RF1AIFCTL2_H, RF1AIFCTL2_H_);
#define RF1AIFERR_              __MSP430_CC1101_BASE__ + 0x06  /* Radio interface error flag register */
sfrw(RF1AIFERR, RF1AIFERR_);
#define RF1AIFERR_L_            __MSP430_CC1101_BASE__ + 0x06
sfrb(RF1AIFERR_L, RF1AIFERR_L_);
#define RF1AIFERR_H_            __MSP430_CC1101_BASE__ + 0x07
sfrb(RF1AIFERR_H, RF1AIFERR_H_);
#define RF1AIFERRV_             __MSP430_CC1101_BASE__ + 0x0c  /* Radio interface error vector word register */
sfrw(RF1AIFERRV, RF1AIFERRV_);
#define RF1AIFERRV_L_           __MSP430_CC1101_BASE__ + 0x0c
sfrb(RF1AIFERRV_L, RF1AIFERRV_L_);
#define RF1AIFERRV_H_           __MSP430_CC1101_BASE__ + 0x0d
sfrb(RF1AIFERRV_H, RF1AIFERRV_H_);
#define RF1AIFIV_               __MSP430_CC1101_BASE__ + 0x0e  /* Radio interface interrupt vector word register */
sfrw(RF1AIFIV, RF1AIFIV_);
#define RF1AIFIV_L_             __MSP430_CC1101_BASE__ + 0x0e
sfrb(RF1AIFIV_L, RF1AIFIV_L_);
#define RF1AIFIV_H_             __MSP430_CC1101_BASE__ + 0x0f
sfrb(RF1AIFIV_H, RF1AIFIV_H_);
#define RF1AINSTRW_             __MSP430_CC1101_BASE__ + 0x10  /* Radio instruction word register */
sfrw(RF1AINSTRW, RF1AINSTRW_);
#define RF1AINSTRW_L_           __MSP430_CC1101_BASE__ + 0x10
sfrb(RF1AINSTRW_L, RF1AINSTRW_L_);
#define RF1AINSTRW_H_           __MSP430_CC1101_BASE__ + 0x11
sfrb(RF1AINSTRW_H, RF1AINSTRW_H_);
#define  RF1ADINB               RF1AINSTRW_L   /* Radio instruction byte register */
#define  RF1AINSTRB             RF1AINSTRW_H   /* Radio byte data in register */
#define RF1AINSTR1W_            __MSP430_CC1101_BASE__ + 0x12  /* Radio instruction 1-byte register with autoread */
sfrw(RF1AINSTR1W, RF1AINSTR1W_);
#define RF1AINSTR1W_L_         __MSP430_CC1101_BASE__ + 0x12
sfrb(RF1AINSTR1W_L, RF1AINSTR1W_L_);
#define RF1AINSTR1W_H_         __MSP430_CC1101_BASE__ + 0x13
sfrb(RF1AINSTR1W_H, RF1AINSTR1W_H_);
#define  RF1AINSTR1B           RF1AINSTR1W_H  /* Radio instruction 1-byte register with autoread */
#define RF1AINSTR2W_           __MSP430_CC1101_BASE__ + 0x14  /* Radio instruction 2-byte register with autoread */
sfrw(RF1AINSTR2W, RF1AINSTR2W_);
#define RF1AINSTR2W_L_         __MSP430_CC1101_BASE__ + 0x14
sfrb(RF1AINSTR2W_L, RF1AINSTR2W_L_);
#define RF1AINSTR2W_H_         __MSP430_CC1101_BASE__ + 0x15
sfrb(RF1AINSTR2W_H, RF1AINSTR2W_H_);
#define  RF1AINSTR2B           RF1AINSTR1W_H  /* Radio instruction 2-byte register with autoread */
#define RF1ADINW_              __MSP430_CC1101_BASE__ + 0x16  /* Radio word data in register */
sfrw(RF1ADINW, RF1ADINW_);
#define RF1ADINW_L_            __MSP430_CC1101_BASE__ + 0x16
sfrb(RF1ADINW_L, RF1ADINW_L_);
#define RF1ADINW_H_            __MSP430_CC1101_BASE__ + 0x17
sfrb(RF1ADINW_H, RF1ADINW_H_);

#define RF1ASTAT0W_            __MSP430_CC1101_BASE__ + 0x20  /* Radio status word register without auto-read */
sfrw(RF1ASTAT0W, RF1ASTAT0W_);
#define RF1ASTAT0W_L_          __MSP430_CC1101_BASE__ + 0x20
sfrb(RF1ASTAT0W_L, RF1ASTAT0W_L_);
#define RF1ASTAT0W_H_          __MSP430_CC1101_BASE__ + 0x21
sfrb(RF1ASTAT0W_H, RF1ASTAT0W_H_);
#define  RF1ADOUT0B            RF1ASTAT0W_L   /* Radio byte data out register without auto-read */
#define  RF1ASTAT0B            RF1ASTAT0W_H   /* Radio status byte register without auto-read */
#define  RF1ASTATW             RF1ASTAT0W     /* Radio status word register without auto-read */
#define  RF1ADOUTB             RF1ASTAT0W_L   /* Radio byte data out register without auto-read */
#define  RF1ASTATB             RF1ASTAT0W_H   /* Radio status byte register without auto-read */
#define RF1ASTAT1W_            __MSP430_CC1101_BASE__ + 0x22  /* Radio status word register with 1-byte auto-read */
sfrw(RF1ASTAT1W, RF1ASTAT1W_);
#define RF1ASTAT1W_L_          __MSP430_CC1101_BASE__ + 0x22
sfrb(RF1ASTAT1W_L, RF1ASTAT1W_L_);
#define RF1ASTAT1W_H_          __MSP430_CC1101_BASE__ + 0x23
sfrb(RF1ASTAT1W_H, RF1ASTAT1W_H_);
#define  RF1ADOUT1B            RF1ASTAT1W_L   /* Radio byte data out register with 1-byte auto-read */
#define  RF1ASTAT1B            RF1ASTAT1W_H   /* Radio status byte register with 1-byte auto-read */
#define RF1ASTAT2W_            __MSP430_CC1101_BASE__ + 0x24  /* Radio status word register with 2-byte auto-read */
sfrw(RF1ASTAT2W, RF1ASTAT2W_);
#define RF1ASTAT2W_L_          __MSP430_CC1101_BASE__ + 0x24
sfrb(RF1ASTAT2W_L, RF1ASTAT2W_L_);
#define RF1ASTAT2W_H_          __MSP430_CC1101_BASE__ + 0x25
sfrb(RF1ASTAT2W_H, RF1ASTAT2W_H_);
#define  RF1ADOUT2B            RF1ASTAT2W_L   /* Radio byte data out register with 2-byte auto-read */
#define  RF1ASTAT2B            RF1ASTAT2W_H   /* Radio status byte register with 2-byte auto-read */
#define RF1ADOUT0W_            __MSP430_CC1101_BASE__ + 0x28  /* Radio core word data out register without auto-read */
sfrw(RF1ADOUT0W, RF1ADOUT0W_);
#define RF1ADOUT0W_L_          __MSP430_CC1101_BASE__ + 0x28
sfrb(RF1ADOUT0W_L, RF1ADOUT0W_L_);
#define RF1ADOUT0W_H_          __MSP430_CC1101_BASE__ + 0x29
sfrb(RF1ADOUT0W_H, RF1ADOUT0W_H_);
#define  RF1ADOUTW             RF1ADOUT0W     /* Radio core word data out register without auto-read */
#define  RF1ADOUTW_L           RF1ADOUT0W_L   /* Radio core word data out register without auto-read */
#define  RF1ADOUTW_H           RF1ADOUT0W_H   /* Radio core word data out register without auto-read */
#define RF1ADOUT1W_            __MSP430_CC1101_BASE__ + 0x2a  /* Radio core word data out register with 1-byte auto-read */
sfrw(RF1ADOUT1W, RF1ADOUT1W_);
#define RF1ADOUT1W_L_          __MSP430_CC1101_BASE__ + 0x2a
sfrb(RF1ADOUT1W_L, RF1ADOUT1W_L_);
#define RF1ADOUT1W_H_          __MSP430_CC1101_BASE__ + 0x2b
sfrb(RF1ADOUT1W_H, RF1ADOUT1W_H_);
#define RF1ADOUT2W_            __MSP430_CC1101_BASE__ + 0x2c  /* Radio core word data out register with 2-byte auto-read */
sfrw(RF1ADOUT2W, RF1ADOUT2W_);
#define RF1ADOUT2W_L_          __MSP430_CC1101_BASE__ + 0x2c
sfrb(RF1ADOUT2W_L, RF1ADOUT2W_L_);
#define RF1ADOUT2W_H_          __MSP430_CC1101_BASE__ + 0x2d
sfrb(RF1ADOUT2W_H, RF1ADOUT2W_H_);
#define RF1AIN_                __MSP430_CC1101_BASE__ + 0x30  /* Radio core signal input register */
sfrw(RF1AIN, RF1AIN_);
#define RF1AIN_L_              __MSP430_CC1101_BASE__ + 0x30
sfrb(RF1AIN_L, RF1AIN_L_);
#define RF1AIN_H_              __MSP430_CC1101_BASE__ + 0x31
sfrb(RF1AIN_H, RF1AIN_H_);
#define RF1AIFG_               __MSP430_CC1101_BASE__ + 0x32  /* Radio core interrupt flag register */
sfrw(RF1AIFG, RF1AIFG_);
#define RF1AIFG_L_             __MSP430_CC1101_BASE__ + 0x32
sfrb(RF1AIFG_L, RF1AIFG_L_);
#define RF1AIFG_H_             __MSP430_CC1101_BASE__ + 0x33
sfrb(RF1AIFG_H, RF1AIFG_H_);
#define RF1AIES_               __MSP430_CC1101_BASE__ + 0x34  /* Radio core interrupt edge select register */
sfrw(RF1AIES, RF1AIES_);
#define RF1AIES_L_             __MSP430_CC1101_BASE__ + 0x34
sfrb(RF1AIES_L, RF1AIES_L_);
#define RF1AIES_H_             __MSP430_CC1101_BASE__ + 0x35
sfrb(RF1AIES_H, RF1AIES_H_);
#define RF1AIE_                __MSP430_CC1101_BASE__ + 0x36  /* Radio core interrupt enable register */
sfrw(RF1AIE, RF1AIE_);
#define RF1AIE_L_              __MSP430_CC1101_BASE__ + 0x36
sfrb(RF1AIE_L, RF1AIE_L_);
#define RF1AIE_H_              __MSP430_CC1101_BASE__ + 0x37
sfrb(RF1AIE_H, RF1AIE_H_);
#define RF1AIV_                __MSP430_CC1101_BASE__ + 0x38  /* Radio core interrupt vector word register */
sfrw(RF1AIV, RF1AIV_);
#define RF1AIV_L_              __MSP430_CC1101_BASE__ + 0x38
sfrb(RF1AIV_L, RF1AIV_L_);
#define RF1AIV_H_              __MSP430_CC1101_BASE__ + 0x39
sfrb(RF1AIV_H, RF1AIV_H_);
#define RF1ARXFIFO_            __MSP430_CC1101_BASE__ + 0x3c  /* Direct receive FIFO access register */
sfrw(RF1ARXFIFO, RF1ARXFIFO_);
#define RF1ARXFIFO_L_          __MSP430_CC1101_BASE__ + 0x3c
sfrb(RF1ARXFIFO_L, RF1ARXFIFO_L_);
#define RF1ARXFIFO_H_          __MSP430_CC1101_BASE__ + 0x3d
sfrb(RF1ARXFIFO_H, RF1ARXFIFO_H_);
#define RF1ATXFIFO_            __MSP430_CC1101_BASE__ + 0x3e  /* Direct transmit FIFO access register */
sfrw(RF1ATXFIFO, RF1ATXFIFO_);
#define RF1ATXFIFO_L_          __MSP430_CC1101_BASE__ + 0x3e
sfrb(RF1ATXFIFO_L, RF1ATXFIFO_L_);
#define RF1ATXFIFO_H_         __MSP430_CC1101_BASE__ + 0x3f
sfrb(RF1ATXFIFO_H, RF1ATXFIFO_H_);

/* RF1AIFCTL0 Control Bits */
#define RFFIFOEN               (0x0001)       /* CC1101 Direct FIFO access enable */
#define RFENDIAN               (0x0002)       /* CC1101 Disable endianness conversion */

/* RF1AIFCTL0 Control Bits */
#define RFFIFOEN_L             (0x0001)       /* CC1101 Direct FIFO access enable */
#define RFENDIAN_L             (0x0002)       /* CC1101 Disable endianness conversion */

/* RF1AIFCTL0 Control Bits */

/* RF1AIFCTL1 Control Bits */
#define RFRXIFG                (0x0001)       /* Radio interface direct FIFO access receive interrupt flag */
#define RFTXIFG                (0x0002)       /* Radio interface direct FIFO access transmit interrupt flag */
#define RFERRIFG               (0x0004)       /* Radio interface error interrupt flag */
#define RFINSTRIFG             (0x0010)       /* Radio interface instruction interrupt flag */
#define RFDINIFG               (0x0020)       /* Radio interface data in interrupt flag */
#define RFSTATIFG              (0x0040)       /* Radio interface status interrupt flag */
#define RFDOUTIFG              (0x0080)       /* Radio interface data out interrupt flag */
#define RFRXIE                 (0x0100)       /* Radio interface direct FIFO access receive interrupt enable */
#define RFTXIE                 (0x0200)       /* Radio interface direct FIFO access transmit interrupt enable */
#define RFERRIE                (0x0400)       /* Radio interface error interrupt enable */
#define RFINSTRIE              (0x1000)       /* Radio interface instruction interrupt enable */
#define RFDINIE                (0x2000)       /* Radio interface data in interrupt enable */
#define RFSTATIE               (0x4000)       /* Radio interface status interrupt enable */
#define RFDOUTIE               (0x8000)       /* Radio interface data out interrupt enable */

/* RF1AIFCTL1 Control Bits */
#define RFRXIFG_L              (0x0001)       /* Radio interface direct FIFO access receive interrupt flag */
#define RFTXIFG_L              (0x0002)       /* Radio interface direct FIFO access transmit interrupt flag */
#define RFERRIFG_L             (0x0004)       /* Radio interface error interrupt flag */
#define RFINSTRIFG_L           (0x0010)       /* Radio interface instruction interrupt flag */
#define RFDINIFG_L             (0x0020)       /* Radio interface data in interrupt flag */
#define RFSTATIFG_L            (0x0040)       /* Radio interface status interrupt flag */
#define RFDOUTIFG_L            (0x0080)       /* Radio interface data out interrupt flag */

/* RF1AIFCTL1 Control Bits */
#define RFRXIE_H               (0x0001)       /* Radio interface direct FIFO access receive interrupt enable */
#define RFTXIE_H               (0x0002)       /* Radio interface direct FIFO access transmit interrupt enable */
#define RFERRIE_H              (0x0004)       /* Radio interface error interrupt enable */
#define RFINSTRIE_H            (0x0010)       /* Radio interface instruction interrupt enable */
#define RFDINIE_H              (0x0020)       /* Radio interface data in interrupt enable */
#define RFSTATIE_H             (0x0040)       /* Radio interface status interrupt enable */
#define RFDOUTIE_H             (0x0080)       /* Radio interface data out interrupt enable */

/* RF1AIFERR Control Bits */
#define LVERR                  (0x0001)       /* Low Core Voltage Error Flag */
#define OPERR                  (0x0002)       /* Operand Error Flag */
#define OUTERR                 (0x0004)       /* Output data not available Error Flag */
#define OPOVERR                (0x0008)       /* Operand Overwrite Error Flag */

/* RF1AIFERR Control Bits */
#define LVERR_L                (0x0001)       /* Low Core Voltage Error Flag */
#define OPERR_L                (0x0002)       /* Operand Error Flag */
#define OUTERR_L               (0x0004)       /* Output data not available Error Flag */
#define OPOVERR_L              (0x0008)       /* Operand Overwrite Error Flag */

/* RF1AIFERR Control Bits */

/* RF1AIFERRV Definitions */
#define RF1AIFERRV_NONE        (0x0000)       /* No Error pending */
#define RF1AIFERRV_LVERR       (0x0002)       /* Low core voltage error */
#define RF1AIFERRV_OPERR       (0x0004)       /* Operand Error */
#define RF1AIFERRV_OUTERR      (0x0006)       /* Output data not available Error */
#define RF1AIFERRV_OPOVERR     (0x0008)       /* Operand Overwrite Error */

/* RF1AIFIV Definitions */
#define RF1AIFIV_NONE          (0x0000)       /* No Interrupt pending */
#define RF1AIFIV_RFERRIFG      (0x0002)       /* Radio interface error */
#define RF1AIFIV_RFDOUTIFG     (0x0004)       /* Radio i/f data out */
#define RF1AIFIV_RFSTATIFG     (0x0006)       /* Radio i/f status out */
#define RF1AIFIV_RFDINIFG      (0x0008)       /* Radio i/f data in */
#define RF1AIFIV_RFINSTRIFG    (0x000A)       /* Radio i/f instruction in */
#define RF1AIFIV_RFRXIFG       (0x000C)       /* Radio direct FIFO RX */
#define RF1AIFIV_RFTXIFG       (0x000E)       /* Radio direct FIFO TX */

/* RF1AIV Definitions */
#define RF1AIV_NONE            (0x0000)       /* No Interrupt pending */
#define RF1AIV_RFIFG0          (0x0002)       /* RFIFG0 */
#define RF1AIV_RFIFG1          (0x0004)       /* RFIFG1 */
#define RF1AIV_RFIFG2          (0x0006)       /* RFIFG2 */
#define RF1AIV_RFIFG3          (0x0008)       /* RFIFG3 */
#define RF1AIV_RFIFG4          (0x000A)       /* RFIFG4 */
#define RF1AIV_RFIFG5          (0x000C)       /* RFIFG5 */
#define RF1AIV_RFIFG6          (0x000E)       /* RFIFG6 */
#define RF1AIV_RFIFG7          (0x0010)       /* RFIFG7 */
#define RF1AIV_RFIFG8          (0x0012)       /* RFIFG8 */
#define RF1AIV_RFIFG9          (0x0014)       /* RFIFG9 */
#define RF1AIV_RFIFG10         (0x0016)       /* RFIFG10 */
#define RF1AIV_RFIFG11         (0x0018)       /* RFIFG11 */
#define RF1AIV_RFIFG12         (0x001A)       /* RFIFG12 */
#define RF1AIV_RFIFG13         (0x001C)       /* RFIFG13 */
#define RF1AIV_RFIFG14         (0x001E)       /* RFIFG14 */
#define RF1AIV_RFIFG15         (0x0020)       /* RFIFG15 */

// Radio Core Registers
#define IOCFG2                 0x00           /*  IOCFG2   - GDO2 output pin configuration  */
#define IOCFG1                 0x01           /*  IOCFG1   - GDO1 output pin configuration  */
#define IOCFG0                 0x02           /*  IOCFG1   - GDO0 output pin configuration  */
#define FIFOTHR                0x03           /*  FIFOTHR  - RX FIFO and TX FIFO thresholds */
#define SYNC1                  0x04           /*  SYNC1    - Sync word, high byte */
#define SYNC0                  0x05           /*  SYNC0    - Sync word, low byte */
#define PKTLEN                 0x06           /*  PKTLEN   - Packet length */
#define PKTCTRL1               0x07           /*  PKTCTRL1 - Packet automation control */
#define PKTCTRL0               0x08           /*  PKTCTRL0 - Packet automation control */
#define ADDR                   0x09           /*  ADDR     - Device address */
#define CHANNR                 0x0A           /*  CHANNR   - Channel number */
#define FSCTRL1                0x0B           /*  FSCTRL1  - Frequency synthesizer control */
#define FSCTRL0                0x0C           /*  FSCTRL0  - Frequency synthesizer control */
#define FREQ2                  0x0D           /*  FREQ2    - Frequency control word, high byte */
#define FREQ1                  0x0E           /*  FREQ1    - Frequency control word, middle byte */
#define FREQ0                  0x0F           /*  FREQ0    - Frequency control word, low byte */
#define MDMCFG4                0x10           /*  MDMCFG4  - Modem configuration */
#define MDMCFG3                0x11           /*  MDMCFG3  - Modem configuration */
#define MDMCFG2                0x12           /*  MDMCFG2  - Modem configuration */
#define MDMCFG1                0x13           /*  MDMCFG1  - Modem configuration */
#define MDMCFG0                0x14           /*  MDMCFG0  - Modem configuration */
#define DEVIATN                0x15           /*  DEVIATN  - Modem deviation setting */
#define MCSM2                  0x16           /*  MCSM2    - Main Radio Control State Machine configuration */
#define MCSM1                  0x17           /*  MCSM1    - Main Radio Control State Machine configuration */
#define MCSM0                  0x18           /*  MCSM0    - Main Radio Control State Machine configuration */
#define FOCCFG                 0x19           /*  FOCCFG   - Frequency Offset Compensation configuration */
#define BSCFG                  0x1A           /*  BSCFG    - Bit Synchronization configuration */
#define AGCCTRL2               0x1B           /*  AGCCTRL2 - AGC control */
#define AGCCTRL1               0x1C           /*  AGCCTRL1 - AGC control */
#define AGCCTRL0               0x1D           /*  AGCCTRL0 - AGC control */
#define WOREVT1                0x1E           /*  WOREVT1  - High byte Event0 timeout */
#define WOREVT0                0x1F           /*  WOREVT0  - Low byte Event0 timeout */
#define WORCTRL                0x20           /*  WORCTRL  - Wake On Radio control */
#define FREND1                 0x21           /*  FREND1   - Front end RX configuration */
#define FREND0                 0x22           /*  FREDN0   - Front end TX configuration */
#define FSCAL3                 0x23           /*  FSCAL3   - Frequency synthesizer calibration */
#define FSCAL2                 0x24           /*  FSCAL2   - Frequency synthesizer calibration */
#define FSCAL1                 0x25           /*  FSCAL1   - Frequency synthesizer calibration */
#define FSCAL0                 0x26           /*  FSCAL0   - Frequency synthesizer calibration */
//#define RCCTRL1             0x27      /*  RCCTRL1  - RC oscillator configuration */
//#define RCCTRL0             0x28      /*  RCCTRL0  - RC oscillator configuration */
#define FSTEST                 0x29           /*  FSTEST   - Frequency synthesizer calibration control */
#define PTEST                  0x2A           /*  PTEST    - Production test */
#define AGCTEST                0x2B           /*  AGCTEST  - AGC test */
#define TEST2                  0x2C           /*  TEST2    - Various test settings */
#define TEST1                  0x2D           /*  TEST1    - Various test settings */
#define TEST0                  0x2E           /*  TEST0    - Various test settings */

/* status registers */
#define PARTNUM                0x30           /*  PARTNUM    - Chip ID */
#define VERSION                0x31           /*  VERSION    - Chip ID */
#define FREQEST                0x32           /*  FREQEST     Frequency Offset Estimate from demodulator */
#define LQI                    0x33           /*  LQI         Demodulator estimate for Link Quality */
#define RSSI                   0x34           /*  RSSI        Received signal strength indication */
#define MARCSTATE              0x35           /*  MARCSTATE   Main Radio Control State Machine state */
#define WORTIME1               0x36           /*  WORTIME1    High byte of WOR time */
#define WORTIME0               0x37           /*  WORTIME0    Low byte of WOR time */
#define PKTSTATUS              0x38           /*  PKTSTATUS   Current GDOx status and packet status */
#define VCO_VC_DAC             0x39           /*  VCO_VC_DAC  Current setting from PLL calibration module */
#define TXBYTES                0x3A           /*  TXBYTES     Underflow and number of bytes */
#define RXBYTES                0x3B           /*  RXBYTES     Overflow and number of bytes */

/* burst write registers */
#define PATABLE                0x3E           /*  PATABLE - PA control settings table */
#define TXFIFO                 0x3F           /*  TXFIFO  - Transmit FIFO */
#define RXFIFO                 0x3F           /*  RXFIFO  - Receive FIFO */

/* Radio Core Instructions */
/* command strobes               */
#define RF_SRES                0x30           /*  SRES    - Reset chip. */
#define RF_SFSTXON             0x31           /*  SFSTXON - Enable and calibrate frequency synthesizer. */
#define RF_SXOFF               0x32           /*  SXOFF   - Turn off crystal oscillator. */
#define RF_SCAL                0x33           /*  SCAL    - Calibrate frequency synthesizer and turn it off. */
#define RF_SRX                 0x34           /*  SRX     - Enable RX. Perform calibration if enabled. */
#define RF_STX                 0x35           /*  STX     - Enable TX. If in RX state, only enable TX if CCA passes. */
#define RF_SIDLE               0x36           /*  SIDLE   - Exit RX / TX, turn off frequency synthesizer. */
//#define RF_SRSVD            0x37      /*  SRVSD   - Reserved.  Do not use. */
#define RF_SWOR                0x38           /*  SWOR    - Start automatic RX polling sequence (Wake-on-Radio) */
#define RF_SPWD                0x39           /*  SPWD    - Enter power down mode when CSn goes high. */
#define RF_SFRX                0x3A           /*  SFRX    - Flush the RX FIFO buffer. */
#define RF_SFTX                0x3B           /*  SFTX    - Flush the TX FIFO buffer. */
#define RF_SWORRST             0x3C           /*  SWORRST - Reset real time clock. */
#define RF_SNOP                0x3D           /*  SNOP    - No operation. Returns status byte. */

#define RF_RXSTAT              0x80           /* Used in combination with strobe commands delivers number of availabe bytes in RX FIFO with return status */
#define RF_TXSTAT              0x00           /* Used in combination with strobe commands delivers number of availabe bytes in TX FIFO with return status */

/* other radio instr */
#define RF_SNGLREGRD           0x80
#define RF_SNGLREGWR           0x00
#define RF_REGRD               0xC0
#define RF_REGWR               0x40
#define RF_STATREGRD           0xC0           /* Read single radio core status register */
#define RF_SNGLPATABRD         (RF_SNGLREGRD+PATABLE)
#define RF_SNGLPATABWR         (RF_SNGLREGWR+PATABLE)
#define RF_PATABRD             (RF_REGRD+PATABLE)
#define RF_PATABWR             (RF_REGWR+PATABLE)
#define RF_SNGLRXRD            (RF_SNGLREGRD+RXFIFO)
#define RF_SNGLTXWR            (RF_SNGLREGWR+TXFIFO)
#define RF_RXFIFORD            (RF_REGRD+RXFIFO)
#define RF_TXFIFOWR            (RF_REGWR+TXFIFO)



#define __MSP430_T0A_BASE__ 0x0340

#define TA0CTL_             __MSP430_T0A_BASE__ + 0x00                      // Timer A 0 Control
sfrw (TA0CTL,TA0CTL_);
#define TA0CCTL1_           __MSP430_T0A_BASE__ + 0x04                      // Timer A 0 Capture/Compare Control 1
sfrw (TA0CCTL1,TA0CCTL1_);
#define TA0CCTL1_L_         __MSP430_T0A_BASE__ + 0x04
sfrb (TA0CCTL1_L,TA0CCTL1_L_);
#define TA0CCTL1_H_         __MSP430_T0A_BASE__ + 0x05
sfrb (TA0CCTL1_H,TA0CCTL1_H_);
#define TA0CCR1_            __MSP430_T0A_BASE__ + 0x14                      // Timer A 0 Capture/Compare 1
sfrw (TA0CCR1,TA0CCR1_);
#define TA0CCR1_L_          __MSP430_T0A_BASE__ + 0x14
sfrb (TA0CCR1_L,TA0CCR1_L_);
#define TA0CCR1_H_          __MSP430_T0A_BASE__ + 0x15
sfrb (TA0CCR1_H,TA0CCR1_H_);
#define TA0R_               __MSP430_T0A_BASE__ + 0x10                      // Timer A 0
sfrw (TA0R,TA0R_);
#define TA0R_L_             __MSP430_T0A_BASE__ + 0x10
sfrb (TA0R_L,TA0R_L_);
#define TA0R_H_             __MSP430_T0A_BASE__ + 0x11
sfrb (TA0R_H,TA0R_H_);

#define CCIFG 0x01 // Capture compare interrupt flag

#define TASSEL1             0x0200  /* Timer A clock source select 1 */
#define TASSEL0             0x0100  /* Timer A clock source select 0 */
#define ID1                 0x0080  /* Timer A clock input divider 1 */
#define ID0                 0x0040  /* Timer A clock input divider 0 */
#define MC1                 0x0020  /* Timer A mode control 1 */
#define MC0                 0x0010  /* Timer A mode control 0 */
#define TACLR               0x0004  /* Timer A counter clear */
#define TAIE                0x0002  /* Timer A counter interrupt enable */
#define TAIFG               0x0001  /* Timer A counter interrupt flag */
#endif /* RADIOCONFIG_H_ */
