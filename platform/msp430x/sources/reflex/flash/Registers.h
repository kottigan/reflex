/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_FLASH_REGISTERS_H
#define REFLEX_MSP430X_FLASH_REGISTERS_H

#include "reflex/types.h"
#include "reflex/data_types/Register.h"


namespace reflex {
namespace msp430x {
	namespace flash {

		  enum Bits {
			FWKEY = 0xA500
		    //FCTL1
		    ,ERASE = 0x0002
		    ,MERAS = 0x0004
		    ,SWRT  = 0x0020
		    ,WRT  = 0x0040
		    ,BLKWRT = 0x0080
		    //FCTl3
		    ,BUSY = 0x0001
		    ,KEYV = 0x0002
		    ,ACCVIFG = 0x0004
		    ,WAIT  = 0x0008
		    ,LOCK  = 0x0010
		    ,LOCKA = 0x0040
		    //FCTl4
		    ,VPE = 0x0001
		    ,MRG0  = 0x0010
		    ,MRG1  = 0x0020
		    ,LOCKINFO = 0x0080
		  };
			struct Registers {
			protected:
				typedef data_types::PWProtectedRegister<uint16,flash::FWKEY> Register;

			public:
				Register FCTL1;
				uint16 :16;
				Register FCTL3;
				Register FCTL4;
	//			volatile unsigned int  FCTL1;   ///FCTL1 at address 0x0140,
	//			uint16 :16;
	//			volatile unsigned int  FCTL3;  ///FCTL3 at address 0x0144,
	//			volatile unsigned int  FCTL4;  ///FCTL4 at address 0x0146,
			public:
				Registers(){}
				operator Registers*() {return operator->();}
				Registers* operator-> () {return reinterpret_cast<Registers*>(bases::FLASHCTL+offsets::FLASHCTL);}
			};


	} //ns flash
}}// ns msp430x reflex

#endif // REGISTERS_H
