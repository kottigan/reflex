/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_WDT_A_REGISTERS_H
#define REFLEX_WDT_A_REGISTERS_H

#include "reflex/mcu/msp430x.h"
#include "reflex/types.h"
#include "reflex/data_types/Register.h"

namespace reflex {
namespace msp430x {
namespace wdt_a {

	enum RegisterBITS {
		PW_VAL= 0x5A
		,PW_MASK=(0x5A<<8)   /* Watchdog timer password. Always read as 069h. Must be written as 05Ah, or a PUC will be generated */
		,HOLD=(1<<7)      /* Watchdog timer hold */
		,S_SMCLK=(0x0<<5)      /* Watchdog timer clock source select SMCLK */
		,S_ACLK=(0x1<<5)      /* Watchdog timer clock source select ACLK */
		,S_VLOCLK=(0x2<<5)      /* Watchdog timer clock source select VLOCLK */
		,S_X_CLK=(0x3<<5)      /* Watchdog timer clock source select X_CLK */

		,TMSEL=(1<<4)      /* Watchdog timer mode select */
		,CNTCL=(1<<3)      /* Watchdog timer counter clear */

		,IS_2G=(0x0<<0)      /* Watchdog timer interval select */
		,IS_128M=(0x1<<0)      /* Watchdog timer interval select */
		,IS_8192k=(0x2<<0)      /* Watchdog timer interval select */
		,IS_512k=(0x3<<0)      /* Watchdog timer interval select */
		,IS_32k=(0x4<<0)      /* Watchdog timer interval select */
		,IS_8192=(0x5<<0)      /* Watchdog timer interval select */
		,IS_512=(0x6<<0)      /* Watchdog timer interval select */
		,IS_64=(0x7<<0)      /* Watchdog timer interval select */
	};

	enum ClockSource {
		 SMCLK = S_SMCLK
		,ACLK = S_ACLK
		,VLOCLK = S_VLOCLK
		,X_CLK = S_X_CLK

		,ClkSrcMASK = SMCLK|ACLK|VLOCLK|X_CLK
	};

	enum Interval {
		 Ivl2G =	IS_2G
		,Ivl128M =	IS_128M
		,Ivl8192k=	IS_8192k
		,Ivl512k =	IS_512k
		,Ivl32k =	IS_32k
		,Ivl8192 =	IS_8192
		,Ivl512 =	IS_512
		,Ivl64 =	IS_64
		,IntervalMASK= Ivl2G|Ivl128M|Ivl8192k|Ivl512k|Ivl32k|Ivl8192|Ivl512|Ivl64
	};

	/** Registerfile for the Watchdog Timer module
	 */
	struct Registers
	{
		//writes implicite the password on writeoperations
		data_types::PWProtectedRegister<uint16,wdt_a::PW_MASK> WDTCTL; /* watchdog timer control register */

	public:
		Registers(){}
		operator Registers*() {return operator->();}
		Registers* operator-> () {return reinterpret_cast<Registers*>(bases::WDT_A + offsets::WDT_A);}
	};
}//ns wdt_a

}}//ns msp430,reflex


#endif // REGISTERS_H
