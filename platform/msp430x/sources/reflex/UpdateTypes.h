#ifndef UPDATETYPES_H_
#define UPDATETYPES_H_

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Author:		 Hannes Menzel
 *
 *	Description: add here your description
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include <reflex/types.h>

enum UpdatePacketTypes
{
    ApplicationPacket  = 0x00,
    MetaPacket   = 0x02,
    DataPacket   = 0x03,
    LeapPacket   = 0x05
};


/* The header is part of every update packet */
struct UpdateHeader
{
    uint8 type;
    uint8 programID;
    uint8 version;
    uint8 reserved;

    inline uint8 getProgramID() { return programID >> 2; }
    inline uint8 getRes() { return (programID >> 1) & 0x1; }
    inline uint8 getOverride() { return programID & 0x1; }
    inline void setProgramID(uint8 progID, uint8 res, uint8 override)
    {
        programID = (progID << 2) | (res << 1) | override;
    }

} __attribute__ ((packed));


/* Meta information about the update process */
struct UpdateMetaPacket
{
    UpdateHeader header;
    uint16 packetCount;
    uint8 remainder;
    uint8 padding;
} __attribute__ ((packed));


/* Contains an image segment */
struct UpdateDataPacket
{
    enum
    {
        PayloadSize = 20
    };

    UpdateHeader header;
    uint16 seqNo;
    uint8 data[PayloadSize];
} __attribute__ ((packed));


/* Represents an empty memory segment filled with 0xff */
struct UpdateLeapPacket
{
    UpdateHeader header;
    uint16 sequenceNo;
    uint16 repeatCount;
} __attribute__ ((packed));


union UpdatePacket
{
    UpdateHeader header;
    UpdateMetaPacket meta;
    UpdateDataPacket data;
    UpdateLeapPacket leap;
};

#endif /* UPDATETYPES_H_ */
