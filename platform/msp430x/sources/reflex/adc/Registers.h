/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_ADC_REGISTERS_H
#define REFLEX_MSP430X_ADC_REGISTERS_H
#include "reflex/types.h"
#include "reflex/data_types/ReadOnly.h"
#include "reflex/data_types/Register.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/interrupts/InterruptDispatcher.h"

namespace reflex{
namespace msp430x {
namespace adc {
	//this bit definitions has been reused from mspgcc-glibc.
	enum Bits {
		/* ADC12CTL0 */
		 SC =			 0x0001      /* ADC12 Start Conversion */
		,ENC=                 0x0002      /* ADC12 Enable Conversion */
		,TOVIE=          0x0004      /* ADC12 Timer Overflow interrupt enable */
		,OVIE=           0x0008      /* ADC12 Overflow interrupt enable */
		,ON=             0x0010      /* ADC12 On/enable */
		,REFON  =             0x0020      /* ADC12 Reference on */
		,REF2_5V=             0x0040      /* ADC12 Ref 0:1.5V / 1:2.5V */
		,MSC=                 0x0080      /* ADC12 Multiple Sample Conversion */
		,MSH=                 0x0080
		,SHT00=               0x0100      /* ADC12 Sample Hold 0 Select 0 */
		,SHT01=               0x0200      /* ADC12 Sample Hold 0 Select 1 */
		,SHT02=               0x0400      /* ADC12 Sample Hold 0 Select 2 */
		,SHT03=               0x0800      /* ADC12 Sample Hold 0 Select 3 */
		,SHT10=               0x1000      /* ADC12 Sample Hold 0 Select 0 */
		,SHT11=               0x2000      /* ADC12 Sample Hold 1 Select 1 */
		,SHT12=               0x4000      /* ADC12 Sample Hold 2 Select 2 */
		,SHT13=               0x8000      /* ADC12 Sample Hold 3 Select 3 */

		,SHT0_0=              (0<<8)      /* 4 */
		,SHT0_1=              (1<<8)      /* 8 */
		,SHT0_2=              (2<<8)      /* 16 */
		,SHT0_3=              (3<<8)      /* 32 */
		,SHT0_4 =             (4<<8)      /* 64 */
		,SHT0_5=              (5<<8)      /* 96 */
		,SHT0_6=              (6<<8)      /* 128 */
		,SHT0_7=              (7<<8)      /* 192 */
		,SHT0_8=              (8<<8)      /* 256 */
		,SHT0_9=              (9<<8)      /* 384 */
		,SHT0_10=             (10<<8)     /* 512 */
		,SHT0_11=             (11<<8)     /* 768 */
		,SHT0_12=             (12<<8)     /* 1024 */
		,SHT0_13=             (13<<8)     /* 1024 */
		,SHT0_14=             (14<<8)     /* 1024 */
		,SHT0_15=             (15<<8)     /* 1024 */

		,SHT1_0=              (0<<12)     /* 4 */
		,SHT1_1=              (1<<12)     /* 8 */
		,SHT1_2=              (2<<12)     /* 16 */
		,SHT1_3=              (3<<12)     /* 32 */
		,SHT1_4=              (4<<12)     /* 64 */
		,SHT1_5=              (5<<12)     /* 96 */
		,SHT1_6=              (6<<12)     /* 128 */
		,SHT1_7=              (7<<12)     /* 192 */
		,SHT1_8=              (8<<12)     /* 256 */
		,SHT1_9=              (9<<12)     /* 384 */
		,SHT1_10=             (10<<12)    /* 512 */
		,SHT1_11=             (11<<12)    /* 768 */
		,SHT1_12=             (12<<12)    /* 1024 */
		,SHT1_13=             (13<<12)    /* 1024 */
		,SHT1_14=             (14<<12)    /* 1024 */
		,SHT1_15=             (15<<12)    /* 1024 */

		/* ADC12CTL1 */
		,BUSY=           0x0001      /* ADC12 Busy */
		,CONSEQ0=             0x0002      /* ADC12 Conversion Sequence Select 0 */
		,CONSEQ1=             0x0004      /* ADC12 Conversion Sequence Select 1 */
		,SSEL0=          0x0008      /* ADC12 Clock Source Select 0 */
		,SSEL1=          0x0010      /* ADC12 Clock Source Select 1 */
		,DIV0 =          0x0020      /* ADC12 Clock Divider Select 0 */
		,DIV1 =          0x0040      /* ADC12 Clock Divider Select 1 */
		,DIV2 =          0x0080      /* ADC12 Clock Divider Select 2 */
		,ISSH      =          0x0100      /* ADC12 Invert Sample Hold Signal */
		,SHP       =          0x0200      /* ADC12 Sample/Hold Pulse Mode */
		,SHS0      =          0x0400      /* ADC12 Sample/Hold Source 0 */
		,SHS1      =          0x0800      /* ADC12 Sample/Hold Source 1 */
		,CSTARTADD0 =         0x1000      /* ADC12 Conversion Start Address 0 */
		,CSTARTADD1 =         0x2000      /* ADC12 Conversion Start Address 1 */
		,CSTARTADD2 =         0x4000      /* ADC12 Conversion Start Address 2 */
		,CSTARTADD3 =         0x8000      /* ADC12 Conversion Start Address 3 */

		,CONSEQ_0 =           (0<<1)      /* Single-channel, single-conversion */
		,CONSEQ_1 =           (1<<1)      /* Sequence-of-channels */
		,CONSEQ_2 =           (2<<1)      /* Repeat-single-channel */
		,CONSEQ_3 =           (3<<1)      /* Repeat-sequence-of-channels */
		,SSEL_0=         (0<<3)      /* ADC12OSC */
		,SSEL_1=         (1<<3)      /* ACLK */
		,SSEL_2=         (2<<3)      /* MCLK */
		,SSEL_3=         (3<<3)      /* SMCLK */
		,DIV_0 =         (0<<5)
		,DIV_1 =         (1<<5)
		,DIV_2 =         (2<<5)
		,DIV_3 =         (3<<5)
		,DIV_4 =         (4<<5)
		,DIV_5 =         (5<<5)
		,DIV_6 =         (6<<5)
		,DIV_7 =         (7<<5)
		,SHS_0      =         (0<<10)     /* ADC12SC bit */
		,SHS_1      =         (1<<10)     /* Timer_A.OUT1 */
		,SHS_2      =         (2<<10)     /* Timer_B.OUT0 */
		,SHS_3      =         (3<<10)     /* Timer_B.OUT1 */
		,CSTARTADD_0=         (0<<12)
		,CSTARTADD_1=         (1<<12)
		,CSTARTADD_2=         (2<<12)
		,CSTARTADD_3=         (3<<12)
		,CSTARTADD_4=         (4<<12)
		,CSTARTADD_5=         (5<<12)
		,CSTARTADD_6=         (6<<12)
		,CSTARTADD_7=         (7<<12)
		,CSTARTADD_8=         (8<<12)
		,CSTARTADD_9=         (9<<12)
		,CSTARTADD_10=        (10<<12)
		,CSTARTADD_11=        (11<<12)
		,CSTARTADD_12=        (12<<12)
		,CSTARTADD_13=        (13<<12)
		,CSTARTADD_14=        (14<<12)
		,CSTARTADD_15=        (15<<12)

		/* ADC12MCTLx */
		,INCH_0=               0         /* A0 */
		,INCH_1=               1         /* A1 */
		,INCH_2=               2         /* A2 */
		,INCH_3=               3         /* A3 */
		,INCH_4=               4         /* A4 */
		,INCH_5=               5         /* A5 */
		,INCH_6=               6         /* A6 */
		,INCH_7=               7         /* A7 */
		,INCH_8=               8         /* VeREF+ */
		,INCH_9=               9         /* VREF/VeREF */
		,INCH_10=             10         /* Temperature diode */
		,INCH_11=             11         /* (AVCC  AVSS) / 2 */
		,INCH_12=             12         /* (AVCC  AVSS) / 2 */
		,INCH_13=             13         /* (AVCC  AVSS) / 2 */
		,INCH_14=             14         /* (AVCC  AVSS) / 2 */
		,INCH_15=             15         /* (AVCC  AVSS) / 2 */

		,SREF_0=              (0<<4)     /* VR+ = AVCC and VR = AVSS */
		,SREF_1=              (1<<4)     /* VR+ = VREF+ and VR = AVSS */
		,SREF_2=              (2<<4)     /* VR+ = VeREF+ and VR = AVSS */
		,SREF_3=              (3<<4)     /* VR+ = VeREF+ and VR = AVSS */
		,SREF_4=              (4<<4)     /* VR+ = AVCC and VR = VREF/ VeREF */
		,SREF_5=              (5<<4)     /* VR+ = VREF+ and VR = VREF/ VeREF */
		,SREF_6=              (6<<4)     /* VR+ = VeREF+ and VR = VREF/ VeREF */
		,SREF_7=              (7<<4)     /* VR+ = VeREF+ and VR = VREF/ VeREF */
		,EOS   =              0x80

//		/* Aliases by mspgcc */
//		,SHT0_DIV4           SHT0_0     /* 4 */
//		,SHT0_DIV8           SHT0_1     /* 8 */
//		,SHT0_DIV16          SHT0_2     /* 16 */
//		,SHT0_DIV32          SHT0_3     /* 32 */
//		,SHT0_DIV64          SHT0_4     /* 64 */
//		,SHT0_DIV96          SHT0_5     /* 96 */
//		,SHT0_DIV128         SHT0_6     /* 128 */
//		,SHT0_DIV192         SHT0_7     /* 192 */
//		,SHT0_DIV256         SHT0_8     /* 256 */
//		,SHT0_DIV384         SHT0_9     /* 384 */
//		,SHT0_DIV512         SHT0_10    /* 512 */
//		,SHT0_DIV768         SHT0_11    /* 768 */
//		,SHT0_DIV1024        SHT0_12    /* 1024 */
//
//		,SHT1_DIV4           SHT1_0     /* 4 */
//		,SHT1_DIV8           SHT1_1     /* 8 */
//		,SHT1_DIV16          SHT1_2     /* 16 */
//		,SHT1_DIV32          SHT1_3     /* 32 */
//		,SHT1_DIV64          SHT1_4     /* 64 */
//		,SHT1_DIV96          SHT1_5     /* 96 */
//		,SHT1_DIV128         SHT1_6     /* 128 */
//		,SHT1_DIV192         SHT1_7     /* 192 */
//		,SHT1_DIV256         SHT1_8     /* 256 */
//		,SHT1_DIV384         SHT1_9     /* 384 */
//		,SHT1_DIV512         SHT1_10    /* 512 */
//		,SHT1_DIV768         SHT1_11    /* 768 */
//		,SHT1_DIV1024        SHT1_12    /* 1024 */
//
//		,CONSEQ_SINGLE       CONSEQ_0    /* Single-channel, single-conversion */
//		,CONSEQ_SEQUENCE     CONSEQ_1    /* Sequence-of-channels */
//		,CONSEQ_REPEAT_SINGLE    CONSEQ_2    /* Repeat-single-channel */
//		,CONSEQ_REPEAT_SEQUENCE  CONSEQ_3    /* Repeat-sequence-of-channels */
		,SSEL_ADC12OSC=  SSEL_0 /* ADC12OSC */
		,SSEL_ACLK =     SSEL_1 /* ACLK */
		,SSEL_MCLK =     SSEL_2 /* MCLK */
		,SSEL_SMCLK=     SSEL_3 /* SMCLK */
//		,SHS_ADC12SC         SHS_0       /* ADC12SC bit */
//		,SHS_TACCR1          SHS_1       /* Timer_A.OUT1 */
//		,SHS_TBCCR0          SHS_2       /* Timer_B.OUT0 */
//		,SHS_TBCCR1          SHS_3       /* Timer_B.OUT1 */
//
//		,INCH_A0              0         /* A0 */
//		,INCH_A1              1         /* A1 */
//		,INCH_A2              2         /* A2 */
//		,INCH_A3              3         /* A3 */
//		,INCH_A4              4         /* A4 */
//		,INCH_A5              5         /* A5 */
//		,INCH_A6              6         /* A6 */
//		,INCH_A7              7         /* A7 */
//		,INCH_VEREF_PLUS      8         /* VeREF+ */
//		,INCH_VEREF_MINUS     9         /* VREF/VeREF */
//		,INCH_TEMP           10         /* Temperature diode */
//		,INCH_VCC2           11         /* (AVCC  AVSS) / 2 */
//
//
//		,SREF_AVCC_AVSS      SREF_0      /* VR+ = AVCC and VR = AVSS */
//		,SREF_VREF_AVSS      SREF_1      /* VR+ = VREF+ and VR = AVSS */
//		,SREF_VEREF_AVSS     SREF_2      /* VR+ = VeREF+ and VR = AVSS */
//		//~ ,SREF_VEREF_AVSS     SREF_3      /* VR+ = VeREF+ and VR = AVSS */
//		,SREF_AVCC_VEREF     SREF_4      /* VR+ = AVCC and VR = VREF/ VeREF */
//		,SREF_VREF_VEREF     SREF_5      /* VR+ = VREF+ and VR = VREF/ VeREF */
//		,SREF_VEREF_VEREF    SREF_6      /* VR+ = VeREF+ and VR = VREF/ VeREF */
//		//~ ,SREF_VEREF_VEREF    SREF_7      /* VR+ = VeREF+ and VR = VREF/ VeREF */
//
	};
	//! Registerfile for the adc module
	class Registers
	{
		enum {	 BASE_ADDR = bases::ADC12_A+offsets::ADC12_A
				,MemCTL_OFFSET = 8*2 // 6 times 2 bytes
				,MemCTL_SIZE = 16*1
				,Mem_OFFSET = MemCTL_OFFSET + MemCTL_SIZE
				,Mem_SIZE = 16*2
		};
		typedef Registers RegisterFile;
	public:
		//! the internal interrupts for this module
		struct Interrupt_traits {
			enum Vector {
				//name alias
				MEMOFLW = interrupts::IV0
				,TIMOFLW = interrupts::IV1
				,ADC0 = interrupts::IV2
				,ADC1 = interrupts::IV3
				,ADC2 = interrupts::IV4
				,ADC3 = interrupts::IV5
				,ADC4 = interrupts::IV6
				,ADC5 = interrupts::IV7
				,ADC6 = interrupts::IV8
				,ADC7 = interrupts::IV9
				,ADC8 = interrupts::IV10
				,ADC9 = interrupts::IV11
				,ADC10 = interrupts::IV12
				,ADC11 = interrupts::IV13
				,ADC12 = interrupts::IV14
				,ADC13 = interrupts::IV15
				,ADC14 = interrupts::IV16
				,ADC15 = interrupts::IV17
			};
			enum {COUNT=20};

			typedef interrupts::IVRef<Registers> VectorRef;

			inline static InterruptVector globalVector() {return interrupts::ADC12_A;}
			inline static Vector adcVector(const unsigned& i) {return static_cast<const Vector>(Registers::Interrupt_traits::ADC0+i); }
		};


	public:
		typedef data_types::Register<uint16> Register;
		typedef data_types::Register<uint8> RegisterHalf;
		typedef data_types::ReadOnly<Register> RORegister;
		typedef reflex::InterruptDispatcher<Interrupt_traits > IVDispatcher; //! interrupt dispatcher for interruptvector of adc12

	public:
	//vars
		Register CTL0;
		Register CTL1;
		Register CTL2;
		uint16:16;
		uint16:16;
		Register IFG;
		Register IE;
		RORegister IV;
		//MemControl
		RegisterHalf MCTL0;
		RegisterHalf MCTL1;
		RegisterHalf MCTL2;
		RegisterHalf MCTL3;
		RegisterHalf MCTL4;
		RegisterHalf MCTL5;
		RegisterHalf MCTL6;
		RegisterHalf MCTL7;
		RegisterHalf MCTL8;
		RegisterHalf MCTL9;
		RegisterHalf MCTL10;
		RegisterHalf MCTL11;
		RegisterHalf MCTL12;
		RegisterHalf MCTL13;
		RegisterHalf MCTL14;
		RegisterHalf MCTL15;
		//Memory
		Register MEM0;
		Register MEM1;
		Register MEM2;
		Register MEM3;
		Register MEM4;
		Register MEM5;
		Register MEM6;
		Register MEM7;
		Register MEM8;
		Register MEM9;
		Register MEM10;
		Register MEM11;
		Register MEM12;
		Register MEM13;
		Register MEM14;
		Register MEM15;
	public:
		Registers(){}
		operator RegisterFile*() {return operator->();}
		RegisterFile* operator-> () {return begin();}
		const RegisterFile* operator-> () const {return begin();}


		RegisterFile* begin() { return reinterpret_cast<RegisterFile*>(BASE_ADDR); }
		const RegisterFile* begin() const { return reinterpret_cast<const RegisterFile*>(BASE_ADDR); }
		uint8* beginMemCTL() { return reinterpret_cast<uint8*>(BASE_ADDR+MemCTL_OFFSET); }
		uint8* endMemCTL() { return reinterpret_cast<uint8*>(BASE_ADDR+MemCTL_OFFSET); }
		uint16* beginMem() { return reinterpret_cast<uint16*>(BASE_ADDR+Mem_OFFSET); }
		const uint16* beginMem() const { return reinterpret_cast<uint16*>(BASE_ADDR+Mem_OFFSET); }
	};

}
}
}


#endif // REGISTERS_H
