/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef OVERFLOW_H_
#define OVERFLOW_H_

#include "reflex/MachineDefinitions.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/sinks/Sink.h"
#include "reflex/timer/Timer.h"
#include "reflex/types.h"

namespace reflex
{

namespace atmega
{

/*!
 \ingroup atmega
 \brief Handler for timer overflow interrupts on the ATMEGA

 \todo Add some more documentation!
 */
template<TimerNr timerNr>
class Overflow: public InterruptHandler
{
public:
	Sink0* output;

	Overflow();
	void handle();

	bool isOverflowPending() const;
	void clearOverflow();
private:
	void enable();
	void disable();

	typedef Core::TimerRegisters<timerNr> Register;
};

template<TimerNr timerNr>
Overflow<timerNr>::Overflow() :
	InterruptHandler(Register::OVF, SECONDARY)
{
	if (timerNr == Timer2)
	{
		this->setSleepMode(PowerSave);
	}
	else
	{
		this->setSleepMode(Idle);
	}
	this->output = 0;
}

template<TimerNr timerNr>
void Overflow<timerNr>::handle()
{
	if (this->output)
	{
		this->output->notify();
	}
}

template<TimerNr timerNr>
void Overflow<timerNr>::enable()
{
	Register()->TIMSKn |= Core::TOIEn;
}

template<TimerNr timerNr>
void Overflow<timerNr>::disable()
{
	Register()->TIMSKn &= ~Core::TOIEn;
}

template<TimerNr timerNr>
bool Overflow<timerNr>::isOverflowPending() const
{
	return ((Register()->TIFRn & Core::TOVn) && (Register()->TIMSKn & Core::TOIEn));
}

template<TimerNr timerNr>
void Overflow<timerNr>::clearOverflow()
{
	Register()->TIFRn |= Core::TOVn; // clear by writing a 'one'
}

}

}
#endif /* OVERFLOW_H_ */
