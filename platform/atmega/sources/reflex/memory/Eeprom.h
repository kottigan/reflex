#ifndef EEPROM_H_
#define EEPROM_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/MachineDefinitions.h"
#include "reflex/types.h"

namespace reflex
{
namespace atmega {
/*!
 \ingroup atmega
 \brief Hardware abstraction for the internal EEPROM of ATMEGA controllers.

 The ATMEGA contains a harvard architecture, which seperates address spaces
 for code and memory. To access the internal EEPROM, a sequence of special
 commands is needed.

 This driver offers synchronous access to the EEPROM memory for maximum
 flexibility. Access timings differ greatly. Reading a single byte needs not
 more than 4 clock cycles, while writing takes almost 3.3ms. Interrupts are
 not locked during this time which means, concurrent access from within
 different priority levels are not permitted.

 Eeprom offers only a set of static methods. There is no need to create
 an object instance.

 \sa Flash

 */
class Eeprom
{
	typedef Core::Registers Registers;
public:

	//! Returns \a true if a write operation is still in progress.
	static bool isWriteOperationPending();

	//! Reads from EEPROM address \a addr synchronously.
	template<typename T>
	static T read(T* addr);

	//! \overload
	template<typename T>
	static void read(T* addr, T& data);

	//! Writes \a data of size \a T to EEPROM address \a addr.
	template<typename T>
	static void write(T* addr, const T& data);

private:

	/* Omitted */
	Eeprom();
	Eeprom(const Eeprom& other);
	Eeprom& operator=(const Eeprom& other);

	uint8 static readByte(const void* addr);

	static void writeByte(uint8* addr, uint8 data);
};

/*!
 This method will return after a few clock cycles, if no write operation is
 pending. Otherwise this method will block up to 3.3ms to complete
 the write operation. Use isWriteOperationPending() to check for ongoing
 writes if timing is critical.

 \sa write(), isWriteOperationPending()
 */
template<typename T>
T Eeprom::read(T* addr)
{
	T data;
	uint8* dst = reinterpret_cast<uint8*>(&data);
	for (uint8 i = 0; i < sizeof(T); i++)
	{
		dst[i] = Eeprom::readByte(reinterpret_cast<uint8*> (addr) + i);
	}
	return data;
}

/*!
 \todo This method seems not to work for large abstract data types -> verify.
 (Richard Weickelt, 2011-05-03).
 */
template<typename T>
void Eeprom::read(T* addr, T& data)
{
	for (uint8 i = 0; i < sizeof(T); i++)
	{
		*(reinterpret_cast<uint8*> (&data) + i) = Eeprom::readByte(
				reinterpret_cast<uint8*> (addr) + i);
	}
}

/*!
 When writing a single byte with no preceding write operation, this method will
 initiate the write operation and return after a few clock cycles.
 The write operation is performed in background and completed after 3.3ms. A
 subsequent write will block for that time before it can start the next
 write operation.
 */
template<typename T>
void Eeprom::write(T* addr, const T& data)
{
	for (uint8 i = 0; i < sizeof(T); i++)
	{
		writeByte(reinterpret_cast<uint8*> (addr) + i,
				*(reinterpret_cast<const uint8*> (&data) + i));
	}
}

}

} //reflex


#endif /* EEPROM_H_ */
