#include <reflex/CoreApplication.h>

extern "C" {
	void _idle();
	void _adc_noise();
	void _powerdown();
	void _powersave();
	void _standby();
	void _extstandby();
}

using namespace reflex;
using namespace atmega;

// These variables are located into the bss segment and are therfore
// being initialized to zero according to C++ Standard 3.6.2 (1):
// "Objects with static storage duration (3.7.1) shall be zero-initialized
// (8.5) before any other initialization takes place."
// We have to rely on that, because CoreApplication might get initialized
// as static variable.
CoreApplication* CoreApplication::_instance;
bool CoreApplication::_isInitialized;

namespace {
    PowerDownFunction powerFunctions[mcu::NrOfPowerModes] = {_active,_idle,_adc_noise,_powerdown,_powersave,_standby,_extstandby};
}

/*!
\brief Creates a CoreApplication object.

CoreApplication is a singleton. Calling this method twice will fail or may
result in undefined behaviour.
 */
CoreApplication::CoreApplication() : _initializer(this),
      _powerManager(powerFunctions),
      _guardian(_handlerTable, interrupts::MAX_HANDLERS)
{
    Assert(_isInitialized == false);
    _instance = this;
    _isInitialized = true;
}
