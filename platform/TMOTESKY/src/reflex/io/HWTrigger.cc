/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Stefan Nuernberger
 */
#include "reflex/io/Port.h"
#include "reflex/io/HWTrigger.h"

using namespace reflex;

/**
 *  Trigger for the GeneralI/O Pins on TmoteSky
 */
HWTrigger::HWTrigger()
{
	Port2()->SEL &= ~ALL; // set all pins to I/O-Function
	Port2()->IE &= ~ALL; // disable interrupts on all pins
	Port2()->DIR |= ALL; // set all pins to output
	Port2()->OUT &= ~ALL; // set all pins low
}

void HWTrigger::turnOn(unsigned char pin, bool only)
{
	if (only)
		Port2()->OUT &= ~ALL; // set all pins low

	Port2()->OUT |= pin; // set pin high
}

void HWTrigger::turnOff(unsigned char pin)
{
	Port2()->OUT &= ~pin; // set pin low
}

void HWTrigger::trigger(unsigned char pin)
{
	turnOn(pin);
	// sleep(); // maybe have a nap
	turnOff(pin);
}

void HWTrigger::assign(unsigned char pin)
{
	Port2()->OUT ^= pin; // toggle pin
}

