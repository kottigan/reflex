/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 */
#include "reflex/io/Port.h"
#include "reflex/io/Led.h"

using namespace reflex;

Led::Led()
{
	Port5()->DIR |= ALL; // switch LED pins to output
	Port5()->SEL &= ~ALL;
	Port5()->OUT |= ALL; //All off
}

void Led::turnOn(unsigned char col, bool only)
{
	if ( only )
		Port5()->OUT |= ALL;

	Port5()->OUT &= ~col;
}

void Led::turnOff(unsigned char col)
{
	Port5()->OUT |=col;
}

void Led::blink(unsigned char col)
{
	Port5()->OUT ^= col;
}

void Led::assign(char value)
{
	uint8 v = Port5()->OUT;
	v &= ~ALL;
	v |= ((~value)<<4); //value will be inverted
	Port5()->OUT = v;
}
