/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:	Andre Sieber, Stefan Nuernberger
 */
#include "reflex/io/SHT11.h"
#include "reflex/interrupts/InterruptLock.h"

using namespace reflex;

/**
 * constructor
 */
SHT11::SHT11(): PortInterruptHandler(PortInterruptHandler::PORT1,
				PortInterruptHandler::PIN5, PowerManageAble::SECONDARY),
				firstRequest(0), nextRequest(0), status(0),
				timer(VirtualTimer::ONESHOT),
				sendCommandFunctor(*this),
				getResultFunctor(*this)
{
	timer.connect_output(&sensorReady);
	sensorReady.init(&sendCommandFunctor); // register Activity for timer event
	resultAvailable.init(&getResultFunctor); // register Activity for interrupt

	// initialize Port
	Port1()->SEL &= ~(CLK | DATA | POWER); // set pins to I/O functionality
	Port1()->IES |= DATA; // select falling edge for DATA line interrupt
	Port1()->IE &= ~(CLK | DATA | POWER); // disable interrupts on all pins
	Port1()->DIR |= POWER; // make power an output pin
	Port1()->OUT &= ~POWER; // deactivate power line (switch sensor off)

	// external port interrupts still work on deepest sleep mode
	setSleepMode(mcu::LPM4);
}

/**
 * init
 * connect the data outputs of the sensor
 * @param temp Temperature output
 * @param hum Humidity output
 */
void SHT11::init(Sink1<uint16>* temp, Sink1<uint16>* hum) {
	temperature = temp;
	humidity = hum;
}

/**
 * assign
 * implements Sink1 interface
 * used to receive measurement requests from application
 * activates the sensor and sets a timer for measurement start
 * @param request bitmask indicating desired operation
 * (see enum Operation)
 */
void SHT11::assign(uint8 request)
{
	// critical, update ringbuffer
	InterruptLock lock();
	
	// enable sensor if deactivated
	if (nextRequest == firstRequest) {
		// activate sensor (power up)
		Port1()->OUT |= POWER;
		// set timer
		timer.set(SHT11_POWERUP_MSEC);
	}

	// copy request (will be evaluated by run())
	this->request[nextRequest++] = request;
	// update nextRequest
	if (nextRequest == SHT11_MAX_REQUESTS)
		nextRequest = 0;
}

/**
 * sendCommand
 * This method issues a command to the sensor and enables the
 * interrupt for the result
 */
void SHT11::sendCommand()
{
	// critical, get request from ringbuffer
	if (true) {
		InterruptLock lock();
		currentRequest = this->request[firstRequest];
	}

	// compute status register from request
	unsigned char newStatus =
			  (((currentRequest & HEATER) && true) << SR_HEATER)
			| (((currentRequest & LOW_RES) && true) << SR_RESOLUTION)
			| (((currentRequest & NO_CALIBRATE) && true) << SR_OTP_RELOAD);

	// write status (if changed)
	if (newStatus != status) {
		initTransmit();
		writeByte(CMD_SR_WRITE);
		if (readAck()) goto err_out;
		writeByte(newStatus);
		if (readAck()) goto err_out;

		status = newStatus;
	}

	initTransmit();
	// transmit measure command
	if (currentRequest & HUMIDITY) { // measure humidity
		writeByte(CMD_MEASURE_HUM);
	} else { // measure temperature
		writeByte(CMD_MEASURE_TEMP);
	}
	// read acknowledgment
	if (readAck()) goto err_out;

	// wait for result (until DATA is low)
	// NOTE: we are notified by interrupt
	Port1()->DIR &= ~DATA; // data is input
	// enable interrupt
	switchOn();

	return;

err_out:
	// return error value
	if (currentRequest & HUMIDITY) {
		if (humidity)
			humidity->assign(SENSOR_ERROR);
	} else {
		if (temperature)
			temperature->assign(SENSOR_ERROR);
	}
	
	// handle next request or deactivate sensor
	handleNextRequest();
}

/**
 * handle
 * implements PortInterruptHandler
 */
void SHT11::handle() {
	// disable Interrupt
	switchOff();
	// clear InterruptFlag
	Port1()->IFG &= ~DATA;
	// announce result available
	resultAvailable.notify();
}

/**
 * getResult
 * This method reads the result from the sensor after an interrupt
 */
void SHT11::getResult() {
	uint16 result = 0;
	result |= (readByte() << 8); // MSB
	writeAck(); // ack first byte
	result |= readByte(); // LSB
	// skip ack since we don't need CRC

	// return value
	if (currentRequest & HUMIDITY) {
		if (humidity) humidity->assign(result);
	} else {
		if (temperature) temperature->assign(result);
	}

	// handle next request or deactivate sensor
	handleNextRequest();
}

/**
 * handleNextRequest
 * advance request pointer (ringbuffer) and
 * trigger next measurement or deactivate sensor
 * if no request is left
 */
void SHT11::handleNextRequest() {
	// critical, update ringbuffer
	InterruptLock lock();

	++firstRequest; // this request is done
	if (firstRequest == SHT11_MAX_REQUESTS)
		firstRequest = 0;
	if (nextRequest == firstRequest) {
		Port1()->OUT &= ~POWER; // deactivate sensor
		status = 0; // reset status
	} else {
		// trigger sendCommandFunctor for next measurement
		sendCommandFunctor.trigger();
	}
}

/**
 * enable
 * PowerManagement enable function
 * enables interrupt on DATA port
 */
void SHT11::enable() {
	Port1()->IFG &= ~DATA; // reset all pending interrupts
	Port1()->IE |= DATA;
}

/**
 * disable
 * PowerManagement disable function.
 * disables interrupt on DATA port
 */
void SHT11::disable() {
	Port1()->IE &= ~DATA;
}

/**
 * initTransmit
 * sends the "start transmission" sequence
 *       ____         ____
 *  DATA     |_______|
 *          ___     ___
 *  CLK  __|   |___|   |__
 */
void SHT11::initTransmit()
{
	Port1()->DIR |= (DATA | CLK); // set data and clock to output
	
	//initial state
	Port1()->OUT &= ~CLK; //CLK=0
	Port1()->OUT |= DATA; //DATA=1
	Port1()->OUT |= CLK; //CLK=1
	Port1()->OUT &= ~DATA; //DATA=0
	Port1()->OUT &= ~CLK; //CLK=0
	Port1()->OUT |= CLK; //CLK=1
	Port1()->OUT |= DATA; //DATA=1
	Port1()->OUT &= ~CLK; //CLK=0
}

/**
 * readAck
 * tries to read an acknowledgment.
 * @return 0 if everything went well
 */
int SHT11::readAck()
{
	Port1()->DIR |= CLK; // set clock to output
	Port1()->DIR &= ~DATA; // set data to input

	Port1()->OUT |= CLK; // CLK = 1;
	// DATA should be low now
	if(Port1()->IN & DATA) return -1;
	Port1()->OUT &= ~CLK; // CLK = 0;

	return 0;
}

/**
 * writeAck
 * write an Acknowledgment
 */
void SHT11::writeAck()
{
	Port1()->DIR |= (CLK | DATA); // set clock and data to output
	
	Port1()->OUT &= ~DATA; // DATA = 0;
	Port1()->OUT |= CLK; // CLK = 1;
	Port1()->OUT &= ~CLK; // CLK = 0;
}

/**
 * readByte
 * read a single byte
 * @return the received byte
 */
unsigned char SHT11::readByte()
{
	unsigned char bit, byte = 0;

	Port1()->DIR |= CLK;	// clock is output
	Port1()->DIR &= ~DATA; // data is input

	for (bit = 0x80; bit != 0; bit >>= 1) // shift bit for masking
	{
		Port1()->OUT |= CLK; // CLK = 1
		if (Port1()->IN & DATA) byte |= bit; // read bit
		Port1()->OUT &= ~CLK; // CLK = 0;
	}

	return byte;
}

/**
 * writeByte
 * write a single byte
 * @param byte the byte to write
 */
void SHT11::writeByte(char byte)
{
	unsigned char bit;
	Port1()->DIR |= (DATA | CLK); // data and clock as output

	for (bit = 0x80; bit != 0; bit >>= 1) //shift bit for masking
	{
		// set data line
		if (bit & byte)
			Port1()->OUT |= DATA;	// DATA = 1
		else
			Port1()->OUT &= ~DATA;	// DATA = 0

		// let the clock tick
		Port1()->OUT |= CLK;	// CLK=1
		Port1()->OUT &= ~CLK;	// CLK=0;
    }
}
