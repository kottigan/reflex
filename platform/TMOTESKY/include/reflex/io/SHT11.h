#ifndef SHT11_H
#define SHT11_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	SHT11
 *
 *	Author:		Andre Sieber, Stefan Nuernberger
 *
 *	Description:	Driver for the temperature/humidity sensor SHT11
 *	    Communication with this sensor requires implementation of a
 *      protocol, which is very similar but not compatible to I2C 
 *      standard. Therefore tight integration in controller
 *      context is done. The Sensor is connected using the P1.5 (DATA)
 *      and P1.6 (CLK) ports. Power for sensor comes from P1.7
 *
 *      ATTENTION!:
 *      Driver does power-on at request time. A virtual timer is used to
 *      generate a sensor ready interrupt. A port interrupt is used to
 *      signal the availability of a measurement result. The power state
 *      of the device is managed exclusively by the driver (SECONDARY).
 *      Thus the device does not need to be put in any PowerManager group.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 */

#include "reflex/types.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/interrupts/PortInterruptHandler.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/io/Port.h"

// we need to check conf.h for redefinition of SHT11_MAX_REQUESTS
#include "conf.h"

// you may define SHT11_MAX_REQUESTS in conf.h yourself
#ifndef SHT11_MAX_REQUESTS
#define SHT11_MAX_REQUESTS 2
#endif

namespace reflex {

class SHT11:
	public PortInterruptHandler,
	public Sink1<uint8>
 {
private:

	uint8 currentRequest; // currently active request (value)
	uint8 request[SHT11_MAX_REQUESTS]; // pending requests
	uint8 firstRequest; // first pending request (index)
	uint8 nextRequest; // value for next request (index)
	uint8 status; // last used status register value
	//volatile Port port; // pointer to physical sensor port

	Event sensorReady; // triggered by the timer
	Event resultAvailable; // triggered by interrupt handler

	// timer to signal ready state -> measurement start
	VirtualTimer timer;

public:

	// outputs for temperature and humidity
	Sink1<uint16>* temperature;
	Sink1<uint16>* humidity;

	/**
	 * operation of sensor
	 * NOTE: TEMPERATURE is zero! Least significant bit is used to
	 * distinguish between measurement types.
	 * HUMIDITY sets LSB, TEMPERATURE leaves it zero.
	 * BIG FAT WARNING: DO NOT USE "TEMPERATURE" TO TEST FOR A CONDITION!
	 * IT WILL ALWAYS EVALUATE TO FALSE! ALWAYS USE "HUMIDITY" INSTEAD!
	 * The other values might be combined as desired.
	 */
	enum Operation {
		TEMPERATURE = 0x00, // measure temperature
		HUMIDITY = 0x01, // measure humidity
		HEATER = 0x02, // use built-in heater
		LOW_RES = 0x04, // use low resolution
		NO_CALIBRATE = 0x08 // do not calibrate from OTP memory
	};

	/**
	 * constructor
	 */
	SHT11();

	/**
	 * assign
	 * implements Sink1 interface
	 * used to receive measurement requests from application
	 * activates the sensor and sets a timer for measurement start
	 * @param request bitmask indicating desired operation
	 * (see enum Operation)
	 */
	virtual void assign(uint8 request);
	
	/**
	 * init
	 * connect the data outputs of the sensor
	 * @param temp Temperature output
	 * @param hum Humidity output
	 */
	void init(Sink1<uint16>* temp, Sink1<uint16>* hum);

	/**
	 * handle
	 * implements PortInterruptHandler
	 * It is responsible for resetting the hardware to allow
	 * further interrupts.
	 */
	virtual void handle();

	/**
	 * enable
	 * PowerManagement enable function.
	 * enables interrupt on DATA port
	 */
	virtual void enable();

	/**
	 * disable
	 * PowerManagement disable function.
	 * disables interrupt on DATA port
	 */
	virtual void disable();
	 	  
private:

	/**
	 * sendCommand
	 * This method issues a command to the sensor and enables the
	 * interrupt for the result
	 */
	void sendCommand();

	/**
	 * getResult
	 * This method reads the result from the sensor after an interrupt
	 */
	void getResult();

	/** activity triggered by the sensorReady event */
	ActivityFunctor<SHT11, &SHT11::sendCommand> sendCommandFunctor;
	/** activity triggered by resultAvailable event */
	ActivityFunctor<SHT11, &SHT11::getResult> getResultFunctor;

	/**
	 * handleNextRequest
	 * advance request pointer (ringbuffer) and
	 * trigger next measurement or deactivate sensor
	 * if no request is left
	 */
	void handleNextRequest();

	/**
	 * initTransmit
	 * sends the "start transmission" sequence
	 */
	void initTransmit();

	/**
	 * readAck
	 * tries to read an acknowledgment.
	 * @return 0 if everything went well
	 */
	int readAck();
	
	/**
	 * writeAck
	 * write an Acknowledgment
	 */
	void writeAck();
	
	/**
	 * readByte
	 * read a single byte
	 * @return the received byte
	 */
	unsigned char readByte();

	/**
	 * writeByte
	 * write a single byte
	 * @param byte the byte to write
	 */
	void writeByte(char byte);

	enum BitmasksAndPositions {
		// those are bitmasks for commands
		CMD_SR_READ = 0x07, // read status register
		CMD_SR_WRITE = 0x06, // write status register
		CMD_MEASURE_TEMP = 0x03, // measure temperature
		CMD_MEASURE_HUM	 = 0x05, // measure humidity
		CMD_SOFT_RESET = 0x1e, // interface and status reg. reset (>11ms wait)

		// those mark bit positions in the status register
		SR_RESOLUTION = 0, // SR resolution bit
							// 0 = high res (12 bit hum / 14 bit temp)
							// 1 = low res (8 bit hum / 12 bit temp)
		SR_OTP_RELOAD = 1, // SR calibration reload bit
							// 0 = reload configuration from OTP memory
							// 1 = do not reload from OTP memory
		SR_HEATER = 2, // SR heater bit (0 = off)
		SR_LOWBATTERY = 6, // SR low battery bit (0 = OK)

		// those identify the pins on the port
		DATA = 0x20, // P1.5 - Serial data
		CLK = 0x40,  // P1.6 - Serial clock input
		POWER = 0x80 // P1.7 - Power supply
	};

	enum Timing {
		SHT11_POWERUP_MSEC = 12 // milliseconds from power to ready
	};

	enum Errors {
		SENSOR_ERROR = 0xffff // default sensor error
	};
};

}

#endif
