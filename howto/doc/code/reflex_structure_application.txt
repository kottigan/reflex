sample/
|-- Sources.mk
|-- doc
|-- include
|   |-- UserStatusBlock.h
|   |-- sample.h
|-- src
|   |-- main.cc
|   |-- sample.cc
`- platform
    `-- TMOTESKY
        |-- Sources.mk
        |-- bin
        |   `-- Makefile
        `-- include
            |-- NodeConfiguration.h
            `-- conf.h
