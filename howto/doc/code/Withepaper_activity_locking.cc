void EDFScheduler::unlock(EDFActivity* act)
{
	InterruptLock lock; //protect this method from interrupts

	act->locked = false; //remove lock

	//if this activity is NOT in scheduling process
	if(	act->status == EDFActivity::IDLE ) {
		//if it must be scheduled
		if(act->rescheduleCount){
			//schedule it as usual
			act->rescheduleCount--;
			enterScheduling();
			act->setDeadline();
			toSchedule.enque(act);
			act->status = EDFActivity::SCHEDULED;
			leaveScheduling();
		}
	}
}
