class NodeConfiguration {
public:
  NodeConfiguration()
  {
    //connection of components
    a.init(c.input1);
    b.output1 = &(c.input1);

    //parameterization of components
    b.setPriority(MAX_PRIORITY);
  }

  //instantiation
  ComponentA a;
  ComponentB b;
  ComponentC c;
};
