#include "SleepManager.h"
#include "NodeConfiguration.h"
using namespace reflex;

SleepManager::SleepManager()
       	  : vtimer(VirtualTimer::PERIODIC)
{
	ready.init(this);
	vtimer.init(ready);
	vtimer.set(1500);
	currentMode = (reflex::PowerManager::PowerGroups) SLEEP;
}


void SleepManager::init(Event *eventOut, OutputChannel *out)
{
	this->eventOut = eventOut;
	this->out = out;
}


void SleepManager::run()
{
	if (currentMode == SLEEP)
	  {	//change mode to awake
  		getApplication().powerManager.switchMode((reflex::PowerManager::PowerGroups) AWAKE,
						  (reflex::PowerManager::PowerGroups) SLEEP);
		currentMode = (reflex::PowerManager::PowerGroups) AWAKE;
		out->write("AWAKE");
    	} else {
	  //change mode to sleep
  		getApplication().powerManager.switchMode((reflex::PowerManager::PowerGroups) SLEEP,
						  (reflex::PowerManager::PowerGroups) AWAKE);
		currentMode = (reflex::PowerManager::PowerGroups) SLEEP;
   		eventOut->notify();
		out->write("SLEEP");
  	}
  	out->writeln();
}
