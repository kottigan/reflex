class NodeConfiguration {
public :
  NodeConfiguration ()
    {
      //connection
      a.init(c.input1);
      b.init(c.input1);
  
      // parametrisation of commponents
      b.setPriority( MAX_PRIORITY );
    }
 
  // instantiation of components
  ComponentA a;
  ComponentB b;
  ComponentC c;
};
