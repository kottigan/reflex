class NodeConfiguration : public System 
{
public:
	NodeConfiguration() 
		: System()
		, pool(0)
		, serial(1,SerialRegisters::B19200)
	{
		//wires components together
		clock.connectToOutput(&hello.timer);
	}
	
	PoolManager poolManager; ///< manages different pools
	SizedPool<IOBufferSize,NrOfStdOutBuffers> pool; ///< a pool of bufferobjects
	Serial serial;	///< the driver for the serial interface
	HelloWorld hello; ///< here the famous HelloWorld message is produzed
};
