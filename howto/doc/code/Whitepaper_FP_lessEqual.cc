bool EDFActivity::lessEqual(EDFActivity& right)
{ 
  Time systemTime = clock.getTime ();

  Time leftOffset = this -> deadline - systemTime ;
  Time rightOffset = right.deadline - systemTime ;

  return ( leftOffset <= rightOffset );
}
