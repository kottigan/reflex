#ifndef FreeList_h
#define FreeList_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	FreeList
 *
 *	Author:		Karsten Walther
 *
 *	Description:	List management of memory blocks.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/data_types/ChainLink.h"

#include "reflex/memory/new.h"

namespace reflex{
class FreeList;
}

/**	get memory from Pool.
 *
 *	@param 	size	buffersize
 *	@param	pool	the manager which owns the Buffers
 *  @return returns a pointer to a buffer or 0 if no buffer is available
 **/
void* operator new(size_t, reflex::FreeList*pool) throw(); //inlined

namespace reflex {

/** This class implements a Freelist for buffers, which uses the data space
 *  for interconnection of free buffers.
 */
class FreeList {
protected:
	friend void* ::operator new(size_t,FreeList*)throw();
	friend class Buffer;

	/** Pointer to the first element of the freelist.
	 */
	data_types::ChainLink* first;

	/** Initializes the pointer to the freelist to 0
	 */
	FreeList() :first(0) {}

	/** Returns pointer to the first memory block in freelist.
	 *
	 *  @return : pointer to a memory block
	 */
	void* alloc();

	/** Inserts the freed object in the freelist, an reuses space of the
	 *  object for next pointer
	 *
	 *  @param elem object which to be freed
	 */
	void free(void* mem)
	{
		InterruptLock lock;
		/* avoid strict aliasing rules violation (ticket #147)
		The aliasing rules were designed to allow compilers more aggressive optimization.
		for further informations see the "Casting does not work as expected when optimization is turned on."
		section on http://gcc.gnu.org/bugs/#nonbugs_c */
		data_types::ChainLink* elem = new (mem) data_types::ChainLink(first);
		first = elem;
	}
};

} //end namespace reflex

inline
void* operator new(size_t, reflex::FreeList*pool) throw() {
	return pool->alloc();
}


#endif

