#ifndef OutputChannel_h
#define OutputChannel_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	OutputChannel
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Formated output with buffer management
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/sinks/Sink.h"
#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"

namespace reflex {

/** Provides formatted output, and makes buffermanagement for output
 *  messages. Send is implicitely initiated in writeln(),
 *  alternatively flush() can be called.
 *  The ouputchannel has no event flow inputs, it provides only a method
 *  based interface, since this is better suited for output purposes. Therefore
 *  the user is responsible for synchronization. Alternatively the user can
 *  instantiate multiple outputchannels, with exactly one writer for each
 *  channel.
 *  OutputChannel requires the definition of IOBufferSize and NrOfStdOutBuffers
 *  in conf.h. Therefore all channels have the same amount of buffers of the
 *  same size.
 *
 *	@author Karsten Walther
  
 */
class OutputChannel {
public:
	/** Allocates first buffer.
	 */
	OutputChannel(Pool* pool): pool(pool),buffer(NULL)
	{}

	/** Sets the pointer to a subsequent component, which handle buffers.
	 *
	 *  @param device : pointer to a subsequent component
	 */
	void init(Sink1<Buffer*>* device);

	/** Writes one character into the buffer.
	 *
	 *  @param c : the character.
     */
	void write(char c);

	/** Writes a string into the buffer, end of string character is 0
	 *
	 *  @param data : the string to write
	 */
	void write(const char* data);
	void write(char* data)
	{
		write((const char*)data);
	}

	/** Writes binary data into the buffer.
	 *
	 *  @param data : pointer to begin of the data
	 *  @param size : length of data in bytes
	 */
	void write(const void* data, unsigned size);

	/** Writes a positive number into the buffer, while the number is formatted
	 *  to base 10. Leading zeros are discarded.
	 *
	 *  @param data : the number to write
	 */
	void write(unsigned data);

	/** Writes a positive fixed length number into the buffer, number base is
	 *  10. Prints leading zeros.
	 *
	 *  @param value : the unsigned number to write
	 *  @param digits : the fixed output length
	 */
	void write(unsigned value, char digits);

	/** Writes a signed number into the buffer, number base is 10. Leading
	 *  zeros are discarded.
	 *
	 *  @param data : the number to write
	 */
	void write(int data);

	/** Writes an address in hexadecimal representation into the buffer.
	 *
	 *  @param address : the address to write
	 */
	void write(void* address);

	/** Writes an endline '\r\n' into the buffer and forwards the buffer to
	 *  the device.
	 */
	void writeln();

	/** Method writes a binary object/variable of any type into the buffer
	 *
	 *  @param object : object/variable to write
	 */
	template<typename T>
	void writeObject(T* object);

	/** Forwards the buffer as it is to the device.
	 */
	void flush();

private:
	/** Pointer to a subsequent component.
	 */
	Sink1<Buffer*> *device;

	/** The pool of buffers used by this channel. Uses IOBufferSize and
	 *  NrOfStdOutBuffers, which must be defined in conf.h .
	 */
	Pool *pool;

	/** The current buffer used for writing.
	 */
	Buffer *buffer;

	/** methods allocates a new buffer for writing.
	 */
	void getBuffer();

	/** Forwards the current buffer.
	 */
	void sendBuffer();

};



template<typename T>
void OutputChannel::writeObject(T* object)
{
	write((const void*)object,sizeof(T));
}

} //end namespace reflex

#endif

