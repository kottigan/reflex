/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	 EEPROM_24xx512
 *
 *	Author:	     Stefan Nuernberger
 *
 *	Description: Driver for the 512kBit EEPROM Family 24AA512/24LC512/24FC512
 *               using the generic I2C interface.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/
#ifndef REFLEX_DEVICES_EEPROM_24xx512_H
#define REFLEX_DEVICES_EEPROM_24xx512_H

#include "reflex/types.h"
#include "reflex/io/I2C.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/scheduling/ActivityFunctor.h"

namespace reflex {
namespace devices {

/* EEPROM_24xx512
 * This offers read/write access via generic I2C to the family
 * of 512kBit EEPROMs 24AA512/24LC512/24FC512. Those devices only
 * differ in electrical characteristics and some minor points
 * that do not affect the driver.
 * NOTE: 512kBit == 64kbyte == 65536 byte memory available on device
 *
 * This driver offers an event based interface with notification
 * after completion of a read or write operation.
 */
class EEPROM_24xx512 : public I2C_Device
{
private:
    /* some constants used throughout the driver */
    enum Constants {
        DEFAULT_I2C_ADDRESS = 0x50 // can be changed through I2C_Device::setAddress(uint8)
        ,MAX_ADDRESS = 0xffff // highest address in EEPROM memory address space (change to 0x7fff for 256kBit)
        ,PAGE_SIZE = 0x40 // page size for write operations
        ,MAX_POLL_COUNT = 0xff // maximum number of retries in acknowledge polling
    };

    /* Seeker - internal class
     * set the read/write position that will be used on next read/write
     * through Event Channel based interface (Sink1).
     * NOTE: This operation does not involve communication with the EEPROM
     */
    class Seeker : public Sink1<uint16> {
    private:
        uint16 offset; // the current read/write position
    public:
        Seeker() : offset(0) {}
        /* assign
         * set the new read/write offset
         * @param offset - new read/write position
         */
        virtual void assign(uint16 offset) {
            InterruptLock lock;
            this->offset = offset & MAX_ADDRESS;
        }
        /* getOffset
         * return current offset value
         */
        uint16 getOffset() {
            InterruptLock lock;
            return offset;
        }
    };

public:
    /* constructor
     * set default I2C address and stuff...
     */
    EEPROM_24xx512();

    /* i2c_configure
     * called after the I2C Master has been set. Should be used
     * for device initialization, which is not needed for this
     * EEPROM device.
     */
    virtual void i2c_configure() {}

    /* init
     * connect the sink that is triggered when a read or write operation
     * has been finished successfully or on error.
     * @param success - sink that is notified after completion of read/write
     * @param error - sink that is notified on error
     */
    void init(Sink0 *success, Sink0 *error) {
        this->success = success;
        this->error = error;
    }

    /* Event interface */
    Seeker wseek; // adjust write position: wseek->assign(offset)
    Seeker rseek; // adjust read position: rseek->assign(offset)
    SingleValue2<unsigned char*, uint16> read; // read->assign(buf, len)
    SingleValue2<unsigned char*, uint16> write; // write->assign(buf, len)

private:
    Sink0 *success; // triggered on successful read or write
    Sink0 *error; // triggered on error

    /* read_bytes
     * Read bytes from the EEPROM. This uses random access/sequential read.
     * NOTE: The read offset automatically wraps around after MAX_ADDRESS has been
     *       reached. Reading continues from 0x0000 in EEPROM address space.
     */
    void read_bytes();

    /* write_bytes
     * Write bytes into EEPROM. The device can only handle PAGE_SIZE bytes writes
     * in a single sequential write operation, but do not worry because this method
     * is aware of the problem and will handle all the inconvenient stuff for you.
     * Just as with reading, writing will continue at the start of EEPROM memory when
     * the highest byte has been written.
     */
    void write_bytes();

    /* start_write_poll
     * implements device acknowledge polling before the next write operation.
     * This maximizes bus throughput as described in the data sheet.
     * NOTE: To avoiod deadlocks if there is no memory device, the polling
     *       will stop after a predefined number of tries.
     * @return bool - true if acknowledge received, false otherwise (#tries exceeded)
     */
    bool start_write_poll();

    /* Activity Functors for read and write */
    ActivityFunctor<EEPROM_24xx512, &EEPROM_24xx512::read_bytes> readFunc;
    ActivityFunctor<EEPROM_24xx512, &EEPROM_24xx512::write_bytes> writeFunc;
};

} // ns devices
} // ns reflex

#endif // REFLEX_DEVICES_EEPROM_24xx512_H
