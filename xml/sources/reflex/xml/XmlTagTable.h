#ifndef XMLTAGTABLE_H_
#define XMLTAGTABLE_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlTagTable
 *
 *	Description: Generic base class for XML tag tables.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
//#include "conf.h"
#include "reflex/types.h"

namespace reflex
{
namespace xml
{
class XmlHandler;

/**
 * XmlStreamReader and XmlStreamWriter need a specific XmlHandler for each XML tag
 * they have to process. XmlHandlers are identified by their names and thus, a lookup
 * mechanism is needed.
 * XmlTagTable contains all XML tags as Entries (string and handler address), which
 * are to be located in flash memory. Once the parser reads a tag name, it will ask
 * XmlTagTable for an appropriate XmlHandler and then call its virtual handler function.
 *
 * XmlTagTable is a very simple lookup mechanism using a linear
 * search and serves as an example interface for faster lookup algorithms.
 * Although the speed seems to be acceptable even with 100 different tags.
 *
 * <h3>Usage</h3>
 * Create an instance of XmlTagTable and register the first element of
 * of a XmlTagTable::LookupEntry list. This list must be terminated by a null-entry!
 * As a result of static const, the tag list will be linked to flash memory.
 * \code
 * // Header file Application.h
 * class Application {
 * public:
 * 	Application();
 *
 * private:
 *	reflex::xml::XmlTagTable tagTable;
 *	static const reflex::xml::XmlTagTable::LookupEntry tags[];
 * };
 *
 * // Source file Application.cc
 * using namespace reflex;
 * using namespace xml;
 * namespace reflex
 * 	{
 *		extern reflex::NodeConfiguration system;
 *	}
 *
 * const XmlTagTable::LookupEntry Applcation::tags[] IN_FLASH =
 *	{
 *		{ "state", &system.application.xml_stateHandler },
 *		{ "commandy", &system.application.xml_commandHandler },
 *		// More handlers/channels whatever...
 *		{ "", 0 }
 *	};
 *
 * Application::Application(): tagTable(&tags[0])
 * {
 * }
 * \endcode
 *
 * The IN_FLASH macro is defined by MemoryPolicy, is explicitly needed for the
 * AVR and will be ignored on any other platform.
 *
 * @see XmlEnvironment as convenience class which already includes XmlTagTable.
 */
class XmlTagTable
{
public:
	/**
	 * A container for a tag-string and a pointer to XmlHandler.
	 * This is a helper class for XmlTagTable.
	 * All entries should be located as a consecutive list in flash memory.
	 */
	struct LookupEntry
	{
#ifndef XML_MAX_TAG_LENGTH
#define XML_MAX_TAG_LENGTH 20
#endif


		enum
		{
			MaxTagLength = XML_MAX_TAG_LENGTH
		};

		/* Tag identifier, \0-terminated */
		const char tag[MaxTagLength + 1];
		/* XmlHandler */
		XmlHandler* const handler;
	};

	/**
	 * Constructs the table object and determines the table's size.
	 */
	XmlTagTable(const LookupEntry* const firstTag);

	/**
	 * Identifies a handler for a given tag.
	 */
	XmlHandler* handlerByName(const char* tag) const;

	/**
	 * Identifies an entry for a given tag.
	 */
	const LookupEntry* entryByName(const char* tag) const;

protected:
	static bool equal(const char* ram, const char* rom);
	static const LookupEntry* findLast(const LookupEntry* first);

	const LookupEntry* const first;
	const LookupEntry* const last;
};

}
}

#endif /* XMLTAGTABLE_H_ */
