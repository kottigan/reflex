#ifndef XMLREADER_H_
#define XMLREADER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlReader
 *
 *	Description: Parses incoming XML streams using XmlHandlers
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
//#include "conf.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Fifo.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/xml/XmlTagTable.h"

namespace reflex
{
class Buffer;

namespace xml
{
class XmlHandler;

/**
 * Splits a XML stream into tokens and delegates them as events to XmlHandler objects.
 *
 * Like the SAX-API, XmlReader converts an XML stream into events and delegates them to
 * handlers. But XmlReader is more lightweight. Neither does it recognize attributes
 * nor name spaces. Unlike SAX, XmlReader calls a separate XmlHandler for each tag
 * which is identified by a lookup mechanism in XmlTagTable.
 *
 * XmlReader understands plain XML tags. When reading a message like
 * 	\code
 * 	...
 * 	<relais>on</relais>
 * 	...
 * 	\endcode
 * 	XmlReader asks XmlTagTable for an appropriate handler that has been registered as "relais"
 * 	and forwards the string "on" to this handler.
 * 	Additionally, XmlReader supports a poll mechanism to query the content of XML tags.
 * 	When receiving "<relais></relais>", XmlReader notifies XmlWriter to
 * 	generate an answer in the form of "<relais>on</relais>". Note, that this is not the same
 * 	as "<relais/>" which is correctly interpreted as empty tag.
 * 	Maybe, this "odd" behavior will be replaced in the future by a dedicated XmlHandler.
 *
 * 	@see XmlWriter the corresponding part to send XML data.
 * 	@see XmlHandler the event-handler interface.
 *  @see XmlTagTable the lookup table for XmlHandler.
 *
 */
class XmlReader
{
	typedef XmlTagTable::LookupEntry LookupEntry;
public:
#ifndef XML_INCOMING_BUFFER_SIZE
#define XML_INCOMING_BUFFER_SIZE 20
#endif
	enum
	{
		IncomingBufferSize = XML_INCOMING_BUFFER_SIZE
	};

	/** Input for single characters, e.g. from the serial interface. */
	Fifo1<uint8, IncomingBufferSize> input_char;
	/** Input for whole buffers, e.g. from a radio or ethernet */
	Queue<Buffer*> input_buffer;

	/** Output to XmlWriter if polling is desired. */
	Sink1<const LookupEntry*>* output_pollRequest;

	XmlReader();

	/**
	 * Sets all necessary resources. Call this method right after
	 * construction.
	 * @param table the table where to search for XmlHandlers.
	 * @param pool where exactly 2 buffers are needed from.
	 */
	void init(XmlTagTable* table, Pool* pool);

private:
	/**
	 * Internal state machine states. It is always reset after
	 * every tag into the state XmlReader::ScanForElement.
	 */
	enum State
	{
		ScanForElement,
		ElementOpened,
		StartElement,
		Attributes,
		StartElementClosed,
		StartElementClosedNoPoll,
		EmptyElement,
		Content,
		EndElement,
		ElementOpenedAndMaybePoll,
		IgnoreUntilElementEnd
	};

	void parse(char c);
	void handleStartElement();
	void handleContent();
	void handlePoll();
	void handleEndElement();
	void handleEmptyElement();

	void run_handleBuffer();
	ActivityFunctor<XmlReader, &XmlReader::run_handleBuffer> act_handleBuffer;
	void run_handleChar();
	ActivityFunctor<XmlReader, &XmlReader::run_handleChar> act_parseChar;

	State state;
	XmlTagTable* table;
	XmlHandler* handler;
	Buffer* data;
	Buffer* tag;
};

}
}
#endif /* XMLSTREAMREADER_H_ */
