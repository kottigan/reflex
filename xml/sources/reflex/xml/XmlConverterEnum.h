#ifndef XMLENUMCONVERTER_H_
#define XMLENUMCONVERTER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlEnumConverter
 *
 *	Description: Converter between enumerations strings and their integer representation
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "conf.h"
#include "reflex/memory/Buffer.h"
#include "reflex/xml/MemoryPolicy.h"

namespace reflex
{
namespace xml
{
/**
 * Converter between enumerations strings and their integer representation.
 * This is a policy class for XmlHandler implementations like XmlChannel or
 * XmlContainer, but can also be used directly. The main methods are
 * stringToRaw() and rawToString().

 * Every XmlHandler for string enumerations needs a lookup table
 * to translate strings into integer enumerations and the other
 * way round. This table is just a unordered list of XmlConverterEnum::LookupEntry,
 * and has to be terminated by a null-entry. after XmlConverterEnum
 * has been constructed. Set the table with setLookupTable().
 *
 * @see XmlChannel and XmlContainer for usage.
 *
 */
template<typename DataT>
class XmlConverterEnum
{
public:
	struct LookupEntry
	{
#ifndef XML_MAX_ENUM_LENGTH
#define XML_MAX_ENUM_LENGTH 20
#endif

		enum
		{
			MaxEnumLength = XML_MAX_ENUM_LENGTH
		};

		const DataT id;
		const char string[MaxEnumLength];
	};

	/**
	 * Sets the lookup table for enumeration strings after XmlConverterEnum
	 * has been constructed.
	 */
	void setLookupTable(const LookupEntry* firstEntry);

	/**
	 * Converts the enum string into the raw enum value.
	 */
	bool stringToRaw(char* string, DataT& result);

	/**
	 * Converts the raw enum value into its string representation.
	 */
	bool rawToString(DataT data, Buffer* buffer);

private:
	const LookupEntry* entryByName(char* xmlString);

	/**
	 * Locate matching entry by enum value
	 */
	const LookupEntry* entryById(DataT enumValue);

	/**
	 * Copy string from flash into the buffer without the trailing \0
	 */
	static void strcopy(Buffer* dest, const char* src);

	/**
	 * Compares a string in RAM to a string in Flash memory (or whatever is
	 * defined in XmlEnumConverter::MemoryPolicy.
	 */
	static bool equal(const char* ram, const char* rom);

	const LookupEntry* lookupTable;
};

template<typename DataT>
void XmlConverterEnum<DataT>::setLookupTable(
		const XmlConverterEnum<DataT>::LookupEntry* firstEntry)
{
	this->lookupTable = firstEntry;
}

template<typename DataT>
bool XmlConverterEnum<DataT>::stringToRaw(char* string, DataT& result)
{
	const LookupEntry* entry = this->entryByName(string);
	if (entry)
	{
		result = MemoryPolicy::read(&entry->id);
	}
	return (entry != 0);
}

template<typename DataT>
bool XmlConverterEnum<DataT>::rawToString(DataT data, Buffer* buffer)
{
	const LookupEntry* entry = this->entryById(data);
	if (entry)
	{
		this->strcopy(buffer, &entry->string[0]);
	}
	return (entry != 0);
}

template<typename DataT>
const typename XmlConverterEnum<DataT>::LookupEntry* XmlConverterEnum<DataT>::entryByName(
		char* xmlString)
{
	for (const LookupEntry* entry = lookupTable; MemoryPolicy::read(&entry->string[0]) != '\0'; entry++)
	{
		const char* lookupString = &entry->string[0];

		if (equal(xmlString, lookupString))
		{
			return entry;
		}
	}
	return 0;
}

/**
 * Locate matching entry by enum value
 */
template<typename DataT>
const typename XmlConverterEnum<DataT>::LookupEntry* XmlConverterEnum<DataT>::entryById(
		DataT enumValue)
{
	const LookupEntry* entry = lookupTable;
	while (MemoryPolicy::read(&entry->id) != enumValue)
	{
		if (MemoryPolicy::read(&entry->string[0]) != '\0')
		{
			entry++;
		}
		else
		{
			return 0;
		}
	}
	return entry;
}

/**
 * Copy string from flash to buffer without \0
 */
template<typename DataT>
void XmlConverterEnum<DataT>::strcopy(Buffer* dest, const char* src)
{
	while (char c = MemoryPolicy::read(src))
	{
		dest->write(c);
		src++;
	}
}

template<typename DataT>
bool XmlConverterEnum<DataT>::equal(const char* ram, const char* rom)
{
	while (*ram == MemoryPolicy::read(rom))
	{
		if (*ram == '\0')
		{
			return true;
		}
		ram++;
		rom++;
	}
	return false;
}

}
}
#endif /* XMLENUMCONVERTER_H_ */
