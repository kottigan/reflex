[![Build Status](https://idun.informatik.tu-cottbus.de/jenkins/buildStatus/icon?job=Reflex-BuildAll-develop)](https://idun.informatik.tu-cottbus.de/jenkins/job/Reflex-BuildAll-develop)
[![Documentation Status](https://readthedocs.org/projects/reflex/badge/?version=latest&style=plastic)](https://reflex.readthedocs.org)

Realtime Event FLow EXecutive
=============================
REFLEX is an operating system for deeply embedded control systems and wireless
sensor network nodes. It is based on the event-flow principle, which greatly
facilitates implementation of typical applications and reusability of components.

User Documentation
------------------
Documentation is provided at [ReadTheDocs](https://reflex.readthedocs.org).
We use Sphinx and Doxygen for generation.

Community
---------

 * Mailing List: reflex-developer(at)informatik.tu-cottbus.de
 * [Subscribe](mailto:majordomo@informatik.tu-cottbus.de?body=subscribe%20reflex-developer)
   to the mailing list.
 * [Mailing List Archive](https://www.mail-archive.com/reflex-developer@informatik.tu-cottbus.de/) at mail-archive.com

Tests
-----
We test the core functionality of REFLEX continuously. For the current status of
all active branches, have a look at our Jenkins
[Build Server](https://idun.informatik.tu-cottbus.de/jenkins/).

Version Control
---------------
We use git as version control system and
[git-flow](http://nvie.com/posts/a-successful-git-branching-model/) as branch
model. The _master_ branch always points to the latest stable release, whereas
the _develop_ branch marks the current working version. Checkout this branch to
work on. Several _feature_ branches contain work-in-progress regarding a single
feature. They are merged back into the _develop_ branch once the feature has
been tested and proofed to work.

License
-------
REFLEX is being developed at the Brandenburg University of Technology, Distributed
Systems / Operating Systems Group. It is Free Libre Open Source Software available
under the 2-clause BSD License. See the LICENSE file for details.