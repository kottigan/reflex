#ifndef BITFIELD_H_
#define BITFIELD_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
namespace reflex
{
namespace data_types
{

//! \cond
namespace helper_BitField
{
template<typename T, T base, uint8 exponent>
struct Power
{
	enum
	{
		Result = base * Power<T, base, exponent - 1>::Result
	};
};

template<typename T, T base>
struct Power<T, base, 1>
{
	enum
	{
		Result = base
	};
};

template<typename T, T base>
struct Power<T, base, 0>
{
	enum
	{
		Result = 1
	};
};

}
//! \endcond

/*!
 \ingroup data_types
 \brief Splits an integer type into different bit sets.

 This implementation is highly portable and does not have any overhead compared
 to the native implementation. Moreover, native bit-fields are non-portable and
 non-predictable and thus not recommended.

 Use BitField always inside a union where every member has the same base-type.
 For this reason, BitField does not specify any constructor. The parameters
 \a mostLeftBit and \a mostRightBit are to be counted from zero.


 Example:
 \code
 * union {
 * 		data_types::BitField<uint8, 7,4> highNibble;
 * 		data_types::BitField<uint8, 3,0> lowNibble;
 * };
 *
 * 		highNibble = 0x0B; // 0xB0 will be stored internally.
 * 		uint8 test = highNibble; // highNibble will be transformed back into 0x0B.
 \endcode

 */
template<typename T, uint8 mostLeftBit, uint8 mostRightBit>
class BitField
{
	enum
	{
		/*!
		  Since more than one BitField object share the same memory
		  address, each must only operate on the specified bits.
		  Thus, masking is needed before store and load.

		  This mask is 2^n-1, where n is the amount of bits.
		 */
		Mask = helper_BitField::Power<T, 2, mostLeftBit - mostRightBit + 1>::Result - 1
	};
public:
	BitField& operator=(const T& value)
	{
		this->data = data | (value & Mask) << mostRightBit;
		return *this;
	}

	operator T() const
	{
		return (data >> mostRightBit) & Mask;
	}

private:
	T data;
};
}
}
#endif /* BITFIELD_H_ */
