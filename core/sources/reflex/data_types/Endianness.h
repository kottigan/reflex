#ifndef ENDIANESS_H_
#define ENDIANESS_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
namespace reflex
{
namespace data_types
{
//! \cond
namespace endianness
{
/**
 * Base class for BigEndian and LittleEndian.
 */
template<typename T, bool wantBig>
class FixedEndian
{
public:
	operator T() const
	{
		return swap(data);
	}

	uint8& operator[](uint8 index)
	{
		return reinterpret_cast<uint8*> (&this->data)[index];
	}

protected:

	/**
	 * Helper union to determine the host endianess.
	 * This code has been stolen from a great article on
	 * http://www.realtime.bc.ca/articles/endian-safe
	 */
	union HostEndianness
	{
		const unsigned i;
		char c[sizeof(unsigned)];

		HostEndianness() :
			i(1)
		{
		}

		/**
		 * Returns true when the host endianness is big.
		 */
		bool isBig() const
		{
			return c[0] == 0;
		}
	};

	T data; ///< internal data representation

	/**
	 * Performs a swap operation if the wanted endianness is
	 * equal to the host's endianness.
	 * It looks complicated, but the optimizer will
	 * reduce the overhead dramatically.
	 */
	static T swap(const T& value)
	{
		if (HostEndianness().isBig() == wantBig)
		{
			return value;
		}
		else
		{
			T result;

			uint8* dest = reinterpret_cast<uint8*> (&result);
			const uint8* src = reinterpret_cast<const uint8*> (&value + 1);

			for (size_t i = 0; i < sizeof(T); i++)
			{
				src--;
				*dest = *src;
				dest++;
			}

			return result;
		}
	}

};

}

//! \endcond

/**
 * \brief A big endian container that does byte-swapping automatically.
 *
 * Use this data type when ever a data structure in memory must have
 * a specified byte-order. BigEndian can be used like a plain integer type.
 *
 * \code
 * BigEndian<uint16> value;
 * value = 0xAABB; // If the host endianness is little, 0xAABB will be swapped.
 * uint16 test = value; // The value is again being swapped.
 * \endcode
 */
template<typename T>
class BigEndian: public endianness::FixedEndian<T, true>
{
public:
	BigEndian& operator=(const T& value)
	{
		this->data = swap(value);
		return *this;
	}
};

/**
 * \brief A little endian container that does byte-swapping automatically.
 *
 * Use this data type when ever a data structure in memory must have
 * a specified byte-order. LittleEndian can be used like a plain integer type.
 *
 * \code
 * LittleEndian<uint16> value;
 * value = 0xAABB; // If the host endianness is big, 0xAABB will be swapped.
 * uint16 test = value; // The value is again being swapped.
 * \endcode
 */
template<typename T>
class LittleEndian: public endianness::FixedEndian<T, false>
{
public:
	LittleEndian& operator=(const T& value)
	{
		this->data = swap(value);
		return *this;
	}
};

}
}

#endif /* ENDIANESS_H_ */
