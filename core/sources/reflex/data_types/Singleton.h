/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:	 Sören Höckner
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef REFLEX_LIB_DATA_TYPES_SINGLETON_H
#define REFLEX_LIB_DATA_TYPES_SINGLETON_H

#include "reflex/debug/Assert.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/memory/new.h"
#include "reflex/types.h"

#ifdef OMNETPP
//! special handling of OMNETPP...
#include "omnetpp/ReflexBaseApp.h"
#endif

namespace reflex
{

namespace data_types
{

/*!
 \brief Adds singleton semantics to a type \c T.
 */
template<typename T, typename TLock = InterruptLock>
class Singleton
{
public:

	//! Creates the single object if it does not exist.
	Singleton();

	//! Creates the single object with arguments if it does not exist.
	template<typename T1>
	explicit Singleton(const T1& val1);

	//! Creates the single object with arguments if it does not exist.
	template<typename T1, typename T2>
	explicit Singleton(const T1& val1, const T2& val2);

	//! Destroys the single-object instance of type \c T when being the last instance.
	~Singleton();

	/**
	 * \brief Dereferences the single-object instance and returns a valid pointer/reference.
	 * @{
	 */
	static inline T& getInstance();
	inline T& operator*();
	inline const T& operator*() const;
	inline T* operator->() throw ();
	inline const T* operator->() const;
	inline operator T*();
	inline operator const T*() const;
	///@}

private:
	//! Internal allocator for the single-instance object.
	class SingleObject
	{
	public:
		//! Dummy constructor, which must not initialize members!
		// SingleObject();
		// ~SingleObject();

		inline T* address();
		inline void upRef();
		inline void downRef();
		inline bool isValid();

		uint8 memory[sizeof(T)]; //! place-holder memory for the single-object

	private:
		uint8 references; //! reference counter of the single-object
	};

#ifndef OMNETPP
public:
	static SingleObject instance; //! the single object allocator
private:
	static inline SingleObject& getObject() {
		return instance;
	}
#else
private:
	//! OMNETPP has a Singleton per logic node...
	static SingleObject instance[NODECOUNT]; //! the single object allocator
	static inline SingleObject& getObject()
    {
		return instance[ReflexBaseApp::currentId()];
	}
#endif
};

// Implementation

/*!
 * \class Singleton
 *
 * \ingroup data_types
 *
 * \tparam T		type to be created once
 * \tparam TLock	sentinel type that guards access to Singleton methods,
 * 					InterruptLock by default
 *
 * Singleton wraps a class \c T and limits its instantiation to exactly one copy,
 * the "single object". Access to this object is only permitted through
 * instances of Singleton, which serve as sentinel objects and may be created
 * at multiple locations across the application.
 *
 * An example for the usage of Singleton is a hardware abstraction driver.
 * Typically, this driver has to be accessed from within multiple classes,
 * but must be initialized before the first usage.
 *
 * \code
 * class A
 * {
 * 		Singleton<Counter> counter;
 * 		//...
 * 		counter->inc();
 * 		//...
 * };
 *
 * class B
 * {
 * 		Singleton<Counter> counter;
 * 		//...
 * 		counter->inc();
 * 		//...
 * };
 *
 * class C
 * {
 * 		void doSomething()
 * 		{
 *  		Singleton<Counter>()->inc();
 * 		}
 * };
 * \endcode
 *
 * In class \c A and class \c B the member \a counter refers to the same instance of the
 * class \c Counter. It is even possible to create Singleton instances during a
 * method on stack as it is shown in class \c C.
 * The latter may be used for rare access to the Singleton instance, e.g. in the constructor
 * of a component. But when being accessed very often during runtime, Singleton should be a
 * class member to minimize overhead for reference counting (class \c A or \c B).
 * Up to 255 Singleton instances are possible in an application.
 *
 * Singleton offers multiple overloaded methods for dereferencing (see
 * getInstance() for example). They are not slower than dereferencing plain pointers
 * and should be resolved by the optimizer.
 *
 * Memory consumption is very low, too. Beside the memory for the single object,
 * Singleton does not consume any extra memory. Therefore the size of Singleton is
 * 1 (empty class).
 *
 * All methods of Singleton related to construction/destruction are guarded by an
 * configurable lock (\c TLock) which is an InterruptLock by default.
 * But access to the single object is not reentrant. Anyway, accessing a Singleton from
 * concurrent locations is generally a bad idea as it will likely cause synchronization
 * problems.
 *
 */

#ifndef OMNETPP
template<typename T, typename TLock>
typename Singleton<T, TLock>::SingleObject Singleton<T, TLock>::instance;
#else
template<typename T, typename TLock>
typename Singleton<T, TLock>::SingleObject Singleton<T, TLock>::instance[NODECOUNT];
#endif

template<typename T, typename TLock>
Singleton<T, TLock>::Singleton()
{
	TLock lock;
	if (!getObject().isValid())
	{
		new (getObject().address()) T();
	}
	getObject().upRef();
}

/*!
 \overload
 */
template<typename T, typename TLock>
template<typename T1>
Singleton<T, TLock>::Singleton(const T1& val1)
{
	TLock lock;
	if (!getObject().isValid())
	{
		new (getObject().address()) T(val1);
	}
	getObject().upRef();
}

/*!
 \overload
 */
template<typename T, typename TLock>
template<typename T1, typename T2>
Singleton<T, TLock>::Singleton(const T1& val1, const T2& val2)
{
	TLock lock;
	if (!getObject().isValid())
	{
		new (getObject().address()) T(val1, val2);
	}
	getObject().upRef();
}

template<typename T, typename TLock>
Singleton<T, TLock>::~Singleton()
{
	TLock lock;
	getObject().downRef();
	if (!getObject().isValid())
	{
		getObject().address()->~T();
	}
}

template<typename T, typename TLock>
T& Singleton<T, TLock>::getInstance()
{
	return *getObject().address();
}

template<typename T, typename TLock>
T& Singleton<T, TLock>::operator*()
{
	return *getObject().address();
}

template<typename T, typename TLock>
const T& Singleton<T, TLock>::operator*() const
{
	return *getObject().address();
}

template<typename T, typename TLock>
T* Singleton<T, TLock>::operator->() throw ()
{
	return getObject().address();
}

template<typename T, typename TLock>
const T* Singleton<T, TLock>::operator->() const
{
	return getObject().address();
}

template<typename T, typename TLock>
Singleton<T, TLock>::operator T*()
{
	return getObject().address();
}

template<typename T, typename TLock>
Singleton<T, TLock>::operator const T*() const
{
	return getObject().address();
}

/*!
 \class Singleton::SingleObject

 SingleObject provides the storage and a reference counter for the
 unique object.

 A struct was chosen here, because that type should have the greatest alignment.
 So SingleObject::memory should start on a well aligned address.

 Members of SingleObject are not explicitly initialized, because it is a static
 member of Singleton. In REFLEX below 2.0, the application instance
 is a static variable, too and it is not guaranteed, that Singleton::instance
 will be initialized before the whole application. To avoid any side-effects,
 SingleObject() must not touch any of its members.
 The solution is easy. According to C++STD::3.6.2, memory space for static
 members is always zeroed out during load-time, even before any constructor
 is called. This guarantees #references to be always zero
 on startup.

 \note Constructor and destructor of this class
 */

//template<typename T, typename TLock>
//Singleton<T, TLock>::SingleObject::SingleObject()
//{
//}

//template<typename T, typename TLock>
//Singleton<T, TLock>::SingleObject::~SingleObject()
//{
//	references = 0;
//}

template<typename T, typename TLock>
T* Singleton<T, TLock>::SingleObject::address()
{
	return reinterpret_cast<T*> (&memory[0]);
}

template<typename T, typename TLock>
void Singleton<T, TLock>::SingleObject::upRef()
{
	Assert(references < 0xFF);
	references++;
}

template<typename T, typename TLock>
void Singleton<T, TLock>::SingleObject::downRef()
{
	Assert(references > 0);
	references--;
}

template<typename T, typename TLock>
bool Singleton<T, TLock>::SingleObject::isValid()
{
	return references > 0;
}

}
}

#endif

