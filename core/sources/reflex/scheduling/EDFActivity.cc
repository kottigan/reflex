/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */

#include "reflex/scheduling/EDFActivity.h"
#include "reflex/CoreApplication.h"

using namespace reflex;

/*!
\fn EDFActivity::run()
\brief Activity functionality.

This method is the place for activity code and has to be re-implemented by the
developer. Normally, one would not derive from this class, but use an activity
functor instead. Activities have run-to-completion semantics. So the developer
has to make sure, that it is free of spin-locks.

\see ActivityFunctor
 */



EDFActivity::EDFActivity()
{
    status = IDLE;
    rescheduleCount = 0;
    _responseTime = 0;
    locked = false;
}

void EDFActivity::trigger()
{
    mcu::CoreApplication::instance()->scheduler()->schedule(this);
}

/*!
\brief Prevents activity from being triggered..

Scheduling is still possible if the activity has already been triggered.
This has to be considered when calling lock(). Therefore locking is only
safe from within the activity itself.
 */
void EDFActivity::lock()
{
    locked = true;
    mcu::CoreApplication::instance()->scheduler()->lockCount++; //notify scheduler
}

/*!
\brief Marks the activity as schedulable again.

If there were scheduling requests during lock time, the activity is scheduled
now.
 */
void EDFActivity::unlock()
{
    mcu::CoreApplication::instance()->scheduler()->unlock(this); //invoke scheduler for proper unlock
}

void EDFActivity::updateDeadline()
{
    deadline = mcu::CoreApplication::instance()->scheduler()->clock.getTime() + _responseTime;
}

bool EDFActivity::lessEqual(EDFActivity& right)
{
    //overflow aware calculation
    Time systemTime = mcu::CoreApplication::instance()->scheduler()->clock.getTime();
    Time leftOffset = this->deadline - systemTime ;
    Time rightOffset = right.deadline - systemTime ;
    return ( leftOffset <= rightOffset );
}


