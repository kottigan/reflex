/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/Scheduler.h"

using namespace reflex;

/** This method makes a signature check on the public elements of the choosen
 *	scheduler.
 */
static void check()
{
	Scheduler sched;
	sched.start();
}
