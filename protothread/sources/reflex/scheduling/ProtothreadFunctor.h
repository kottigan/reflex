#ifndef ProtothreadFunctor_h
#define ProtothreadFunctor_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	ProtothreadFunctor
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Activity-Wrapper for any memberfunction.
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as 
 *    published by the Free Software Foundation, either version 3 of the 
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/scheduling/Functor.h"

namespace reflex {

/**
 *	Wraps member function to become a schedulable object.
 *	Use it at follows:
 *	class A{ void foo(){} };
 *	A a;
 *	ProtothreadFunctor<A,&A::foo> functor(A& a);
 */
template <typename T, bool (T::* MemFn)()>
class ProtothreadFunctor : public Functor {
public:

	/** Constructor attaches the functor with an object of template type T
	 */
	ProtothreadFunctor(T& pObj) : pObj(pObj) {
	}

	bool get() {
		return (pObj.*MemFn)();
	}

private:
	/** Reference to the related object.
	 */
	T& pObj;
};

} //namespace reflex

#endif

