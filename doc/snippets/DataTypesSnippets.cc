#include "reflex/data_types/Flags.h"

using namespace reflex;
using namespace data_types;

//! [FlagsDeclaration]
class MyClass {
public:
	enum OutputFlag : uint8 {
		Nothing = 0,
		Output0 = 1 << 0,
		Output1 = 1 << 1,
		Output2 = 1 << 2
	};
	DECLARE_FLAGS(OutputFlags, OutputFlag)

	void setOutputsEnabled(OutputFlags outputs);
	...
};

DECLARE_OPERATORS_FOR_FLAGS(MyClass::OutputFlags)
//! [FlagsDeclaration]


//! [FlagsUsage]
obj.setOutputsEnabled(MyClass::Output0 | MyClass::Output1);
//! [FlagsUsage]
