Porting REFLEX to a new platform
================================

Several files are needed.

Controller
----------
For each controller port of REFLEX the files given below are needed. Files
marked with an asterisk must be named and placed as mentioned. The non marked files
can be named as you want only the functionality must be provided.

:Controller.mk*: Configuration of the compile toolchain
:Sources.mk*: Definition of source files needed for each application for this microcontroller
:include/System.h*: Definition of the base system for this microcontroller;
                    Contains the base system components (Scheduler, InterruptGuardian, PowerManagement and Clock) and a timer which feeds the system clock
:include/MachineDefinitions.h: Defines the base addresses for register sets for the drivers and may specific definitions for the controller (BigEndian vs. LittleEndian)
:include/types.h*: Definition of the basic type aliases for the system only Time, caddr\_t and size\_t are needed;
                   But also definition for intx, uintx should be done e.g. to allow applications to define data packets for communication.
:include/interrupts/InterruptVector.h*: The definition of the interrupt vectors addresses (not their values) and definition of the number of vectors in variable `MAX_HANDLERS`.
:src/interrupts/interruptFunctions.s: The definition of ``extern "C" void _interruptsEnable()`` and ``extern "C" bool _interruptsDisable()`` in assembly form; 
                                      Here you have to know about procedure calling convention of your compiler - microcontroller environment.
:src/interrupts/MachineInterruptGuardian.cc: Implements the ``registerInterrupt()`` and ``init()`` methods declared by InterruptGuardian in the system tree.
:src/powerManagement/sleepFunctions.s: Implement the function (all ``extern "C"``) ``void _wait()``, ``void _dream()``, ``void _sleep()``,
                                       ``void _stop()`` and ``void _reset()``, which are used for powerManagement.

Platform
--------
:Platform.mk: Mainly definition of download toolchain and selection of specific member of a processor family.
:linkerscript: Memory layout definition for the used microcontroller.

