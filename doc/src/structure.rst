Directory Structure
===================
The directory structure of REFLEX splits the bunch of code into the parts
which are described below. The main intention was to build a structure which allows
re-use and portability of code.

- __system__: This directory contains the code for the base system (Scheduler, InterruptGuardian ...) as well as some fundamental classes for effective event flow programming.
- __lib__: This directory contains code which is not basic part of the system, but often used, like memory management functionality or timers.
- __controller__: This directory contains environment files for compilation and drivers for the on-chip devices of the microcontroller e.g. the AD-Converter.
- __platform__ This directory contains environment files for compilation and download. Additionaly driver code for devices on the board e.g. LEDs can be found here.
- __devices__ This directory contains drivers for external devices which are connected via a standard interface, such as RS232 or I2C, to the controller.
- __examples__ This directory contains some sample applications.

Build Environment
-----------------
When an application is compiled the structure reflects also in the Makefiles
which are included as shown in the following figure:

![Include graph of files during make][img:makefiles]

The purpose of the files is:
- __Makefile__: Definition of platform and variables controlling the build process which are not defined in the `*.mk` files.
- __StandardMake.mk__: Assembling of sources and paths and definition of standard targets e.g. clean, all.
- __Platform.mk__: Mainly configuration of used controller, final format (e.g. SRec) and configuration for download.
- __Controller.mk__: Mainly configuration of the build tools like setting default parameters for compilation.
- __Sources.mk__ (_system, controller_ and _platform_): Definitions of the files for the base system on a choosen platform.
- __Sources.mk__ (_application_): All sources which are not in the base system but needed for the application.


It is mandatory that the application Makefile is in the directory where the binaries
are placed and that make is called inside of this directory.


[img:makefiles]: figures/filesForMake.svg ""
