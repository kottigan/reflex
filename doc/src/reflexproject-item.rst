.. _reflexproject:

==================
ReflexProject Item
==================

Root Qbs item for REFLEX projects. Inherits
`Project <http://doc.qt.io/qbs/project-item.html>`_

This Qbs item serves as a default root item for REFLEX projects. It controls,
which REFLEX packages are built and contains configuration properties for them.

When having a multi-project setup where multiple products are built in
separate sub-projects, the ``ReflexProject`` item has to be the root item.

.. code-block:: qml

    import qbs 1.0
    import ReflexApplication
    import ReflexProject

    ReflexProject {
        ReflexApplication {
            name: "myApplication"
            files : [
                //
            ]

            // ...
        }

        Product {
            name : "myLib"

            // ...
        }

    }


Platform-independent Properties
===============================

+---------------------------------+--------------+------------+---------------------------------------------------------+
| Property                        | Type         | Default    | Description                                             |
+=================================+==============+============+=========================================================+
| buffer_enabled                  | bool         | true       | Build the buffer package and add a dependency to each   |
|                                 |              |            | |ReflexApplication| item.                               |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| buffer_maxPoolCount             | int          | 8          | Maximum amount of buffer pools.                         |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| buffer_sizeType                 | string       | ``uint8``  | C++ type for variables related to the buffer size. The  |
|                                 |              |            | default value is ``uint8`` which allows an efficient    |
|                                 |              |            | implementation on 8-bit architectures if the buffer size|
|                                 |              |            | does not exceed 255 Bytes.                              |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| components_enabled              | bool         | false      | Build the components package and add a dependency to    |
|                                 |              |            | each |ReflexApplication| item.                          |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| core_schedulingScheme           | string       | |rep1|     | Scheduling scheme being used. Possible values are       |
|                                 |              |            | EDF_SCHEDULING, EDF_SCHEDULING_NONPREEMPTIVE,           |
|                                 |              |            | EDF_SCHEDULING_SIMPLE, FIFO_SCHEDULING,                 |
|                                 |              |            | PRIORITY_SCHEDULING, PRIORITY_SCHEDULING_NONPREEMPTIVE  |
|                                 |              |            | PRIORITY_SCHEDULING_SIMPLE, TIME_TRIGGERED_SCHEDULING   |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| devices_enabled                 | bool         | true       | Build the devices package and add a dependency to each  |
|                                 |              |            | |ReflexApplication| item.                               |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| protothread_enabled             | bool         | true       | Build the protothread package and add a dependency to   |
|                                 |              |            | each |ReflexApplication| item.                          |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| reflex_path                     | path         | n/a        | The REFLEX sources root folder. This property is        |
|                                 |              |            | read-only.                                              |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| reflex_platform                 | string       | n/a        | The current platform. The value of this item is deduced |
|                                 |              |            | from ``qbs.architecture`` in the toolchain. This        |
|                                 |              |            | property is read-only.                                  |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| virtualtimer_enabled            | bool         | true       | Build the virtualtimer package and add a dependency to  |
|                                 |              |            | each |ReflexApplication| item.                          |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| xml_enabled                     | bool         | false      | Build the xml package and add a dependency to           |
|                                 |              |            | each |ReflexApplication| item.                          |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| xml_messageQueueSize            | int          | 1          | Maximum amount of message requests in the output queue  |
|                                 |              |            | of :cpp:class:`reflex::xml::XmlWriter`                  |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| xml_maxTagLength                | int          | 20         | Maximum character length of XML tags                    |
+---------------------------------+--------------+------------+---------------------------------------------------------+


.. |rep1| replace:: FIFO_SCHEDULING


Msp430x-specific Properties
===========================

+---------------------------------+--------------+------------+---------------------------------------------------------+
| Property                        | Type         | Default    | Description                                             |
+=================================+==============+============+=========================================================+
| msp430x_infoaddress             | int          | 0x1800     | Memory address of application meta information. That    |
|                                 |              |            | includes the node-identifier.                           |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| msp430x_mcu                     | string       | cc430x6137 | Controller type                                         |
|                                 |              |            |                                                         |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| msp430x_bootloader_enabled      | bool         | false      | Whether the wireless-bootstrap-loader is enabled. If    |
|                                 |              |            | true, a bootloader binary is being built. This requires |
|                                 |              |            | ``msp430x_update_enabled`` to be true.                  |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| msp430x_bootloader_address      | int          | 0x8000     | Start address to which the bootloader is being linked.  |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| msp430x_bootloader_channel      | int          | 0          | Physical Radio Channel that the bootloader uses.        |
|                                 |              |            | Allowed values are 0, 50, 80, 110                       |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| msp430x_update_enabled          | bool         | false      | Whether update functionality is enabled. If true, every |
|                                 |              |            | application product is linked by default to             |
|                                 |              |            | ``msp430x_update_firmwareaddress``. This property can   |
|                                 |              |            | be disabled for every |ReflexApplication| item, but if  |
|                                 |              |            | update functionality is desired, it has to be activated |
|                                 |              |            | globally.                                               |
+---------------------------------+--------------+------------+---------------------------------------------------------+
| msp430x_update_firmwareaddress  | int          | 0x8c00     | Memory address to which the application is being linked |
|                                 |              |            | when update support is enabled.                         |
+---------------------------------+--------------+------------+---------------------------------------------------------+
|                                 |              |            |                                                         |
+---------------------------------+--------------+------------+---------------------------------------------------------+


Omnetpp-specific Properties
===========================

+---------------------------------+--------------+------------+---------------------------------------------------------+
| Property                        | Type         | Default    | Description                                             |
+=================================+==============+============+=========================================================+
| omnetpp_nodeCount               | int          | 1          | Maximum number of nodes in an omnetpp simulation.       |
+---------------------------------+--------------+------------+---------------------------------------------------------+

.. |ReflexApplication| replace:: :ref:`ReflexApplication <reflexapplication>`
