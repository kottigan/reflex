Event Channels
==============
Since event flow in REFLEX is asynchronous, buffers are needed between
components. Buffers store data or events and trigger the corresponding
activity. Writing to buffers is interpreted as raising an event. It is
possible that multiple sources write to one buffer, but only the corresponding
activity is allowed to read. All buffers implement the `Sink` interface. The
most important buffer types, [Event], [SingleValue], [Fifo] and [Queue] are
already provided. The base class [Sink0] \ref reflex::Sink0 "" represents a
dataless sink and declares a `notify()` method. In contrast, [Sink1<T>]
declares an `assign()` method which allows assigning a value to derived sinks.
Since buffers implement a sink interface they can be written to. For reading
them, they usually implement a `get()` method. Note, that the application
programmer is free to implement specialized versions of sinks for their own
purposes.

An [Event] is only a pseudo-buffer since it stores no data. It only triggers
the corresponding activity. This is useful, for example, for clock ticks which
must be propagated. [SingleValue] buffers store exactly one value. The value
can be overwritten, so a new value replaces the old one. The corresponding
activity is triggered when a new value is written on the buffer and the
previous value was already read. This is an optimization since it makes no
sense to read the same data twice. The next buffer type is [Fifo], which
stores `m` n-tuples and rejects assignment when it is full. [Queue] can
store chain-able elements, therefore it is never full.


The buffers are implemented as templates, and can therefore store any data
type, while providing a type safe interface. Event propagation is done by
either writing data to a sink or calling `notify()` for dataless event
channels. The sink is responsible for the scheduling of its corresponding
activity. The execution in the graph is inherently asynchronous, since the
scheduled activities are dispatched sometime in the future and not immediately
after scheduling. The components are connected by event channels, which can
propagate pure events and data. When a buffer receives an event it triggers
the corresponding activity.

[Event]: \ref reflex::Event ""
[SingleValue]: \ref reflex::SingleValue1 ""
[Fifo]: \ref reflex::Fifo1 ""
[Queue]: \ref reflex::Queue
[Sink0]: \ref reflex::Sink0 ""
[Sink1<T>]: \ref reflex::Sink1 ""
