...
enum globalEpochCounterConstants
{
	/* The node, which is responsable for the epoch synchronization. */
	RESPONSIBLE_FOR_THE_CLOCK = 0,

	/* This value defines the interval for the clock synchronisation. */
	EPOCH_SYNCH_INTERVAL 	= 30*1000,

	/* This value defines the resolution (in ms) of the tinydsm replication clock. */
	EPOCH_DURATION_IN_MS = 100
};
...