...
/* The datatype for the variable id. */
typedef uint8 	varid_t;

/* The datatype for the sensornode id. */
typedef uint8 	nodeid_t;

/* The tinydsm time datatype. */
typedef uint32 	timestamp_t;

/* The datatype for the hop count. #*/
typedef uint8 	hops_t;
...