#include "reflex/rf/RADIO.h"
#include "reflex/memory/SizedPool.h"
...
#include "tinydsm/TinyDSMComponents.h"
#include "tinydsm/CommunicationProtocol.h"
#include "Application.h"

namespace reflex {
...
class NodeConfiguration : public System {
public:
	NodeConfiguration():
			...,
			application(),
			radio(&radioPool),
			tinydsmComponents(&dsmPool),
			communicationProtocol(&dsmPool)
	{
		...
		radio.connect_out_data(communicationProtocol.getLowerInput());
		communicationProtocol.setLowerOutput(radio.get_in_input());

		tinydsmComponents.setProtocolOutput(communicationProtocol.getUpperInput());
		communicationProtocol.setUpperOutput(tinydsmComponents.getProtocolInput());

		tinydsmComponents.init();
		application.init();
		...
	}
	...
	SizedPool<IOBufferSize,NrOfStdOutBuffers> dsmPool;
	SizedPool<IOBufferSize,NrOfStdOutBuffers> radioPool;

	Application						application;
	mcu::RADIO						radio;
	tinydsm::TinyDSMComponents 		tinydsmComponents;
	tinydsm::CommunicationProtocol	communicationProtocol;
};
}
...