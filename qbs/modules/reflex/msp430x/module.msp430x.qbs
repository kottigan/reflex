import qbs 1.0
import qbs.FileInfo
import qbs.File
import qbs.ModUtils
import qbs.TextFile


/*!
Module for msp430x platform.

- provides a rule to convert elf binaries into .hex files

*/
Module
{
    property string mcu : project.msp430x_mcu

    readonly property int nodeinfoaddress : project.msp430x_nodeinfoaddress
    readonly property bool bootloader_enabled : project.msp430x_bootloader_enabled
    readonly property int bootloader_address : project.msp430x_bootloader_address
    readonly property string bootloader_channel : project.msp430x_bootloader_channel

    readonly property bool update_enabled : product.msp430x_update_enabled
    readonly property int update_firmwareaddress : project.msp430x_update_firmwareaddress

    Depends { name : "cpp" }

    cpp.commonCompilerFlags : [
        "-ffunction-sections",
        "-fdata-sections",
        "-mmcu=" + mcu
    ]

    cpp.linkerFlags : [
        "-Wl,--gc-sections"
    ]

    validate : {
        if ((update_enabled == true)
                && (project.msp430x_update_enabled == false))
            throw("Update functionality has to be enabled project-wide before "
                  + "it can be used in product '" + product.name + "'. "
                  + "Please set 'msp430x_update_enabled' in the main project "
                  + "item.");
        if (mcu == undefined)
        {
            throw("Define a msp430x_mcu property in Your project item or use "
                  + "the ReflexProject item as root item in Your project or "
                  + "set the mcu property for the reflex.msp430x module in "
                  + "Your toolchain profile.");
        }
    }

    /* Transforms elf into hex files */
    property string objcopyInstallPath : cpp.toolchainInstallPath
    property string objcopyName : "objcopy"

    Rule {
        inputs: ['application']

        Artifact {
            fileTags: ['hex']
            filePath: FileInfo.baseName(input.filePath) + '.hex'
        }

        prepare : {
            var args = [
                        "-O",
                        "ihex",
                        input.filePath,
                        output.filePath
                    ];
            var objCopyPath =  product.moduleProperty('reflex.msp430x', 'objcopyInstallPath');
            var prefix = product.moduleProperty('cpp', 'toolchainPrefix');
            var executable = objCopyPath + "/" + prefix + "objcopy";
            var cmd = new Command(executable, args);
            cmd.description = "converting to hex: "
                    + FileInfo.fileName(output.filePath);
            cmd.highlight = "linker";
            return cmd;
        }
    }

    /* Generates sources for firmware version information */
    Transformer {
        condition : product.type.contains("application")
                    && product.msp430x_update_enabled == true
                    && product.noVersionInformation != true

        Artifact {
            fileTags : ['cpp']
            filePath: 'generated-files/firmware-version.cpp'
        }

        prepare : {
            var cmd = new JavaScriptCommand();
            cmd.description = "Generating firmware meta information";
            cmd.highlight = "codegen";
            cmd.sourceCode = function() {
                var tf = new TextFile(output.filePath, TextFile.WriteOnly);
                tf.write('#include <reflex/CoreApplication.h>\n');
                tf.write("DEFINE_SOFTWARE_VERSION(" + product.version + ");\n");
                tf.close();
            }
            return cmd;
        }
    }

    /* Produces firmware update information */
    Rule {
        inputs: ['hex']

        Artifact {
            fileTags: ['firmware-info']
            filePath: FileInfo.baseName(input.filePath) + '.json'
        }

        prepare : {
            var cmd = new JavaScriptCommand();
            cmd.description = "Generating firmware info '" + output.fileName + "'.";
            cmd.highlight = "codegen";
            cmd.sourceCode = function() {
                var content = {
                    file : input.fileName,
                    address : project.msp430x_update_firmwareaddress,
                    version : product.version
                }

                var tf = new TextFile(output.filePath, TextFile.WriteOnly);
                tf.write(JSON.stringify(content));
                tf.close();
            }
            return cmd;
        }
    }
}
