import qbs 1.0
import qbs.FileInfo
import qbs.File
import qbs.ModUtils


/*!
Module for omnetpp environment.

It contains path declarations for the omnet and mixim library which have to be
set via the toolchain profile.

Additionally, it contains a transformer rule for omnet message files, which
transforms `.msg` files into source code. Those files are written to the
`generated-files` folder in the build directory. This path is automatically
added to the include path of the `cpp` module.

*/
Module
{
    property string omnetpath : undefined
    property bool hasTcl : undefined
    property string miximpath : undefined

    validate : {
        if (!omnetpath)
        {
            throw "omnetpath is not set. Please choose the omnet installation path "
                + "for the omnetpp module in Your toolchain profile.";
        }
        if (!miximpath)
        {
            throw "miximpath is not set. Please choose a mixim installation path "
                + "for the omnetpp module in Your toolchain profile.";
        }
        if (hasTcl == undefined)
        {
            throw "hasTcl is not set. Please specify in omnetpp.hasTcl "
                + "(true/false) if omnetpp has been configured with TCL support "
                + " or not. This will affect the library linkage.";
        }
    }

    Depends { name: "cpp" }

    /* Tags omnetpp .ned files as ned */
    FileTagger {
        patterns: "*.ned"
        fileTags: ["ned"]
    }

    /* Tags omnetpp .msg files as as msg */
    FileTagger {
        patterns: "*.msg"
        fileTags: ["msg"]
    }

    /* Rule for msg files */
    Rule {
        inputs: ['msg']

        Artifact {
            fileTags: ['cpp']
            filePath: 'generated-files/' + input.completeBaseName + '_m.cc'
        }

        Artifact {
            fileTags: ['hpp']
            filePath: 'generated-files/' + input.completeBaseName + '_m.h'
        }

        prepare: {
            var args = [];
            args.push("-h");
            args.push(input.filePath);
            var compilerPath = product.moduleProperty('reflex.omnetpp', 'omnetpath') + "/bin/opp_msgc"
            var cmd = new Command(compilerPath, args);
            cmd.workingDirectory = FileInfo.path(outputs['cpp'][0].filePath);
            cmd.description = 'generating code from ' + input.filePath;
            cmd.highlight = 'compiler';
            return cmd;
        }
    }

    cpp.includePaths : [
        product.buildDirectory + "/generated-files",
        omnetpath + "/include",
        miximpath + "/src",
        miximpath + "/src/base/connectionManager/",
        miximpath + "/src/base/utils/",
        miximpath + "/src/inet_stub/base/",
        miximpath + "/src/inet_stub/util/",
        miximpath + "/src/modules/connectionManager/"
    ]

    cpp.defines : [
        "OMNETPP",
        "LINUX",
        "NODECOUNT=" + project.omnetpp_nodeCount
    ]

    cpp.libraryPaths : [
        omnetpath + "/lib/",
        miximpath + "/src/"
    ]

    /* Linker flags apply to the final application product only */
    Properties {
        condition : product.type == "application"

        /* Linker flags apply to the final application product only */
        cpp.linkerFlags : "-Wl,--no-as-needed"

        cpp.libraryPaths : [
            omnetpath + "/lib/",
            miximpath + "/src/"
        ]

        /* We link to all available libraries, even though not all are needed */
        cpp.dynamicLibraries : {
            var libs = [];
            if (qbs.buildVariant == "debug")
            {
                if (hasTcl) {
                    libs.push("opptkenvd");
                } else {
                    libs.push("oppcmdenvd");
                }
                libs.push("oppenvird");
                libs.push("oppeventlogd");
                libs.push("opplayoutd");
                libs.push("oppscaved");
                libs.push("oppsimd");
                libs.push("oppnedxmld");
                libs.push("mixim");
            }
            return libs;
        }

        cpp.staticLibraries : {
            var libs = [];
            if (qbs.buildVariant == "debug")
            {
                libs.push("oppmaind")
            }
            return libs;
        }
    }
}
