import qbs 1.0

// Default QBS file for REFLEX applications
Product {
    type : [ "application" ]
    readonly property string reflex_path : project.reflex_path
    readonly property string reflex_platform : project.reflex_platform
    property int    version : undefined
    property bool   buffer_enabled : project.buffer_enabled
    property bool   components_enabled : project.components_enabled
    property bool   devices_enabled : project.devices_enabled
    property bool   protothread_enabled : project.protothread_enabled
    property bool   virtualtimer_enabled : project.virtualtimer_enabled
    property bool   xml_enabled : project.xml_enabled

    /* Msp430x-specific properties */
    property bool msp430x_update_enabled : project.msp430x_update_enabled

    Properties {
        condition : msp430x_update_enabled == true
        type : [ "application", "firmware-info" ]
    }

    Properties {
        condition : (msp430x_update_enabled == true)
        cpp.linkerFlags : base.concat([
            "-Ttext=0x"
            + reflex.msp430x.update_firmwareaddress.toString(16)
        ])
    }

    /* Load modules depending on the platform */
    Depends { name : "cpp" }         // The cpp module is essential
    Depends {
        condition : [ "ez430chronos", "msp430x" ].contains(reflex_platform)
        name : "reflex.msp430x"
    }
    Depends {
        condition : reflex_platform == "omnetpp"
        name : "reflex.omnetpp"
    }

    /* Load reflex packages depending on the configuration */
    Depends {
        condition : buffer_enabled == true
        name : "buffer"
    }
    Depends {
        condition : components_enabled == true
        name : "components"
    }
    Depends { name : "core" }
    Depends {
        condition : devices_enabled == true
        name : "devices"
    }
    Depends {
        condition : reflex_platform == "ez430chronos"
        name : "ez430chronos"
    }
    Depends { name : "platform" }
    Depends {
        condition : protothread_enabled == true
        name : "protothread"
    }
    Depends {
        condition : virtualtimer_enabled == true
        name : "virtualtimer"
    }
    Depends {
        condition : xml_enabled == true
        name : "xml"
    }


    // This group item applies to files tagged with "application" only
    // which is the application binary. It is marked for installation
    // in the bin folder.
    Group {
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir : "bin"
    }

    // Install firmware update files along with the binaries
    Group {
        condition : msp430x_update_enabled == true
        fileTagsFilter: [
            "firmware-info",
            "hex"
        ]
        qbs.install : true
        qbs.installDir : "bin"
    }
}
